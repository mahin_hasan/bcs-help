package codedrizzlers.com.bcsrelease.others.PAlert.common;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import static android.content.Context.MODE_PRIVATE;
import static codedrizzlers.com.bcsrelease.main.MainActivity.context;

/**
 * Created by code drizzlers on 2/9/2017.
 */

public class PAlertUtil {
    public static SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences("progress", MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getPreferencesEditor() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();

        return editor;
    }

    public static void putLongToPreferences(String key, long value) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        editor.putLong(key, value);
        editor.apply();
    }

    public static void putBooleanToPreferences(String key, boolean bool) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        editor.putBoolean(key, bool);
        editor.apply();
    }

    public static View createAlertView(int layout) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alertView = inflater.inflate(layout, null);

        return alertView;
    }

    public static AlertDialog createAlert() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        AlertDialog alert = dialogBuilder.create();

        return alert;
    }

    public static void initImageView(View alertView, int imageId, int drawableId) {
        Drawable drawable =
                ContextCompat.getDrawable(context.getApplicationContext(), drawableId);

        ImageView imageView = (ImageView) alertView.findViewById(imageId);
        imageView.setImageDrawable(drawable);

    }

    public static void initButton(int buttonInt, View view, View.OnClickListener listener) {
        Button button = (Button) view.findViewById(buttonInt);
        button.setOnClickListener(listener);
    }

    public static void initTextView(View alertView, int textViewId, int msgId) {
        String msg = context.getResources().getString(msgId);

        TextView msgTextView = (TextView) alertView.findViewById(textViewId);
        msgTextView.setText(msg);
    }
}
