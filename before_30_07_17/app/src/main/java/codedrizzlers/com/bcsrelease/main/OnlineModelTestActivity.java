package codedrizzlers.com.bcsrelease.main;

import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.json.ResponseMapper.OnlineModelTestResponse;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.ADNCRVideo;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SPHandler;
import cz.msebera.android.httpclient.Header;

public class OnlineModelTestActivity extends BaseActivity implements OnlineModelTestRecyclerViewAdapter.FragmentInteractionListener {
    private static final String TAG = "OnlineModelTestActivity";

    Toolbar toolbar;
    ActionBar actionBar;
    RecyclerView recyclerView;
    OnlineModelTestRecyclerViewAdapter adapter;
    TextView scoreBoardTextCorrect, scoreBoardTextWrong, scoreBoardTextResult, scoreBoardTextOutOf;
    Handler handler;
    Runnable runnable;
    CardView scoreBoardContainer;
    ImageView closeScoreBoard;
    long subjectId;
    int examReward = 0;
    boolean isInstructionTourDone;

    int totalSec;
    final int singleQuestionAnswerTime = 36;
    private RewardedVideoAd mRewardedAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_model_test);

        subjectId = getIntent().getLongExtra(getString(R.string.subject_id), 0);

        allInit();
    }

    void allInit(){
        initToolbar();
        initRecyclerView();
        setTitle(getString(R.string.model_test));
        getAModelTest();
        initScoreBoard();

        //loading ad view
        initAllAds();

        isInstructionTourDone();
    }

    private void isInstructionTourDone() {
        if(SPHandler.getIsModelTestActivityInstructionTourDone()){
            startInstructionTour();
        }
    }

    void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    void initScoreBoard() {

        scoreBoardContainer = (CardView) findViewById(R.id.score_board_container);
        closeScoreBoard = (ImageView) findViewById(R.id.close);
        scoreBoardTextCorrect = (TextView) findViewById(R.id.correct);
        scoreBoardTextWrong = (TextView) findViewById(R.id.wrong);
        scoreBoardTextResult = (TextView) findViewById(R.id.result);
        scoreBoardTextOutOf = (TextView) findViewById(R.id.out_of);

        closeScoreBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scoreBoardContainer.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (totalSec == 0) {
            return false;
        }
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_model_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            checkExamFinished();
            return true;
        } else if (id == R.id.action_end_exam) {
            ADNCRVideo alert = new ADNCRVideo(this);
            alert.showAlert();

        }
        return false;
    }

    void initRecyclerView() {

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        ArrayList<OnlineModelTestResponse.Mcq> list = new ArrayList<OnlineModelTestResponse.Mcq>();
        adapter = new OnlineModelTestRecyclerViewAdapter(list, (OnlineModelTestRecyclerViewAdapter.FragmentInteractionListener) OnlineModelTestActivity.this);
        recyclerView.setAdapter(adapter);
    }

    public void getAModelTest() {

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                showUpdateProgressDialog("loading...");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                headerSyncOnSuccess(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                dismissProgressDialog();
            }
        };

        RequestParams requestParams = new RequestParams();
        requestParams.put(getString(R.string.subject), subjectId);
        AppSingleTon.ASYNC_HTTP_CLIENT.get(OnlineModelTestActivity.this, AppSingleTon.APP_URL.MT_MCQ, requestParams, handler);
    }


    void headerSyncOnSuccess(JSONObject jsonObject) {

        Log.d("model test response", jsonObject.toString());
        OnlineModelTestResponse response = AppSingleTon.APP_JSON_PARSER.onlineModelTestMcqResponse(jsonObject);
        adapter.mValues.addAll(response.results);
        adapter.initCheckedMonitorList();
        setSubTitle();
        adapter.notifyDataSetChanged();

        /**
         * edited by mahin
         * this line represent total time for the model exam
         * so question number multiply by the time for answering each question
         * so question number multiply by singleQuestionAnswerTime variable
         */
        totalSec = adapter.mValues.size() * singleQuestionAnswerTime;

        startExam();
    }

    void startExam() {

        setTitle(getString(R.string.model_test));
        startTimeMonitoring();
        invalidateOptionsMenu();

    }

    void endExam() {
        //showRewardedAd();

        setTitle(getString(R.string.model_test) + " " + getString(R.string.done));
        totalSec = 0;
        invalidateOptionsMenu();
        handler.removeCallbacks(runnable);
        adapter.setModelTestDone();
        scoreBoardContainer.setVisibility(View.VISIBLE);

        int correctCount = 0;

        for (int i = 0; i < adapter.mValues.size(); i++) {

            OnlineModelTestRecyclerViewAdapter.CheckedMonitor monitor = adapter.checkedMonitorList.get(i);
            OnlineModelTestResponse.Mcq mcq = adapter.mValues.get(i);

            int answerIndex = 0;

            if (mcq.answer.equals(mcq.choice_one)) {

                answerIndex = 0;

            } else if (mcq.answer.equals(mcq.choice_two)) {

                answerIndex = 1;

            } else if (mcq.answer.equals(mcq.choice_three)) {

                answerIndex = 2;

            } else {

                answerIndex = 3;

            }

            log("From endExam() if(modelTest) : Question number is : " + i +
                    "answer is " + answerIndex + mcq.answer + "given answer is " + monitor.checkedPosition);
            if (answerIndex == monitor.checkedPosition) {
                correctCount++;
            }
        }

        log("correctCount is : " + correctCount);

        String correctStr = AppSingleTon.METHOD_BOX.convertNumericInBangla(correctCount + getString(R.string.ti));
        String wrongStr = AppSingleTon.METHOD_BOX.convertNumericInBangla((adapter.mValues.size() - correctCount) + getString(R.string.ti));

        scoreBoardTextCorrect.setText(correctStr);
        scoreBoardTextWrong.setText(wrongStr);

        int wrongAnswered = getQuestionAnswerd() - correctCount;

        String result = (correctCount == 0) ? "0" : AppSingleTon.METHOD_BOX.convertNumericInBangla((correctCount - ((wrongAnswered) * 0.5)) + "");
        scoreBoardTextResult.setText(result);
        scoreBoardTextOutOf.setText(AppSingleTon.METHOD_BOX.convertNumericInBangla(adapter.mValues.size() + ""));

    }

    void startTimeMonitoring() {

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                setTitle();
                totalSec--;
                if (totalSec > 0) {
                    handler.postDelayed(this, 1000);
                    return;
                }
                /*
                author goutom
                endExam();
                commented by mahin
                replaced by showRewardedAD()
                 */
                showRewardedAd();
            }
        };

        handler.post(runnable);
    }

    void setTitle() {
        String minutes = (int) (totalSec / 60) + "";
        String seconds = (int) (totalSec % 60) + "";
        String title = minutes + " " + getString(R.string.minute) + " " + seconds + " " + getString(R.string.second_left);

        if ((int) (totalSec / 60) == 0) {
            title = seconds + " " + getString(R.string.second_left);
        }
        actionBar.setTitle(title);
    }

    void setTitle(String finishMsg) {
        actionBar.setTitle(finishMsg);
    }

    /**
     * this method is created by mahin
     */
    public int getQuestionAnswerd() {
        int count = 0;
        int questionNo = 0;
        for (OnlineModelTestRecyclerViewAdapter.CheckedMonitor each : adapter.checkedMonitorList) {
            questionNo++;
            if (each.checkedPosition != -1) {
                count++;
            }

            log("From getQuestionAnswerd() : Question no : " + questionNo +
                    "number answer is " + each.checkedPosition + " count is " + count);
        }

        return count;
    }

    void setSubTitle() {

        int count = getQuestionAnswerd();
        String str = getString(R.string.number_of_mcq)
                + " " + AppSingleTon.METHOD_BOX.convertNumericInBangla(adapter.mValues.size() + "")
                + getString(R.string.ti) + " - " + getString(R.string.done) + " " + AppSingleTon.METHOD_BOX.convertNumericInBangla(count + "")
                + getString(R.string.ti);

        actionBar.setSubtitle(str);
    }


    @Override
    public void onListFragmentInteraction(int position) {
        setSubTitle();
    }


    boolean doubleBackToExitPressedOnce = false;

    void checkExamFinished() {


        if (adapter.isModelTestDone) {
            finish();
        } else if (getQuestionAnswerd() == adapter.mValues.size()) {
            showRewardedAd();
        } else if (totalSec > 0) {
            ContextThemeWrapper context = new ContextThemeWrapper(this, R.style.AlertDialogCustom);
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Quit ?");
            builder.setMessage(getString(R.string.exam_not_finished));
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    onBackOrHome();
                }
            });
            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
        } else {
            //here showRewardedAd() method call may
            //cause of further error
            showRewardedAd();
        }
    }


    @Override
    public void onBackPressed() {
        checkExamFinished();
    }

    void initAllAds() {
        initAdView();
        initInerestialAd();
        initRewardedAd();
    }

    void initRewardedAd() {
        MobileAds.initialize(this, "ca-app-pub-6027121519638799");
        mRewardedAd = MobileAds.getRewardedVideoAdInstance(this);
        loadRewardedVideoAd();
        mRewardedAd.setRewardedVideoAdListener(getRewardedVideoAdListener());
    }

    private void loadRewardedVideoAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mRewardedAd.loadAd(getString(R.string.admob_rewarded_ad_id),
                adRequest);
    }

    public void showRewardedAd() {
        if (mRewardedAd.isLoaded()) {
            mRewardedAd.show();
        } else {
            //Toast.makeText(this,"Cann't load rewarded video.",Toast.LENGTH_SHORT).show();
            endExam();
        }
    }

    @Override
    public void onResume() {
        mRewardedAd.resume(this);
        if ((examReward == 1)) {
            log("From Onresume Exam if() reward is " + examReward + "Model test done : " + adapter.isModelTestDone);
            endExam();
        }

        log("From Onresume Exam !if()Exam reward is " + examReward + "Model test done : " + adapter.isModelTestDone);
        super.onResume();

    }

    @Override
    public void onPause() {
        mRewardedAd.pause(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mRewardedAd.destroy(this);
        super.onDestroy();
    }


    private RewardedVideoAdListener getRewardedVideoAdListener() {
        RewardedVideoAdListener listener = new RewardedVideoAdListener() {
            @Override
            public void onRewarded(RewardItem reward) {
                //Toast.makeText(OnlineModelTestActivity.this, "onRewarded! currency: " + reward.getType() + "  amount: " +
                //reward.getAmount(), Toast.LENGTH_SHORT).show();
                if (reward.getAmount() == 1) {
                    examReward = 1;
                }
            }

            @Override
            public void onRewardedVideoAdLeftApplication() {
                //Toast.makeText(OnlineModelTestActivity.this, "onRewardedVideoAdLeftApplication",
                //Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoAdClosed() {
                //Toast.makeText(OnlineModelTestActivity.this, "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();
                if (examReward != 1)
                    onBackOrHome();
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int errorCode) {
                //Toast.makeText(OnlineModelTestActivity.this, "onRewardedVideoAdFailedToLoad "+errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoAdLoaded() {
                //Toast.makeText(OnlineModelTestActivity.this, "onRewardedVideoAdLoaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoAdOpened() {
                //Toast.makeText(OnlineModelTestActivity.this, "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoStarted() {
                //Toast.makeText(OnlineModelTestActivity.this, "onRewardedVideoStarted", Toast.LENGTH_SHORT).show();
            }


        };

        return listener;
    }

    private static void log(String msg) {
        Log.d(TAG, msg);
    }

    ///method for dealing with model test close hint close


    public void startInstructionTour(){
        TapTargetView.showFor(this,
                getTapTarget(getString(R.string.menu_button_name),getString(R.string.model_test_hints)),
                getTapTargetViewListener());
    }

    public TapTarget getTapTarget(String title, String description){
        return TapTarget.forToolbarOverflow(
                toolbar,title, description)
                .outerCircleColor(R.color.black)
                .outerCircleAlpha(.9f)
                .targetCircleColor(R.color.white)
                .dimColor(R.color.aluminum)
                .titleTextColor(R.color.white)
                .descriptionTextColor(R.color.white)
                .cancelable(false);
    }

    public TapTargetView.Listener getTapTargetViewListener(){
        return new TapTargetView.Listener(){
            @Override
            public void onTargetClick(TapTargetView view) {
                super.onTargetClick(view);
                SPHandler.setIsModelTestActivityInstructionTourDone(false);
            }
        };
    }
}

