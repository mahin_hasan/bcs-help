package codedrizzlers.com.bcsrelease.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.greendao.query.QueryBuilder;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswer;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswerDao;
import codedrizzlers.com.bcsrelease.database.QapSubSubject;
import codedrizzlers.com.bcsrelease.database.QapSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.QapSubject;
import codedrizzlers.com.bcsrelease.main.QapSubjectListFragment.OnSubjectListFragmentInteractionListener;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

import java.util.ArrayList;
import java.util.List;

public class QapSubjectRecyclerViewAdapter extends RecyclerView.Adapter<QapSubjectRecyclerViewAdapter.ViewHolder> {

    public ArrayList<QapSubject> mValues;
    public ArrayList<String> mSubtitleList;
    private final  OnSubjectListFragmentInteractionListener mListener;

    public QapSubjectRecyclerViewAdapter(ArrayList<QapSubject> items, OnSubjectListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        initSubtitleList();
        generaterSubtitleList();
    }

    void initSubtitleList() {
        mSubtitleList = new ArrayList<>(mValues.size()*2);
        for(int i=0;i<mValues.size()*2;i++){
            mSubtitleList.add(i,"");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_subject, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        String title = AppSingleTon.METHOD_BOX.convertNumericInBangla((position+1)+"");
        title = title+ ". " + mValues.get(position).getTitle();
        holder.title.setText(title);

        QapSubSubjectDao subSubjectDao = AppSingleTon.DB_MANAGER.daoSession.getQapSubSubjectDao();
        QueryBuilder<QapSubSubject> qb = subSubjectDao.queryBuilder();
        qb.where(QapSubSubjectDao.Properties.Subject_remote_id.eq(mValues.get(position).getRemote_id()));

        String sub = holder.mView.getContext().getString(R.string.sub_subject);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(qb.list().size()+"");
        sub = sub + holder.mView.getContext().getString(R.string.ti);
        sub = sub+ " - "+mSubtitleList.get(position);
        holder.subtitle.setText(sub);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(position);
                }
            }
        });
    }

    void generaterSubtitleList(){
        if(!(mSubtitleList.size() > mValues.size()))
            initSubtitleList();
        for(int i = 0; i < mValues.size();i++){
            mSubtitleList.set(i,generateNumOfDone(i,AppSingleTon.CONTEXT));
        }

    }
    String generateNumOfDone(int position, Context context) {

        int numOfDone = 0, listSize = 0;
        List<QapSubSubject> subSubjectList = AppSingleTon.DB_MANAGER.daoSession.getQapSubSubjectDao().queryBuilder()
                .where(QapSubSubjectDao.Properties.Subject_remote_id.eq(mValues.get(position).getRemote_id())).list();

        for(QapSubSubject each: subSubjectList){

            List<QapQuestionAnswer> qaList = AppSingleTon.DB_MANAGER.daoSession.getQapQuestionAnswerDao().queryBuilder()
                    .where(QapQuestionAnswerDao.Properties.Sub_subject_remote_id.eq(each.getRemote_id())).list();
            listSize+=qaList.size();
            for(QapQuestionAnswer qa : qaList){
                if(qa.getDone()){
                    numOfDone+=1;
                }
            }
        }
        return generateCompletePercentage(numOfDone, listSize, context);
    }

    String generateCompletePercentage(int numOfDone, int listSize, Context context){
        if(numOfDone == 0){
            String str = "0% "+ context.getString(R.string.complete);
            return str;
        }

        int complete = (numOfDone*100)/listSize;
        String str = AppSingleTon.METHOD_BOX.convertNumericInBangla(complete+"")+"% "+ context.getString(R.string.complete);
        return str;
    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView title, subtitle;
        public QapSubject mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            title = (TextView) view.findViewById(R.id.title);
            subtitle = (TextView) view.findViewById(R.id.sub_title);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + title.getText() + "'";
        }
    }
}
