package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.internet;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.text.InputType;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.others.PAlert.LikeRateFeedbackAlert;

/**
 * Created by code drizzlers on 11/5/2016.
 */

public class AccessNetworkJob {
    Context context;
    String mail = "";
    EditText input;

    public static boolean isNetworkConnected() {
        Boolean result = false;
        try {
            //get the result after executing AsyncTask
            result = new GetInternetStatus().execute().get();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean isNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public static boolean isInternetConnectionAvailable(int timeOut) {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = Executors.newSingleThreadExecutor().submit(new Callable<InetAddress>() {
                @Override
                public InetAddress call() {
                    try {
                        return InetAddress.getByName("google.com");
                    } catch (UnknownHostException e) {
                        return null;
                    }
                }
            });
            inetAddress = future.get(timeOut, TimeUnit.MILLISECONDS);
            future.cancel(true);
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
        } catch (TimeoutException e) {
        }
        return inetAddress!=null && !inetAddress.equals("");
    }

    public AccessNetworkJob(Context context){
        this.context = context;
    }

    public void goToPlayStroe(String appID) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        //Try Google play

        String marketUri = context.getString(R.string.app_play_store_appurl) + appID;
        String httpUri = context.getString(R.string.app_play_store_url) + appID;
        intent.setData(Uri.parse(marketUri));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (!startNewIntent(intent)) {
            //Market (Google play) app seems not installed, let's try to open a webbrowser
            intent.setData(Uri.parse(httpUri));
            if (!startNewIntent(intent)) {
                //Well if this also fails, we have run out of options, inform the user.
                Toast.makeText(context, "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean startNewIntent(Intent intent) {
        try {
            context.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    public void goToURL(String url){
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        url = checkAndAddHTTP(url);
        intent.setData(Uri.parse(url));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startNewIntent(intent);
    }

    private String checkAndAddHTTP(String url){
        if (!url.startsWith(context.getString(R.string.string_http)) && !url.startsWith(context.getString(R.string.string_https)))
            url = context.getString(R.string.string_http) + url;

        return url;
    }

    /**
     * sharing method
     */
    public void shareIt() {
        Intent sendIntent = new Intent();
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT,context.getString(R.string.app_name));
        sendIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.share_comment)+" "
                +context.getString(R.string.app_play_store_url)+
                context.getString(R.string.play_store_id)+context.getString(R.string.sahre_priview_extra));
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.share_app_title)));
    }

    /**
     * method for sending email with dialog
     * for getting content
     *
     * @return
     */
    public boolean showEmailDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(R.drawable.ic_feedback_black_18dp);
        builder.setTitle(R.string.send_feedback_title);

        // Set up the input
        input = new EditText(context);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        input.setHint(getString(R.string.feedback_hint));
        input.setLines(5);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(getString(R.string.feedback_send_button), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mail = input.getText().toString();
                Log.d("Mail Check", mail);
                sendEmail(mail);
            }
        });
        builder.setNegativeButton(getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        alertButtonTextColorChange(alertDialog,AlertDialog.BUTTON_POSITIVE,R.color.white);
        alertButtonTextColorChange(alertDialog,AlertDialog.BUTTON_NEGATIVE,R.color.white);

        return true;
    }

    public void alertButtonTextColorChange(AlertDialog alertDialog,int buttonid,int color){
        Button button = (Button)alertDialog.getButton(buttonid);
        button.setTextColor(context.getResources().getColor(color));
    }

    /**
     * sending method for email
     *
     * @param mail
     */
    public void sendEmail(String mail) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType("message/rfc822");
        intent.setData(Uri.parse(getString(R.string.feedback_email))); // only email apps should handle this
        //intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback from "+context.getString(R.string.app_name)+".");
        intent.putExtra(Intent.EXTRA_TEXT, mail);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.


        ComponentName emailApp = intent.resolveActivity(context.getPackageManager());
        ComponentName unsupportedAction = ComponentName.unflattenFromString("com.android.fallback/.Fallback");
        if (emailApp != null && !emailApp.equals(unsupportedAction))
            try {
                // Needed to customise the chooser dialog title since it might default to "Share with"
                // Note that the chooser will still be skipped if only one app is matched
                Intent chooser = Intent.createChooser(intent, "Send email with");
                context.startActivity(chooser);
                Toast
                        .makeText(context, "Feedback sent.", Toast.LENGTH_LONG)
                        .show();
                return;
            } catch (ActivityNotFoundException ignored) {
                Toast
                        .makeText(context, "Couldn't send email for some error.", Toast.LENGTH_LONG)
                        .show();
            }
        else {
            //Log.d("SendingMail", "Come to else of mail.");
            try {
                intent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                context.startActivity(intent);

            } catch (ActivityNotFoundException anfe) {
                Toast.makeText(context, "Couldn't find an email app and account", Toast.LENGTH_LONG)
                        .show();
            }

        }
    }

    public void moreApps(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        //Try Google play

        String marketUri = context.getString(R.string.more_apps_playstore_url)+context.getString(R.string.market_id);
        String httpUri = context.getString(R.string.more_apps_http_url)+context.getString(R.string.market_id);
        intent.setData(Uri.parse(marketUri));

        if (!startNewIntent(intent)) {
            //Market (Google play) app seems not installed, let's try to open a webbrowser
            intent.setData(Uri.parse(httpUri));
            if (!startNewIntent(intent)) {
                //Well if this also fails, we have run out of options, inform the user.
                Toast.makeText(context, "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getString(int stringId){
        return context.getString(stringId);
    }

}
