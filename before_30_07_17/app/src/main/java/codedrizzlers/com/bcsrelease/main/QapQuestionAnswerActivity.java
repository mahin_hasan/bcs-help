package codedrizzlers.com.bcsrelease.main;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.turingtechnologies.materialscrollbar.TouchScrollBar;
import java.util.ArrayList;
import java.util.List;
import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswer;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswerDao;
import codedrizzlers.com.bcsrelease.database.QapSubSubject;
import codedrizzlers.com.bcsrelease.database.RecyclerItemClickListener;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SPHandler;

public class QapQuestionAnswerActivity extends BaseActivity implements
        FilterDialogFragment.FilterFragmentInteractionListener {

    Toolbar toolbar;
    ActionBar actionBar;
    RecyclerView recyclerView;
    QapSubSubject subSubject;
    QapQuestionAnswerRecyclerViewAdapter adapter;
    List<QapQuestionAnswer> dataList;
    ImageView qapTutorialClose;
    RelativeLayout qapTutorial;

    int numOfDone = 0;
    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qap_question_answer);
        allInit();
    }

    void allInit(){
        initToolbar();
        initParse();
        initFastScrollBar();
        generateAll();
        initQapTutorial();

        initAdView();
        initInerestialAd();

        isInstructionTourDone();
    }

    private void isInstructionTourDone() {
        if(SPHandler.getIsQuestionAnswerActivityInstructionTourDone()){
            startInstructionTour();
        }
    }

    void initToolbar(){

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    void initParse(){

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            subSubject = (QapSubSubject) bundle.getSerializable(getString(R.string.sub_subject));
            position = bundle.getInt(getString(R.string.position), 0);
        }

        actionBar.setTitle(subSubject.getTitle());
    }

    void initQapTutorial(){

        qapTutorial = (RelativeLayout) findViewById(R.id.qap_tutorial);
        qapTutorialClose = (ImageView) findViewById(R.id.imageView_tutorial_close);
        qapTutorialClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppSingleTon.SHARED_PREFERENCE.setQapTutorial(true);
                qapTutorial.setVisibility(View.GONE);
            }
        });

        if(!AppSingleTon.SHARED_PREFERENCE.getQapTutorial()){
            qapTutorial.setVisibility(View.VISIBLE);
        }
    }

    public void setSubTitle(int size){
        String sub = getString(R.string.number_of_question);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(size+"");
        sub = sub + getString(R.string.ti);
        int complete = 0;
        //check size to prevent arithmatic exception divide by zero
        if(size>0)
            complete = (numOfDone*100)/size;
        sub = sub+ " - "+AppSingleTon.METHOD_BOX.convertNumericInBangla(complete+"")+"% "+ getString(R.string.complete);
        actionBar.setSubtitle(sub);
    }

    void initRecyclerView(){

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new QapQuestionAnswerRecyclerViewAdapter(dataList);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onDoubleClick(View view, int position) {
                adapter.mValues.get(position).setDone(!adapter.mValues.get(position).getDone());
                onListFragmentInteraction(adapter.mValues.get(position));
                adapter.notifyItemChanged(position);
            }

            @Override
            public void onSingleClick(View view, int position) {

            }

            @Override
            public void onLongPress(View view, int position) {

            }
        }));
    }

    void initFastScrollBar(){
        ((TouchScrollBar)findViewById(R.id.touchScrollBar))
                //.addIndicator(new CustomIndicator(this), true)
                .setHandleColour(ContextCompat.getColor(this, R.color.grey_300))
                .setHideDuration(500)
                .setTextColour(ContextCompat.getColor(this, R.color.monsoon))
                .setBarColour(ContextCompat.getColor(this, R.color.white));
    }

    void generateAll(){

        dataList = AppSingleTon.DB_MANAGER.daoSession.getQapQuestionAnswerDao().queryBuilder().where(QapQuestionAnswerDao.Properties.Sub_subject_remote_id.eq(subSubject.getRemote_id())).list();
        generateNumOfDone();
        setSubTitle(dataList.size());
        initRecyclerView();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);


    }

    void generateNumOfDone(){

        for(QapQuestionAnswer each : dataList){
            if(each.getDone()) {
                numOfDone += 1;
            }
        }
    }

    void resetAdapter(List<QapQuestionAnswer> dataList){

        adapter.mValues = dataList;
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == android.R.id.home){
            setResult();
            onBackOrHome();
        }else if(id == R.id.action_filter){
            startFilterDialog();
            return true;
        }else if(id == R.id.action_undone_all){
            startAllUnDoneDialog();
            return true;
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            setResult();
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    void setResult(){
        Intent intent = new Intent();
        intent.putExtra(getString(R.string.position), position);
        intent.putExtra(getString(R.string.num_of_done), numOfDone);
        setResult(Activity.RESULT_OK, intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter, menu);
        return true;
    }

    void startFilterDialog(){
        DialogFragment dialog = FilterDialogFragment.newInstance(dataList.size(), numOfDone, subSubject.getTitle());
        dialog.show(getSupportFragmentManager(), "FilterDialogFragment");
    }

    void startAllUnDoneDialog(){

        ContextThemeWrapper context = new ContextThemeWrapper(this, R.style.AlertDialogCustom);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(getString(R.string.all_undone_msg));
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                undoneAll();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor( ContextCompat.getColor(QapQuestionAnswerActivity.this, R.color.colorAccent));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor( ContextCompat.getColor(QapQuestionAnswerActivity.this, R.color.colorAccent));

            }
        });

        dialog.show();
    }

    void undoneAll(){

        for(int i=0;i<dataList.size();i++){
            dataList.get(i).setDone(false);
            AppSingleTon.DB_MANAGER.daoSession.getQapQuestionAnswerDao().update(dataList.get(i));
        }
        resetAdapter(dataList);
        numOfDone = 0;
        setSubTitle(dataList.size());
    }

    public void onListFragmentInteraction(QapQuestionAnswer item) {

        if(item.getDone() == false){
            if(numOfDone > 0) {
                numOfDone -= 1;
            }
            Snackbar.make(recyclerView, getString(R.string.undone_message), Snackbar.LENGTH_SHORT).show();
        }else{
            numOfDone+=1;
            Snackbar.make(recyclerView, getString(R.string.done_message), Snackbar.LENGTH_SHORT).show();
        }

        for(int i=0;i<dataList.size();i++){
            QapQuestionAnswer each = dataList.get(i);
            if(each.getRemote_id() == item.getRemote_id()){
                dataList.get(i).setDone(item.getDone());
                break;
            }
        }

        AppSingleTon.DB_MANAGER.daoSession.getQapQuestionAnswerDao().update(item);
        setSubTitle(dataList.size());

    }

    @Override
    public void onAllDoneSelected() {

        ArrayList<QapQuestionAnswer> tempList = new ArrayList<QapQuestionAnswer>();
        for(QapQuestionAnswer each : dataList){
            if(each.getDone()){
                tempList.add(each);
            }
        }
        resetAdapter(tempList);
    }

    @Override
    public void onAllUnDoneSelected() {

        ArrayList<QapQuestionAnswer> tempList = new ArrayList<QapQuestionAnswer>();
        for(QapQuestionAnswer each : dataList){
            if(!each.getDone()){
                tempList.add(each);
            }
        }
        resetAdapter(tempList);
    }

    @Override
    public void onAllSelected() {
        resetAdapter(dataList);
    }

    @Override
    public void onCustomSerial(int startIndex, int endIndex) {
        resetAdapter(dataList.subList(startIndex-1, endIndex));
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onBackOrHome();
    }

    public void startInstructionTour(){
        TapTargetView.showFor(this,
                getTapTarget(getString(R.string.menu_button_name),getString(R.string.qap_activity_hints)),
                getTapTargetViewListener());
    }

    public TapTarget getTapTarget(String title, String description){
        return TapTarget.forToolbarOverflow(
                toolbar,title, description)
                .outerCircleColor(R.color.black)
                .outerCircleAlpha(.9f)
                .targetCircleColor(R.color.white)
                .dimColor(R.color.aluminum)
                .titleTextColor(R.color.white)
                .descriptionTextColor(R.color.white)
                .cancelable(false);
    }

    public TapTargetView.Listener getTapTargetViewListener(){
        return new TapTargetView.Listener(){
            @Override
            public void onTargetClick(TapTargetView view) {
                super.onTargetClick(view);
                SPHandler.setIsQuestionAnswerActivityInstructionTourDone(false);
            }
        };
    }
}
