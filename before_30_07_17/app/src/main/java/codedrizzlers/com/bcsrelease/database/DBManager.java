package codedrizzlers.com.bcsrelease.database;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import org.greenrobot.greendao.database.Database;

import codedrizzlers.db.generator.DBFields;


/**
 * Created by goutom roy on 7/6/2016.
 */

public class DBManager {
    private final String DB_PASSWORD = "code_DRIZZLERS_#_123_bcs_help";
    /**
     * Class tag. Used for debug.
     */
    private static final String TAG = DBManager.class.getCanonicalName();
    /**
     * Instance of DatabaseManager
     */
    public static DBManager instance;
    public Context context;
    public DaoMaster.DevOpenHelper mHelper;
    public Database database;
    public DaoMaster daoMaster;
    public DaoSession daoSession;



    public DBManager(Context context){
        this.context = context;
        mHelper = new DaoMaster.DevOpenHelper(this.context, DBFields.DATABASE_NAME, null);

    }

    public static DBManager getInstance(Context context) {

        if (instance == null) {
            instance = new DBManager(context);
        }
        return instance;
    }

    public void openReadableDb() throws SQLiteException {
        database = mHelper.getReadableDb();
        daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
    }

    public void openWritableDb() throws SQLiteException {

        database = mHelper.getWritableDb();
        daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
        daoSession.clear();
    }

    public void closeDbConnections() {

        if (daoSession != null) {
            daoSession.clear();
            daoSession = null;
        }

        if (database != null && !database.inTransaction()) {
            database.close();
        }

        if (mHelper != null) {
            mHelper.close();
            mHelper = null;
        }

        if (instance != null) {
            instance = null;
        }

    }

}
