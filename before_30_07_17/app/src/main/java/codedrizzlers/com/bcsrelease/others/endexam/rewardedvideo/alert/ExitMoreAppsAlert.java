package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.alert;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.main.MainActivity;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.helper.MoreAppsXmlParser;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.internet.AccessNetworkJob;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.internet.GetInputStreamFromWeb;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.internet.MoreAppsImageSettingTask;

/**
 * Created by code drizzlers on 11/6/2016.
 */

public class ExitMoreAppsAlert {

    Context context;

    AlertDialog alert;

    public ExitMoreAppsAlert(Context context) {
        this.context = context;
    }

    private List readMoreAppsXML(InputStream in) {
        MoreAppsXmlParser parser = new MoreAppsXmlParser();
        try {
            final List apps = parser.parse(in);
            Log.d("MoreApps", apps.get(0).toString());
            return apps;
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (android.os.NetworkOnMainThreadException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    private InputStream getXMLFromNet() {
        try {

            return new GetInputStreamFromWeb().execute(context.getString(R.string.exti_more_apps_alert_url)).get();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private MoreAppsXmlParser.App getApp(List apps) {
        try {
            MoreAppsXmlParser.App app = (MoreAppsXmlParser.App) apps.get(0);

            if (app != null) {
                Log.d("GetNewApp", "Package name is : " + context.getPackageName());
                Log.d("GetNewApp", "App PlayStore URL is : " + app.playStoreID);
                Log.d("GetNewApp", "App descriptoin is : " + app.description);
                if (!context.getPackageName().equalsIgnoreCase(app.playStoreID))
                    return app;
                else
                    return (MoreAppsXmlParser.App) apps.get(1);
            } else
                exit();
        } catch (NullPointerException e) {
            e.printStackTrace();
            exit();
        }

        return null;
    }

    private MoreAppsXmlParser.App getApp() {
        InputStream in = getXMLFromNet();
        Log.d("ExiAlertLayout", in.toString());
        return processAppOrExit(in);
    }

    private MoreAppsXmlParser.App processAppOrExit(InputStream in) {
        if (in != null)
            return createApp(in);
        else
            exit();

        return null;
    }

    private MoreAppsXmlParser.App createApp(InputStream in) {
        List apps = readMoreAppsXML(in);
        return getApp(apps);
    }

    private void goToAppPlayStore(String playStoreId) {
        AccessNetworkJob job = new AccessNetworkJob(context);
        job.goToPlayStroe(playStoreId);
    }

    private void initializeOrExitAlert(View alertView,
                                       MoreAppsXmlParser.App app) {
        if (app != null) {
            Log.d("ExitAppAlert", "Program enter to initializeOrExitAlert() if{}.");
            intializeMoreAppAlertViewContent(alertView, app);
        } else {
            Log.d("ExitAppAlert", "Program comes to initializeOrExitAler else{program going to exit}.");
            exit();
        }
    }

    public void showExitMoreAppsAlertDialog() {
        try{
        MoreAppsXmlParser.App app = getApp();

        if ((app != null))
            alert = creatAlert();

        View alertView = createAlertLayoutView();

        alert.setView(alertView);

        initializeOrExitAlert(alertView, app);

        showAlert();}
        catch (Exception e){
            exit();
        }
    }

    private void showAlert() {
        try {
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
            exit();
        }
    }

    private AlertDialog creatAlert() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        AlertDialog alert = dialogBuilder.create();

        return alert;
    }

    private View createAlertLayoutView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alertView = inflater.inflate(R.layout.exit_more_apps_alert_layout, null);

        return alertView;
    }


    private void setMoreAppsAlertImageViewThroughTask(String path, ImageView view) {
        try {
            MoreAppsImageSettingTask task = new MoreAppsImageSettingTask();
            task.setImageView(view);
            task.execute(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void initializeMoreAppsAlertImageView(View view, String iconUrl) {
        ImageView appIcon = (ImageView) view.findViewById(R.id.new_app_icon_logo);
        setMoreAppsAlertImageViewThroughTask(iconUrl, appIcon);
    }

    private void initializeTextView(View view, int textViewId, String text) {
        TextView textView = (TextView) view.findViewById(textViewId);
        Log.d("ExitOnInitText", text);
        textView.setText(text);
    }

    private void setRatingBarColor(RatingBar ratingBar) {
        Drawable drawable = ratingBar.getProgressDrawable();
        drawable.setColorFilter(Color.parseColor(context.getString(R.string.exti_more_apps_alert_ratingbar_color)),
                PorterDuff.Mode.SRC_ATOP);
    }

    private void initializeRatingBar(View view, MoreAppsXmlParser.App app) {
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.new_app_ratingbar);

        ratingBar.setRating(app.rating);
        //Log.d("APPRating", "new app rating : " + app.rating);

        setRatingBarColor(ratingBar);
    }

    private void setButtonListener(View view, int buttonId, View.OnClickListener listener) {
        Button button = (Button) view.findViewById(buttonId);
        button.setOnClickListener(listener);
    }

    private void selectButtonAction(int buttonId) {
        switch (buttonId) {
            case R.id.new_app_alert_exit_button:
                exit();
                break;
            case R.id.new_app_alert_cancel_button:
                alert.dismiss();
                break;
        }
    }

    private void initializeButton(View view, final int buttonId) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectButtonAction(buttonId);
            }
        };

        setButtonListener(view, buttonId, listener);
    }


    private void intializeMoreAppAlertViewContent(View alertView,
                                                  final MoreAppsXmlParser.App app) {
        initializeMoreAppsAlertImageView(alertView, app.iconUrl);

        initRootLayout(alertView, app.playStoreID);

        initializeTextView(alertView, R.id.new_app_name, app.name);

        initializeRatingBar(alertView, app);

        initializeTextView(alertView, R.id.new_app_description, app.description);

        initializeButton(alertView, R.id.new_app_alert_exit_button);

        initializeButton(alertView, R.id.new_app_alert_cancel_button);

        initializeGooglePlayButton(alertView, app.playStoreID);

    }

    private void initRootLayout(View alertView, String playStoreID) {
        LinearLayout layout = (LinearLayout) alertView.findViewById(R.id.exit_root_linearlayout);
        layout.setOnClickListener(getGooglePlayListener(playStoreID));
    }

    private void initializeGooglePlayButton(View alertView, String playStoreId) {
        LinearLayout googlePlay = (LinearLayout) alertView.findViewById(R.id.new_app_get_on_google_paly_button);
        setGooglePlayButtonListener(googlePlay, playStoreId);
    }

    private void setGooglePlayButtonListener(LinearLayout googlePlay, String playStoreId) {
        View.OnClickListener listener = getGooglePlayListener(playStoreId);
        googlePlay.setOnClickListener(listener);
    }

    private View.OnClickListener getGooglePlayListener(final String playStoreId) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
                goToAppPlayStore(playStoreId);
            }
        };

        return listener;
    }

    private void finallyClosedAlert() {
        if (alert != null) {
            try {
                alert.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void exit() {
        Activity activity = (Activity) context;

        finallyClosedAlert();

        if (activity instanceof Activity) {
            MainActivity mainActivity = (MainActivity) context;
            mainActivity.finish();
        }
    }
}
