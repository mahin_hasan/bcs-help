package codedrizzlers.com.bcsrelease.main;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;


public class FilterDialogFragment extends DialogFragment {


    int numberOfQuestion = 0;
    int numOfDone = 0;
    String title = "";
    EditText from, to;
    FilterFragmentInteractionListener mListener;
    public FilterDialogFragment() {

    }

    public static FilterDialogFragment newInstance(int num, int numOfDone, String title) {
        FilterDialogFragment f = new FilterDialogFragment();
        Bundle args = new Bundle();
        args.putInt("num", num);
        args.putInt("num_of_done", numOfDone);
        args.putString("title", title);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            numberOfQuestion = args.getInt("num");
            numOfDone = args.getInt("num_of_done");
            title = args.getString("title");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FilterFragmentInteractionListener) {
            mListener = (FilterFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ContextThemeWrapper context = new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustom);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_dialog_filter, null);

        from = (EditText) view.findViewById(R.id.editText_from);
        to = (EditText) view.findViewById(R.id.editText_to);

        final RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);

        RadioButton radioButtonDone = (RadioButton) view.findViewById(R.id.radioButton_done);
        String tempDoneText = getString(R.string.done)+" ("+numOfDone+" "+getString(R.string.ti)+")";
        radioButtonDone.setText(AppSingleTon.METHOD_BOX.convertNumericInBangla(tempDoneText));

        RadioButton radioButtonUnDone = (RadioButton) view.findViewById(R.id.radioButton_undone);
        String tempUnDoneText = getString(R.string.undone)+" ("+(numberOfQuestion-numOfDone)+" "+getString(R.string.ti)+")";
        radioButtonUnDone.setText(AppSingleTon.METHOD_BOX.convertNumericInBangla(tempUnDoneText));

        RadioButton radioButtonAllQuestion = (RadioButton) view.findViewById(R.id.radioButton_all_question);
        String tempAllQuestionText = getString(R.string.all_question)+" ("+numberOfQuestion+" "+getString(R.string.ti)+")";
        radioButtonAllQuestion.setText(AppSingleTon.METHOD_BOX.convertNumericInBangla(tempAllQuestionText));

        final LinearLayoutCompat customNumbering = (LinearLayoutCompat) view.findViewById(R.id.custom_numbering);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.radioButton_done){
                    customNumbering.setVisibility(View.GONE);
                }else if(checkedId == R.id.radioButton_undone){
                    customNumbering.setVisibility(View.GONE);
                }else if(checkedId == R.id.radioButton_all_question){
                    customNumbering.setVisibility(View.GONE);
                }else if(checkedId == R.id.radioButton_custom){
                    customNumbering.setVisibility(View.VISIBLE);
                }
            }
        });

        from.setHint(getActivity().getString(R.string.question) + AppSingleTon.METHOD_BOX.convertNumericInBangla(" 1"));
        to.setHint(AppSingleTon.METHOD_BOX.convertNumericInBangla(""+numberOfQuestion));

        builder.setTitle(title);
        builder.setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        int checkedId = radioGroup.getCheckedRadioButtonId();

                        if(checkedId == R.id.radioButton_done){
                            mListener.onAllDoneSelected();

                        }else if(checkedId == R.id.radioButton_undone){
                            mListener.onAllUnDoneSelected();

                        }else if(checkedId == R.id.radioButton_all_question){
                            mListener.onAllSelected();

                        }else if(checkedId == R.id.radioButton_custom){

                            String strFrom = from.getText().toString();
                            strFrom = strFrom.equals("") ? "1" : strFrom;
                            String strTo = to.getText().toString();
                            strTo = strTo.equals("") ? numberOfQuestion+"" : strTo;

                            mListener.onCustomSerial(Integer.parseInt(strFrom), Integer.parseInt(strTo));
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor( ContextCompat.getColor(getActivity(), R.color.colorAccent));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor( ContextCompat.getColor(getActivity(), R.color.colorAccent));

            }
        });
        return dialog;
    }

    public interface FilterFragmentInteractionListener {
        void onAllDoneSelected();
        void onAllUnDoneSelected();
        void onAllSelected();
        void onCustomSerial(int startIndex, int endIndex);
    }
}
