package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.overflowmenu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.main.MainActivity;

/**
 * Created by code drizzlers on 4/2/2017.
 */

public class AboutUsAlert {
    private static final String TAG = "AboutUsAlert";

    MainActivity activity;

    public AboutUsAlert(MainActivity activity) {
        this.activity = activity;
    }

    public void showAlert(){
        AlertDialog alert = createAboutUsDialog();
        alert.show();
        changeOkTextColor(alert);
        textViewHyperLinkSetup(alert);
    }

    private AlertDialog createAboutUsDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        return aboutUsDialogViewSetting(builder);
    }

    private AlertDialog aboutUsDialogViewSetting(AlertDialog.Builder builder) {
        builder.setIcon(R.drawable.ic_people_black_18dp);
        return aboutUsDialogButtonInitialize(builder);
    }

    private AlertDialog aboutUsDialogButtonInitialize(AlertDialog.Builder builder) {
        // Linkify the message
        SpannableString aboutUsMsg = new SpannableString(activity.getString(R.string.us_details));
        Linkify.addLinks(aboutUsMsg, Linkify.ALL);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
            }
        });
        builder.setMessage(aboutUsMsg)
                .setTitle("  ");

        return builder.create();
    }


    private void changeOkTextColor(AlertDialog alertDialog) {
        Button ok = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        ok.setTextColor(activity.getResources().getColorStateList(R.color.white));
    }

    private void textViewHyperLinkSetup(AlertDialog alertDialog){
        TextView textView = (TextView)alertDialog.findViewById(android.R.id.message);
        textView.setLinkTextColor(activity.getResources().getColor(R.color.jumbo));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private static void log(String msg){
        Log.d(TAG,msg);
    }
}
