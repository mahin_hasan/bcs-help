package codedrizzlers.com.bcsrelease.main;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.turingtechnologies.materialscrollbar.ICustomAdapter;
import java.util.ArrayList;
import java.util.List;
import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.json.ResponseMapper.OnlineModelTestResponse;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class OnlineModelTestRecyclerViewAdapter extends RecyclerView.Adapter<OnlineModelTestRecyclerViewAdapter.ViewHolder> implements ICustomAdapter{

    public List<OnlineModelTestResponse.Mcq> mValues;
    public ArrayList<CheckedMonitor> checkedMonitorList;
    private final FragmentInteractionListener mListener;
    public boolean isModelTestDone = false;

    public OnlineModelTestRecyclerViewAdapter(List<OnlineModelTestResponse.Mcq> items, FragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    public void initCheckedMonitorList(){

        checkedMonitorList = new ArrayList<CheckedMonitor>();
        Log.d("size fir", checkedMonitorList.size()+"");
        for(int i=0;i<mValues.size();i++){
            CheckedMonitor checkedMonitor = new CheckedMonitor();
            checkedMonitorList.add(checkedMonitor);
        }
    }

    public void setModelTestDone(){
        isModelTestDone = true;
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_model_test_mcq, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.mItem = mValues.get(position);

        String title = AppSingleTon.METHOD_BOX.convertNumericInBangla((position+1)+"");
        title = title+ ". " + mValues.get(position).question;

        holder.question.setText(title);
        holder.choiceOne.setText(holder.mItem.choice_one);
        holder.choiceTwo.setText(holder.mItem.choice_two);
        holder.choiceThree.setText(holder.mItem.choice_three);
        holder.choiceFour.setText(holder.mItem.choice_four);

        holder.radioGroup.setTag(position);
        holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                int position = (int) group.getTag();
                int index = group.indexOfChild(group.findViewById(group.getCheckedRadioButtonId()));
                checkedMonitorList.get(position).checkedPosition = index;
                mListener.onListFragmentInteraction(position);

            }
        });

        if(isModelTestDone){

            //this is need to set the text color to black
            holder.choiceOne.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.black ));
            holder.choiceTwo.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.black ));
            holder.choiceThree.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.black ));
            holder.choiceFour.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.black ));

            String answer = holder.mItem.answer;

            if (answer.equals(holder.mItem.choice_one)) {
                holder.choiceOne.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.teal_500 ));

            } else if (answer.equals(holder.mItem.choice_two)) {
                holder.choiceTwo.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.teal_500 ));

            } else if (answer.equals(holder.mItem.choice_three)) {
                holder.choiceThree.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.teal_500 ));

            } else {
                holder.choiceFour.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.teal_500 ));
            }

            checkedRadiGroup(holder,position);

            holder.choiceOne.setEnabled(false);
            holder.choiceTwo.setEnabled(false);
            holder.choiceThree.setEnabled(false);
            holder.choiceFour.setEnabled(false);
        }
        /**
         * edited by mahin
         * this else is for preventing auto checked radio button
         */
        else{
            checkedRadiGroup(holder,position);
        }
    }

    /*this method is created by mahin*/
    public void checkedRadiGroup(ViewHolder holder, int position){
        int checkedPosition = checkedMonitorList.get(position).checkedPosition;
        //if checkedPosition is 0 to 3 then assume user answer it earlier
        if((checkedPosition>-1)&&(checkedPosition<4)){
            ((RadioButton)holder.radioGroup.getChildAt(checkedPosition)).setChecked(true);
        }
        //if checkedPosition is -1 or greater than 3 then user did not answer it earlier
        else
            holder.radioGroup.clearCheck();
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public String getCustomStringForElement(int element) {
        return ""+element;
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView question;
        public OnlineModelTestResponse.Mcq mItem;
        public RadioGroup radioGroup;
        public RadioButton choiceOne, choiceTwo, choiceThree, choiceFour;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            question = (TextView) view.findViewById(R.id.question);
            radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
            choiceOne = (RadioButton) view.findViewById(R.id.radioButton_choice_one);
            choiceTwo = (RadioButton) view.findViewById(R.id.radioButton_choice_two);
            choiceThree = (RadioButton) view.findViewById(R.id.radioButton_choice_three);
            choiceFour = (RadioButton) view.findViewById(R.id.radioButton_choice_four);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + question.getText() + "'";
        }
    }

    public interface FragmentInteractionListener {
        void onListFragmentInteraction(int position);
    }

    public class CheckedMonitor{
        public int checkedPosition = -1;
    }
}
