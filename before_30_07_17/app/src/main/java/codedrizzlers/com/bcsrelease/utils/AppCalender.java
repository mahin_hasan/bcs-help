package codedrizzlers.com.bcsrelease.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AppCalender {
	
	//private String[] months = {"January","February","March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	//month starts from January = 0
	//sunday is 1 and saturday is 7
	//hour starts 0 to 11
	
	private String[] days = {"null", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
	
	
	public CalendarBundle getDefualt(){
		
		CalendarBundle bundle = new CalendarBundle();
		Calendar calendar = Calendar.getInstance(Locale.getDefault());
		
		bundle.timeinSeconds = String.valueOf(calendar.getTimeInMillis() / 1000);
		bundle.year = String.valueOf(calendar.get(Calendar.YEAR));
		bundle.month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		bundle.dayOfTheMonth = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		bundle.dayOfTheWeek = days[calendar.get(Calendar.DAY_OF_WEEK)];
		
		int tempHour = calendar.get(Calendar.HOUR);
		bundle.hour = (tempHour== 0)? "12": String.valueOf(tempHour);
		
		bundle.minute = String.valueOf(calendar.get(Calendar.MINUTE));
		
		int tempAmPm = calendar.get(Calendar.AM_PM);
		bundle.amPm = (tempAmPm == 0) ? "AM":"PM";
		
		return bundle;
	}

	public Calendar getCalendar(String time) {
		Calendar calendar = Calendar.getInstance();
		String format = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
		SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
		try {
			calendar.setTime(sdf.parse(time));

			return calendar;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	public CalendarBundle getTime(long timeInMS){

		CalendarBundle bundle = new CalendarBundle();
		Calendar calendar = Calendar.getInstance(Locale.getDefault());
		calendar.setTimeInMillis(timeInMS);

		bundle.timeinSeconds = String.valueOf(calendar.getTimeInMillis() / 1000);
		bundle.year = String.valueOf(calendar.get(Calendar.YEAR));
		bundle.month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		bundle.dayOfTheMonth = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		bundle.dayOfTheWeek = days[calendar.get(Calendar.DAY_OF_WEEK)];

		int tempHour = calendar.get(Calendar.HOUR);
		bundle.hour = (tempHour== 0)? "12": String.valueOf(tempHour);

		bundle.minute = String.valueOf(calendar.get(Calendar.MINUTE));

		int tempAmPm = calendar.get(Calendar.AM_PM);
		bundle.amPm = (tempAmPm == 0) ? "AM":"PM";

		return bundle;
	}

	public class CalendarBundle{
		public String timeinSeconds,timeInMSeconds, year, month, dayOfTheMonth, dayOfTheWeek, hour, minute, amPm;
	}
}
