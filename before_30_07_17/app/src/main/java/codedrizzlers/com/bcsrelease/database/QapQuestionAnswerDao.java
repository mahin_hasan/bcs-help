package codedrizzlers.com.bcsrelease.database;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.SqlUtils;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "QAP_QUESTION_ANSWER".
*/
public class QapQuestionAnswerDao extends AbstractDao<QapQuestionAnswer, Long> {

    public static final String TABLENAME = "QAP_QUESTION_ANSWER";

    /**
     * Properties of entity QapQuestionAnswer.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Remote_id = new Property(0, Long.class, "remote_id", true, "REMOTE_ID");
        public final static Property Sub_subject_remote_id = new Property(1, long.class, "sub_subject_remote_id", false, "SUB_SUBJECT_REMOTE_ID");
        public final static Property Done = new Property(2, Boolean.class, "done", false, "DONE");
        public final static Property Question = new Property(3, String.class, "question", false, "QUESTION");
        public final static Property Answer = new Property(4, String.class, "answer", false, "ANSWER");
    }

    private DaoSession daoSession;


    public QapQuestionAnswerDao(DaoConfig config) {
        super(config);
    }
    
    public QapQuestionAnswerDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"QAP_QUESTION_ANSWER\" (" + //
                "\"REMOTE_ID\" INTEGER PRIMARY KEY ," + // 0: remote_id
                "\"SUB_SUBJECT_REMOTE_ID\" INTEGER NOT NULL ," + // 1: sub_subject_remote_id
                "\"DONE\" INTEGER," + // 2: done
                "\"QUESTION\" TEXT," + // 3: question
                "\"ANSWER\" TEXT);"); // 4: answer
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"QAP_QUESTION_ANSWER\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, QapQuestionAnswer entity) {
        stmt.clearBindings();
 
        Long remote_id = entity.getRemote_id();
        if (remote_id != null) {
            stmt.bindLong(1, remote_id);
        }
        stmt.bindLong(2, entity.getSub_subject_remote_id());
 
        Boolean done = entity.getDone();
        if (done != null) {
            stmt.bindLong(3, done ? 1L: 0L);
        }
 
        String question = entity.getQuestion();
        if (question != null) {
            stmt.bindString(4, question);
        }
 
        String answer = entity.getAnswer();
        if (answer != null) {
            stmt.bindString(5, answer);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, QapQuestionAnswer entity) {
        stmt.clearBindings();
 
        Long remote_id = entity.getRemote_id();
        if (remote_id != null) {
            stmt.bindLong(1, remote_id);
        }
        stmt.bindLong(2, entity.getSub_subject_remote_id());
 
        Boolean done = entity.getDone();
        if (done != null) {
            stmt.bindLong(3, done ? 1L: 0L);
        }
 
        String question = entity.getQuestion();
        if (question != null) {
            stmt.bindString(4, question);
        }
 
        String answer = entity.getAnswer();
        if (answer != null) {
            stmt.bindString(5, answer);
        }
    }

    @Override
    protected final void attachEntity(QapQuestionAnswer entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public QapQuestionAnswer readEntity(Cursor cursor, int offset) {
        QapQuestionAnswer entity = new QapQuestionAnswer( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // remote_id
            cursor.getLong(offset + 1), // sub_subject_remote_id
            cursor.isNull(offset + 2) ? null : cursor.getShort(offset + 2) != 0, // done
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // question
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4) // answer
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, QapQuestionAnswer entity, int offset) {
        entity.setRemote_id(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setSub_subject_remote_id(cursor.getLong(offset + 1));
        entity.setDone(cursor.isNull(offset + 2) ? null : cursor.getShort(offset + 2) != 0);
        entity.setQuestion(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setAnswer(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(QapQuestionAnswer entity, long rowId) {
        entity.setRemote_id(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(QapQuestionAnswer entity) {
        if(entity != null) {
            return entity.getRemote_id();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(QapQuestionAnswer entity) {
        return entity.getRemote_id() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getQapSubSubjectDao().getAllColumns());
            builder.append(" FROM QAP_QUESTION_ANSWER T");
            builder.append(" LEFT JOIN QAP_SUB_SUBJECT T0 ON T.\"SUB_SUBJECT_REMOTE_ID\"=T0.\"REMOTE_ID\"");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected QapQuestionAnswer loadCurrentDeep(Cursor cursor, boolean lock) {
        QapQuestionAnswer entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        QapSubSubject qapSubSubject = loadCurrentOther(daoSession.getQapSubSubjectDao(), cursor, offset);
         if(qapSubSubject != null) {
            entity.setQapSubSubject(qapSubSubject);
        }

        return entity;    
    }

    public QapQuestionAnswer loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<QapQuestionAnswer> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<QapQuestionAnswer> list = new ArrayList<QapQuestionAnswer>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<QapQuestionAnswer> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<QapQuestionAnswer> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
