package codedrizzlers.com.bcsrelease.utils;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;

import com.loopj.android.http.AsyncHttpClient;

import codedrizzlers.com.bcsrelease.database.DBManager;
import codedrizzlers.com.bcsrelease.json.AppJsonParser;

public class AppSingleTon extends Application {

	public static Context CONTEXT;
	public static DBManager DB_MANAGER;
	public static Resources RESOURCES;
	public static LayoutInflater LAYOUT_INFLATER;
	public static AppPreference SHARED_PREFERENCE;
	public static MethodBox METHOD_BOX;
	public static AppUrl APP_URL;
	public static AppJsonParser APP_JSON_PARSER;
	public  static AppCalender APP_CALENDAR;
	public static SDCardManager SD_CARD_MANAGER;
	public static AsyncHttpClient ASYNC_HTTP_CLIENT;
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		initStaticObject();
		initSD();
	}

	
	public void initStaticObject() {
		
		CONTEXT = getApplicationContext();
		RESOURCES = CONTEXT.getResources();
		APP_URL = new AppUrl();
		APP_JSON_PARSER = new AppJsonParser();


		Log.d("initStaticObject", "initStaticObject");
		//Toast.makeText(CONTEXT, "initStaticObject", Toast.LENGTH_LONG).show();
		METHOD_BOX = new MethodBox();
		APP_CALENDAR = new AppCalender();
		SHARED_PREFERENCE = new AppPreference();
		LAYOUT_INFLATER = (LayoutInflater)CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		SD_CARD_MANAGER =  new SDCardManager();
		ASYNC_HTTP_CLIENT = new AsyncHttpClient();
		ASYNC_HTTP_CLIENT.setBasicAuth("codedrizzlers", "code2016");
		ASYNC_HTTP_CLIENT.setTimeout(30000);
	}

	void initSD(){
		SD_CARD_MANAGER.createFolder(SD_CARD_MANAGER.sd_card_mother_folder);
	}

}
