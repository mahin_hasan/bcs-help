package codedrizzlers.com.bcsrelease.service.category;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswer;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswerDao;
import codedrizzlers.com.bcsrelease.json.ResponseMapper;
import codedrizzlers.com.bcsrelease.service.SyncService;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SPHandler;
import cz.msebera.android.httpclient.Header;

/**
 * Created by code drizzlers on 6/19/2017.
 */

public class PreparationQuestionSyncService extends Service {
    public static final String TAG_HTTP_QUESTION_ANSWER_SYNC = "TAG_HTTP_QUESTION_ANSWER_SYNC";

    public static final String ACTION_SYNC_PREPARATION_QUESTION = "ACTION_PREPARATION_QESTION_SYNC_DONE";

    @Override
    public void onCreate() {
        super.onCreate();
        questionAnswerSync(1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void questionAnswerSync(int page) {

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.d("preparation onSuccess", "syncHeaders :" + response.toString());
                questionAnswerSyncOnSuccess(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("preparation onFailure", "syncHeaders :" + errorResponse + "");
                Intent intent = new Intent(SyncService.FILTER_ACTION_SYNC_ERROR);
                sendBroadcast(intent);
                stopSelf();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        handler.setTag(TAG_HTTP_QUESTION_ANSWER_SYNC);
        RequestParams params = new RequestParams();
        params.put(getString(R.string.page), page);
        params.put(getString(R.string.last_scanned_date), SPHandler.getPreparationQuestionAnswerScannedDate());
        AppSingleTon.ASYNC_HTTP_CLIENT.post(PreparationQuestionSyncService.this, AppSingleTon.APP_URL.SYNC_QUESTION_ANSWER, params, handler);
    }

    void questionAnswerSyncOnSuccess(final JSONObject response) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ResponseMapper.QuestionAnswerResponse syncResponse = AppSingleTon.APP_JSON_PARSER.syncQuestionAnswerResponse(response);
                pushToQapQuestionAnswer(syncResponse.results);

                if (syncResponse.next_page > 0) {
                    questionAnswerSync(syncResponse.next_page);
                }
                else {
                    stopService(syncResponse);
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);

    }

    void pushToQapQuestionAnswer(final ArrayList<ResponseMapper.QuestionAnswerResponse.QuestionAnswer> questionAnswers) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                QapQuestionAnswerDao questionAnswerDao = AppSingleTon.DB_MANAGER.daoSession.getQapQuestionAnswerDao();

                for (ResponseMapper.QuestionAnswerResponse.QuestionAnswer questionAnswer : questionAnswers) {

                    QapQuestionAnswer checkQuestionAnswerDb = questionAnswerDao.load(questionAnswer.id);

                    if (checkQuestionAnswerDb == null) {

                        QapQuestionAnswer questionAnswerDb = new QapQuestionAnswer(questionAnswer.id,
                                questionAnswer.sub_subject, questionAnswer.is_deleted,
                                questionAnswer.question, questionAnswer.answer);
                        questionAnswerDao.insert(questionAnswerDb);

                    } else {

                        if (questionAnswer.is_deleted == true) {
                            questionAnswerDao.deleteByKey(questionAnswer.id);
                        } else {
                            checkQuestionAnswerDb.setSub_subject_remote_id(questionAnswer.sub_subject);
                            checkQuestionAnswerDb.setAnswer(questionAnswer.answer);
                            checkQuestionAnswerDb.setQuestion(questionAnswer.question);
                            questionAnswerDao.update(checkQuestionAnswerDb);
                        }
                    }

                    Log.d("InsertPre","Still inserting Preparation question. question number "+questionAnswer.id);

                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void stopService(ResponseMapper.QuestionAnswerResponse syncResponse){
        SPHandler.setPreparationQuestionAnswerScannedDate(syncResponse.last_scanned_date);
        Intent intent = new Intent(PreparationQuestionSyncService.ACTION_SYNC_PREPARATION_QUESTION);
        sendBroadcast(intent);
        stopSelf();
    }

}
