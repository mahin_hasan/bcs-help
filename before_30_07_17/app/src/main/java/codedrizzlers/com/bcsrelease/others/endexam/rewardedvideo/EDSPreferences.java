package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;


import codedrizzlers.com.bcsrelease.json.ResponseMapper;
import codedrizzlers.com.bcsrelease.main.OnlineModelTestActivity;
import codedrizzlers.com.bcsrelease.main.OnlineModelTestRecyclerViewAdapter;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.Utils.SharedPreferencesUtils;

/**
 * Created by code drizzlers on 3/31/2017.
 */
//class name short for Exam Data Shared Preferences
public class EDSPreferences {
    public static final String TAG = "EDSPreferences";

    private String sharedPreferencesFile = "prefs_file";

    OnlineModelTestActivity activity;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    public EDSPreferences(OnlineModelTestActivity activity) {
        this.activity = activity;
        intPrefsVar();
    }

    //shor form for initialize preferences variables
    public void intPrefsVar() {
        prefs = activity.getSharedPreferences(sharedPreferencesFile,Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void putPrefsVar(int reward, boolean bool,
                            ArrayList<OnlineModelTestRecyclerViewAdapter.CheckedMonitor> monitors,
                            ArrayList<ResponseMapper.OnlineModelTestResponse.Mcq> mcqs){

        SharedPreferencesUtils.putInt("reward",reward,editor);
        SharedPreferencesUtils.putBoolean("model_test_done",bool,editor);
        SharedPreferencesUtils.putObject("monitors",monitors,editor);
        SharedPreferencesUtils.putObject("mcqs",mcqs,editor);
    }

    public boolean resumeFReward(){
        boolean done = prefs.getBoolean("model_test_done",false);
        int reward = prefs.getInt("reward",0);

        if((reward == 1) && done){
            return true;
        }
        else return false;
    }

    public ArrayList<OnlineModelTestRecyclerViewAdapter.CheckedMonitor> getMonitors() throws Exception {
        return SharedPreferencesUtils.getOgjects(prefs,"monitors");
    }

    public ArrayList<ResponseMapper.OnlineModelTestResponse.Mcq> getMcqs() throws Exception {
        return SharedPreferencesUtils.getOgjects(prefs,"mcqs");

    }

    private static void log(String msg) {
        Log.d(TAG, msg);
    }
}
