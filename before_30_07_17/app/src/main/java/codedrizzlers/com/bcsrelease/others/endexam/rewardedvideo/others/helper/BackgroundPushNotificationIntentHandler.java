package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.helper;

import android.content.Intent;
import android.util.Log;

import codedrizzlers.com.bcsrelease.main.MainActivity;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.internet.AccessNetworkJob;


/**
 * Created by code drizzlers on 11/6/2016.
 */

public class BackgroundPushNotificationIntentHandler {
    MainActivity activity;
    public BackgroundPushNotificationIntentHandler(MainActivity activity){
        this.activity = activity;
    }

    public boolean checkIntentAction(Intent intent) {
        return isPushNotification(intent);
    }

    private boolean isPushNotification(Intent intent){
        if(intent.hasExtra("drizzlers_msg")){
            processIntent(intent);
            return true;
        }else
            return false;
    }

    private void processIntent(Intent intent){
        doIntentAction(intent);
        finishActivity(true);
    }

    private void doIntentAction(Intent intent) {
        checkPlaystoreAction(intent);
        checkInfoURLAction(intent);
        checkOnlyMsgAction(intent);
    }

    private void finishActivity(boolean finish){
        Log.d("BACKGROUNDPUSH","Program comes to FINISH ACTIVITY");
        if(finish)
            activity.finish();
    }

    private void checkPlaystoreAction(Intent intent) {
        AccessNetworkJob networkJob = new AccessNetworkJob(activity);

        if (intent.hasExtra("package_app_store")) {
            networkJob.goToPlayStroe(intent.getStringExtra("package_app_store"));
        }
    }

    private void checkInfoURLAction(Intent intent) {
        AccessNetworkJob networkJob = new AccessNetworkJob(activity);

        if (intent.hasExtra("info_url")) {
            networkJob.goToURL(intent.getStringExtra("info_url"));
        }
    }

    private void checkOnlyMsgAction(Intent intent) {
        if (intent.hasExtra("only_msg")) {
//            Log.d("BackGroundPush","Program comes to CHECKONLYMESSAGE.");
        }
    }

}
