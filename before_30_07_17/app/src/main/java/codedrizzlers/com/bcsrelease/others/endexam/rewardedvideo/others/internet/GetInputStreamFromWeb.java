package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.internet;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by code drizzlers on 10/29/2016.
 */
public class GetInputStreamFromWeb extends AsyncTask<String,Void,InputStream> {


    @Override
    protected InputStream doInBackground(String... strings) {
        String urlString = strings[0];
        try  {
            try {
                URL url = new URL(urlString);
                    /*URLConnection conn = url.openConnection();
                    InputStream in = conn.getInputStream();
                    URL url = new URL("http://www.google.com");*/
                //System.setProperty("http.agent", "Chrome");
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                //urlc.setRequestProperty("User-Agent", "Mozilla/4.76");
                //urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(10 * 30);
                urlc.setRequestMethod("POST");
                urlc.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                urlc.setRequestProperty("Content-Language", "en-US");
                urlc.setRequestProperty("User-Agent",
                        "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11");
                urlc.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                urlc.setRequestProperty("Accept-Language","en-gb,en;q=0.5");
                urlc.setRequestProperty("Accept-Encoding","UTF-8");
                urlc.setRequestProperty("Cookie","_ga=GA1.2.989262483.1379652403; __qca=P0-1317688601-1379652402575");
                urlc.setRequestProperty("Connection","keep-alive");

                //urlc.setUseCaches(false);
                urlc.setDoInput(true);
                //.d("INPUTSTREAM","THIS IS FROM DO IN BACKGROUND BEFORE CONNECT.");
                urlc.connect();
                //Log.d("INPUTSTREAM","THIS IS FROM DO IN BACKGROUND AFTER CONNECT.");
                //Log.d("ResponseCode","Response code is : "+urlc.getResponseCode());
                if(urlc.getResponseCode()==403) {
                    InputStream in = urlc.getErrorStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();

                    String line = null;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line).append('\n');
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            in.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    //Log.d("ErrorStream",sb.toString());
                }
                else if(urlc.getResponseCode() == HttpURLConnection.HTTP_OK)
                    return urlc.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(InputStream in){
        super.onPostExecute(in);
    }
}
