package codedrizzlers.com.bcsrelease.service.category;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.PqMcq;
import codedrizzlers.com.bcsrelease.database.PqMcqDao;
import codedrizzlers.com.bcsrelease.json.ResponseMapper;
import codedrizzlers.com.bcsrelease.service.SyncService;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SPHandler;
import cz.msebera.android.httpclient.Header;

/**
 * Created by code drizzlers on 6/19/2017.
 */

public class PreviousQuestionSyncService extends Service {
    private static final String TAG_HTTP_PQ_MCQ_SYNC = "TAG_HTTP_PQ_MCQ_SYNC";

    public static final String ACTION_SYNC_PREVIOIUS_QUESTION = "ACTION_PREVIOUS_QUESTION_SYNC_DONE";

    @Override
    public void onCreate() {
        super.onCreate();
        pqMcqSync(1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void pqMcqSync(int page) {

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.d("previous onSuccess", "syncHeaders :" + response.toString());
                pqMcqSyncOnSuccess(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("previous onFailure", "syncHeaders :" + errorResponse + "");
                Intent intent = new Intent(SyncService.FILTER_ACTION_SYNC_ERROR);
                sendBroadcast(intent);
                stopSelf();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        handler.setTag(TAG_HTTP_PQ_MCQ_SYNC);
        RequestParams params = new RequestParams();
        params.put(getString(R.string.page), page);
        params.put(getString(R.string.last_scanned_date), SPHandler.getPreviousQuestionSyncDate());
        AppSingleTon.ASYNC_HTTP_CLIENT.post(PreviousQuestionSyncService.this, AppSingleTon.APP_URL.SYNC_PQ_MCQ, params, handler);
    }
    void pqMcqSyncOnSuccess(JSONObject response) {

        ResponseMapper.McqResponse syncResponse = AppSingleTon.APP_JSON_PARSER.syncMcqResponse(response);
        pushToPqMcq(syncResponse.results);


        if (syncResponse.next_page > 0) {
            pqMcqSync(syncResponse.next_page);
        } else {

            stopService(syncResponse);
        }
    }

    void pushToPqMcq(final ArrayList<ResponseMapper.McqResponse.Mcq> mcqs) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                PqMcqDao mcqDao = AppSingleTon.DB_MANAGER.daoSession.getPqMcqDao();

                for (ResponseMapper.McqResponse.Mcq mcq : mcqs) {
                    if(mcq.sub_subject == 87)
                        Log.d("PqQuestion",mcq.question);

                    PqMcq checkMcqDb = mcqDao.load(mcq.id);
                    if (checkMcqDb == null) {

                        PqMcq mcqDb = new PqMcq();
                        mcqDb.setRemote_id(mcq.id);
                        mcqDb.setSub_subject_remote_id(mcq.sub_subject);
                        mcqDb.setQuestion(mcq.question);
                        mcqDb.setAnswer(mcq.answer);
                        mcqDb.setChoice_one(mcq.choice_one);
                        mcqDb.setChoice_two(mcq.choice_two);
                        mcqDb.setChoice_three(mcq.choice_three);
                        mcqDb.setChoice_four(mcq.choice_four);
                        mcqDb.setDone(false);
                        mcqDao.insert(mcqDb);
                    } else {

                        if (mcq.is_deleted == true) {
                            mcqDao.deleteByKey(mcq.id);
                        } else {
                            checkMcqDb.setSub_subject_remote_id(mcq.sub_subject);
                            checkMcqDb.setQuestion(mcq.question);
                            checkMcqDb.setAnswer(mcq.answer);
                            checkMcqDb.setChoice_one(mcq.choice_one);
                            checkMcqDb.setChoice_two(mcq.choice_two);
                            checkMcqDb.setChoice_three(mcq.choice_three);
                            checkMcqDb.setChoice_four(mcq.choice_four);
                            mcqDao.update(checkMcqDb);
                        }
                    }

                    Log.d("InsertPQ","Still inserting PQ question. question number "+mcq.id);

                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }
    void stopService(ResponseMapper.McqResponse syncResponse){
        SPHandler.setPreviousQuestionSyncDate(syncResponse.last_scanned_date);
        Intent intent = new Intent(PreviousQuestionSyncService.ACTION_SYNC_PREVIOIUS_QUESTION);
        sendBroadcast(intent);
        stopSelf();
    }
}
