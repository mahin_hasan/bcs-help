package codedrizzlers.com.bcsrelease.utils;

import android.os.AsyncTask;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by code drizzlers on 9/17/2016.
 */
public class GetInternetStatus extends AsyncTask<Void,Void,Boolean> {

    @Override
    protected Boolean doInBackground(Void... params) {

        return hasInternetAccess();
    }

    protected boolean hasInternetAccess() {

        try {
            URL url = new URL("http://www.google.com");

            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
            urlc.setRequestProperty("User-Agent", "Android Application:1");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1000 * 30);
            urlc.connect();

            // http://www.w3.org/Protocols/HTTP/HTRESP.html
            if (urlc.getResponseCode() == 200 || urlc.getResponseCode() > 400) {
                // Requested site is available
                return true;
            }
        } catch (Exception ex) {
            // Error while trying to connect
            ex.printStackTrace();
            return false;
        }
        return false;
    }
}
