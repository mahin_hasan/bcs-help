package codedrizzlers.com.bcsrelease.utils;

import android.content.Context;
import android.content.SharedPreferences;

import codedrizzlers.com.bcsrelease.R;

/**
 * Created by code drizzlers on 6/19/2017.
 */

public class SPHandler {

    private static String prefsFileName = "BCS_HELP_SHARED_PFERENCE";
    private static final String CATEGORY_HEADER_SYNC_DATE_KEY = "CATEGORY_HEADER_SCANNED_DATE";
    private static final String PREPARATION_QUESTION_ANSWER_SP_DATE_KEY = "PREPARATION_QUESTION_ANSWER_SCANNED_DATE";
    private static final String MCQ_QUESTION_SYNC_DATE_KEY = "MCQ_QUESTION_SCANNED_DATE";
    private static final String PREVIOUS_QUESTION_SYNC_DATE_KEY = "PREVIOUS_QUESITON_SCANNED_DATE";
    private static final String MODEL_TEST_CLOSE_HINT_SHOW_KEY = "IS_MODEL_TEST_HINT_SHOWN";
    private static final String IS_MAIN_ATIVITY_INSTRUCTION_TOUR_SHOWN_KEY = "IS_MAIN_ACTIVITY_INSTRUCTION_TOUR_SHOWN_DONE";
    private static final String IS_QUESTION_ANSWER_ACTIVITY_INSTRUCTION_TOUR_SHOWN_KEY =
            "IS_QUESTION_ANSWER_ACTIVITY_INSTRUCTION_TOUR_SHOWN_DONE";
    private static final String IS_MODEL_TEST_ACTIVITY_INSTRUCTION_TOUR_SHOWN_KEY =
            "IS_MODEL_TEST_ACTIVITY_INSTRUCTION_TOUR_DONE";

    public static boolean getIsModelTestActivityInstructionTourDone(){
        return getBoolFromSP(IS_MODEL_TEST_ACTIVITY_INSTRUCTION_TOUR_SHOWN_KEY,
                true);
    }

    public static void setIsModelTestActivityInstructionTourDone(boolean bool){
        setBootToSP(IS_MODEL_TEST_ACTIVITY_INSTRUCTION_TOUR_SHOWN_KEY,
                bool);
    }

    public static boolean getIsQuestionAnswerActivityInstructionTourDone(){
        return getBoolFromSP(IS_QUESTION_ANSWER_ACTIVITY_INSTRUCTION_TOUR_SHOWN_KEY,
                true);
    }

    public static void setIsQuestionAnswerActivityInstructionTourDone(boolean bool){
        setBootToSP(IS_QUESTION_ANSWER_ACTIVITY_INSTRUCTION_TOUR_SHOWN_KEY,
                bool);
    }

    public static boolean getIsMainActivityInstructionTourShown(){
        return getBoolFromSP(IS_MAIN_ATIVITY_INSTRUCTION_TOUR_SHOWN_KEY,true);
    }

    public static void setIsMainActivityInstructionTourShown(boolean bool){
        setBootToSP(IS_MAIN_ATIVITY_INSTRUCTION_TOUR_SHOWN_KEY,bool);
    }

    public static boolean getModelTestCloseHintShown(){
        return getBoolFromSP(MODEL_TEST_CLOSE_HINT_SHOW_KEY,true);
    }

    public static void setModelTestCloseHintShow(boolean value){
        setBootToSP(MODEL_TEST_CLOSE_HINT_SHOW_KEY,value);
    }
    public static String getPreviousQuestionSyncDate(){
        return getStringFromSP(PREVIOUS_QUESTION_SYNC_DATE_KEY);
    }

    public static void setPreviousQuestionSyncDate(String date){
        setStringToSP(PREVIOUS_QUESTION_SYNC_DATE_KEY,date);
    }

    public static String getMcqQuestionSyncDate(){
        return getStringFromSP(MCQ_QUESTION_SYNC_DATE_KEY);
    }

    public static void setMcqQuestionSyncDate(String date){
        setStringToSP(MCQ_QUESTION_SYNC_DATE_KEY,date);
    }


    public static String getCategoryHeaderSyncDate(){
        return getStringFromSP(CATEGORY_HEADER_SYNC_DATE_KEY);
    }

    public static void setCategoryHeaderSyncDate(String date){
        setStringToSP(CATEGORY_HEADER_SYNC_DATE_KEY,date);
    }

    public static String getPreparationQuestionAnswerScannedDate(){
        return getStringFromSP(PREPARATION_QUESTION_ANSWER_SP_DATE_KEY);
    }

    public static void setPreparationQuestionAnswerScannedDate(String date){
        setStringToSP(PREPARATION_QUESTION_ANSWER_SP_DATE_KEY,date);
    }

    private static String getStringFromSP(String key){
        SharedPreferences preference = getSP();
        return preference.getString(key,
                AppSingleTon.CONTEXT.getString(R.string.last_scanned_date_date));

    }

    private static void setStringToSP(String key,String date){
        SharedPreferences.Editor preferenceEditor = getSPEditor();
        preferenceEditor.putString(key, date);
        preferenceEditor.commit();
    }

    private static boolean getBoolFromSP(String key,boolean defaultBool){
        SharedPreferences preferences = getSP();
        return preferences.getBoolean(key,defaultBool);
    }

    private static void setBootToSP(String key, boolean value){
        SharedPreferences.Editor editor = getSPEditor();
        editor.putBoolean(key,value);
        editor.commit();
    }

    private static SharedPreferences getSP(){
        return  AppSingleTon.CONTEXT.getSharedPreferences(prefsFileName, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getSPEditor(){
        SharedPreferences preferences = AppSingleTon.CONTEXT.getSharedPreferences(prefsFileName, Context.MODE_PRIVATE);
        return preferences.edit();
    }

}
