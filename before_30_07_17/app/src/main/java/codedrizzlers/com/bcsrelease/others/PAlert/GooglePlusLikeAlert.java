package codedrizzlers.com.bcsrelease.others.PAlert;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.google.android.gms.plus.PlusOneButton;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.main.MainActivity;
import codedrizzlers.com.bcsrelease.others.PAlert.common.PAlertUtil;
import codedrizzlers.com.bcsrelease.others.PAlert.interfaces.PAlertSPBoolUpdate;


/**
 * Created by code drizzlers on 2/3/2017.
 */

public class GooglePlusLikeAlert {
    Context context;
    PlusOneButton mPbutton_standard;
    PAlertSPBoolUpdate updateBool;
    private static final int REQUEST_CODE = 0;
    private static String APPURL;
    AlertDialog alert;
    View alertView;

    public GooglePlusLikeAlert(Context context, PAlertSPBoolUpdate update){
        setContext(context);
        APPURL = getString(R.string.app_play_store_url)+getString(R.string.play_store_id);
        setUpdateBool(update);

        createGPlusAlert();
        new GPlusLikeResumeInterface(context,this);
    }

    public void showGPlusAlert(){
        alert.show();
    }

    public void createGPlusAlert(){
        alertView = PAlertUtil.createAlertView(R.layout.google_plus_like_alert_layout);
        alert = PAlertUtil.createAlert();
        alert.setView(alertView);

        PAlertUtil.initImageView(alertView,R.id.g_plus_like_alert_title_image,R.drawable.ic_thumbs_up_down_black_24dp);
        initPlusButton();
        initCancelButton();
    }

    private void initPlusButton() {
        mPbutton_standard = (PlusOneButton)alertView.findViewById(R.id.plus_one_button);

        //listener for plus one
        PlusOneButton.OnPlusOneClickListener listner = new PlusOneButton.OnPlusOneClickListener() {
            @Override
            public void onPlusOneClick(Intent intent) {
                updateBool.updateBool();
                ((Activity) MainActivity.context).startActivityForResult(intent, REQUEST_CODE);
            }
        };
        //end of listener

        mPbutton_standard.setOnPlusOneClickListener(listner);
    }

    private void initCancelButton() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        };

        PAlertUtil.initButton(R.id.g_plus_like_cancel_button,alertView,listener);
    }

    public void pButtonOnResume(){
        mPbutton_standard.initialize(APPURL, REQUEST_CODE);
    }

    public String getString(int id){
        return getContext().getString(id);
    }

    public void setUpdateBool(PAlertSPBoolUpdate updateBool) {
        this.updateBool = updateBool;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
