package codedrizzlers.com.bcsrelease.main;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.turingtechnologies.materialscrollbar.ICustomAdapter;

import java.util.List;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswer;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SPHandler;

public class QapQuestionAnswerRecyclerViewAdapter
        extends RecyclerView.Adapter<QapQuestionAnswerRecyclerViewAdapter.ViewHolder>
        implements ICustomAdapter {

    public  List<QapQuestionAnswer> mValues;
    Activity activity;

    public QapQuestionAnswerRecyclerViewAdapter(List<QapQuestionAnswer> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_question_answer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.mItem = mValues.get(position);

        String title = AppSingleTon.METHOD_BOX.convertNumericInBangla((position+1)+"");
        title = title+ ". " + mValues.get(position).getQuestion();
        holder.mTitle.setText(title);
        holder.mSubTitle.setText(mValues.get(position).getAnswer());

        if(holder.mItem.getDone()){
            holder.mTitle.setTextColor(ContextCompat.getColor(holder.mTitle.getContext(), R.color.colorAccent));
        }else{
            holder.mTitle.setTextColor(ContextCompat.getColor(holder.mTitle.getContext(), R.color.grey_700));
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public String getCustomStringForElement(int element) {
        return ""+element;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public  View mView;
        public  TextView mTitle, mSubTitle;
        public QapQuestionAnswer mItem;
        public  ImageView done;

        public ViewHolder(View view) {
            super(view);

            mView = view;
            mTitle = (TextView) view.findViewById(R.id.title);
            mSubTitle = (TextView) view.findViewById(R.id.sub_title);
            done = (ImageView)view.findViewById(R.id.done);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }

}
