package codedrizzlers.com.bcsrelease.main;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Locale;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.alert.PushNotificationAlert;


public class BaseActivity extends AppCompatActivity {

	private ProgressDialog progressDialog;

	AdView mAdView;
	InterstitialAd mInterstitialAd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initLocale();
	}

	public void initLocale(){

		String languageToLoad  = "bn";
		Locale locale = new Locale(languageToLoad);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());

	}

    public void showUpdateProgressDialog(final String message) {

		if (progressDialog == null) {
			ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(this, R.style.ProgressDialogCustom);
			progressDialog = new ProgressDialog(contextThemeWrapper);
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.setCancelable(false);

			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setOnDismissListener(new OnDismissListener() {
				
				@Override
				public void onDismiss(DialogInterface dialog) {

				}
			});
		}

		if (progressDialog.isShowing()) {
			progressDialog.setMessage(message);

		} else {
			progressDialog.setMessage(message);
			progressDialog.show();
		}

	}
	
    public void dismissProgressDialog() {
		if (progressDialog != null)
			progressDialog.dismiss();
	}
	 
    public void showToast(final String msg) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_LONG).show();
			}
		});
	}

	/**
	 * portion added by CodeDrizzlers(mahin)
	 * for banner adview
	 */
	void initAdView(){

		mAdView = (AdView) findViewById(R.id.adView);
		//RelativeLayout adContainer = (RelativeLayout) findViewById(R.id.ad_container);
		//mAdView = new AdView(this);
		//mAdView.setAdSize(AdSize.BANNER);
		// mAdView.setAdUnitId(getString(R.string.banner_home_footer));
		//adContainer.addView(mAdView);

		AdRequest adRequest = new AdRequest.Builder()
				.build();
		mAdView.loadAd(adRequest);
	}


	@Override
	protected void onPause() {
		if (mAdView != null) {
			mAdView.pause();
		}
		super.onPause();
		unregisterReceiver(firebasePushReceiver);

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mAdView != null) {
			mAdView.resume();
		}
		initLocale();
		registerPushNotificationReciever();
	}

	@Override
	public void onDestroy(){

		if (mAdView != null) {
			mAdView.destroy();
		}
		super.onDestroy();

	}
	/***
	 * end adview added
	 */

	//for interetial ad
	public void initInerestialAd(){
		mInterstitialAd = new InterstitialAd(this);
		mInterstitialAd.setAdUnitId(getString(R.string.admob_interesial_ad_id));

		requestNewInterstitial();
	}

	private void requestNewInterstitial() {
		AdRequest adRequest = new AdRequest.Builder().build();

		mInterstitialAd.loadAd(adRequest);
	}

	public void showAdmobInterstitialAd(){
		if (mInterstitialAd.isLoaded()) {
			mInterstitialAd.show();
		}
	}

	public boolean onBackOrHome(){
		showAdmobInterstitialAd();
		finish();
		return true;
	}

	//methods&var for processin firebase push notification
	BroadcastReceiver firebasePushReceiver;

	public BroadcastReceiver getPushNotificationReciever() {
		firebasePushReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.getAction() == "push_notification") {
					processPushNotificationIntent(context, intent);
				}
			}
		};

		return firebasePushReceiver;
	}

	public void processPushNotificationIntent(Context context, Intent intent) {
		PushNotificationAlert alert =
				new PushNotificationAlert(context, intent.getExtras());

		alert.isPushNotificationShouldShown();

	}

	private void registerPushNotificationReciever() {
		IntentFilter filter = new IntentFilter("push_notification");
		registerReceiver(getPushNotificationReciever(),filter);
	}
	//end of methods
}
