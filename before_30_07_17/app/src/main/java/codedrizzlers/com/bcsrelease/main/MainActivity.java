package codedrizzlers.com.bcsrelease.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.plus.PlusOneButton;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.DBManager;
import codedrizzlers.com.bcsrelease.database.MpSubject;
import codedrizzlers.com.bcsrelease.database.PqSubject;
import codedrizzlers.com.bcsrelease.database.QapSubject;
import codedrizzlers.com.bcsrelease.others.PAlert.GPlusLikeResumeInterface;
import codedrizzlers.com.bcsrelease.others.PAlert.managers.AppPAlertController;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.alert.ExitMoreAppsAlert;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.helper.BackgroundPushNotificationIntentHandler;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.overflowmenu.OverflowMenuHandler;
import codedrizzlers.com.bcsrelease.service.SyncService;
import codedrizzlers.com.bcsrelease.service.category.CategoryHeaderSync;
import codedrizzlers.com.bcsrelease.service.category.MCQuestionService;
import codedrizzlers.com.bcsrelease.service.category.PreparationQuestionSyncService;
import codedrizzlers.com.bcsrelease.service.category.PreviousQuestionSyncService;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SPHandler;
import codedrizzlers.com.bcsrelease.utils.StateUpdateAll;

public class MainActivity extends BaseActivity implements
        QapSubjectListFragment.OnSubjectListFragmentInteractionListener,
        MpSubjectListFragment.OnMpSubjectListFragmentInteractionListener,
        PqSubjectListFragment.FragmentInteractionListener,
        OnlineModelTestFragment.OnFragmentInteractionListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private UpdateStatusReceiver updateStatusReceiver;
    AdView mAdView;

    boolean qapIsUpdateNeeded = false, mpIsUpdateNeeded = false, pqIsUpdateNeeded = false;
    int ACTIVITY_REQUEST_CODE_QAP = 100;

    /*variable need for
    implement gPlus and likeRate Alert
     */
    PlusOneButton mPbutton_standard;
    private static final int REQUEST_CODE = 0;
    //app's Google plus page URL
    private static String APPURL;
    AppPAlertController manager;
    public static Context context;
    boolean isInstructionTourDone;

    GPlusLikeResumeInterface gPlusLikeResumeInterface;
    //end gPlus and likeRate Alert variable declaration


    //cheking for update
    private boolean isUpdateRunning = false;
    private StateUpdateAll stateUpdateAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!processPushNotificationIntent())
            appInitSetup(savedInstanceState);
    }

    void appInitSetup(Bundle savedInstanceState) {
        Log.d("onCreate", "onCreate");

        //showToast("onCreate");
        initContentView();
        context = this;
        //openReadableDatabase();
        openDatabase();
        //showUpdateProgressDialog("data loading");


        initToolbar();
        initViewPager();
        initTabLayout();
        registerReceiver();
        initFirstTime(SPHandler.getPreparationQuestionAnswerScannedDate(),
                R.string.first_time_preparation_question_sync, PreparationQuestionSyncService.class);
        initAdView();

        initSetupGPlus();
        initIsInstructionTourDone();


    }

    void initIsInstructionTourDone(){
        isInstructionTourDone = SPHandler.getIsModelTestActivityInstructionTourDone();
    }

    void initAdView() {

        mAdView = (AdView) findViewById(R.id.adView);
        //RelativeLayout adContainer = (RelativeLayout) findViewById(R.id.ad_container);
        //mAdView = new AdView(this);
        //mAdView.setAdSize(AdSize.BANNER);
        // mAdView.setAdUnitId(getString(R.string.banner_home_footer));
        //adContainer.addView(mAdView);

        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);
    }


    @Override
    protected void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
        AppSingleTon.DB_MANAGER.daoSession.clear();
        resumeGPlusButton();
    }

    private void resumeGPlusButton() {
        // Refresh the state of the +1 button each time the activity receives focus.
        if (mPbutton_standard != null)
            mPbutton_standard.initialize(APPURL, REQUEST_CODE);
        if (gPlusLikeResumeInterface != null)
            gPlusLikeResumeInterface.onResumeContext();
    }

    void initContentView() {
        setContentView(R.layout.activity_main);
    }

    void openDatabase() {
        AppSingleTon.DB_MANAGER = DBManager.getInstance(MainActivity.this);
        AppSingleTon.DB_MANAGER.openWritableDb();
    }

    void closeDatabase() {
        AppSingleTon.DB_MANAGER.closeDbConnections();
        AppSingleTon.DB_MANAGER = null;
    }

    void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.logo_36);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.app_name);
    }

    void initViewPager() {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                onViewPagerPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    void onViewPagerPageSelected(int position) {
        //showToast("onPageSelected : " + position);
        if (position == 0) {
            onPreparationTabSelected();
        } else if (position == 1) {
            onMcqTabSelected();
        } else if (position == 2) {
            onPreviousQuestionTabSelected();
        } else if (position == 3 && (!AppSingleTon.METHOD_BOX.isInternetConnected())) {
            startNoInternetDialog();
        }

        promptRating();
    }

    void onPreparationTabSelected() {
        if (qapIsUpdateNeeded) {
            mSectionsPagerAdapter.qapSubjectListFragment.refreshAdapter();
            qapIsUpdateNeeded = false;
        }
    }

    void onMcqTabSelected() {
        initFirstTime(SPHandler.getMcqQuestionSyncDate(),
                R.string.first_time_mcq_sync, MCQuestionService.class);

        if (mpIsUpdateNeeded) {
            mSectionsPagerAdapter.mpSubjectListFragment.refreshAdapter();
            mpIsUpdateNeeded = false;
        }
    }

    void onPreviousQuestionTabSelected() {
        initFirstTime(SPHandler.getPreviousQuestionSyncDate(),
                R.string.first_time_previous_question_sync, PreviousQuestionSyncService.class);

        if (pqIsUpdateNeeded) {
            mSectionsPagerAdapter.pqSubjectListFragment.refreshAdapter();
            pqIsUpdateNeeded = false;
        }
    }

    void initTabLayout() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    void initFirstTime(String headerDate, int msg, Class<?> tClass) {

        if (headerDate.equals(getString(R.string.last_scanned_date_date))) {
            //showToast("service starting...");
            CategoryHeaderSync.setIsCompleted(false);
            startService(R.string.first_header_download, CategoryHeaderSync.class);
            //startNextDataService(msg,tClass);
        }

    }


    public void startService(int stringId, Class tClass) {
        if (!AppSingleTon.METHOD_BOX.isInternetConnected()) {
            startNoInternetDialog();
        } else {
            showUpdateProgressDialog(getString(stringId));
            //Toast.makeText(this, getString(stringId), Toast.LENGTH_LONG).show();
            Intent syncIntent = new Intent(this, tClass);
            Log.d("MainActivity","Before starting service");
            startService(syncIntent);
        }
    }

    void stopService() {
        dismissProgressDialog();
        Intent syncIntent = new Intent(this, SyncService.class);
        stopService(syncIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_update) {
            //startService(R.string.current_updating);
            isUpdateRunning = true;
            stateUpdateAll = new StateUpdateAll(this);
            stateUpdateAll.stateInit();
            return true;
        }/*else if(id == R.id.action_send_your_opinion){

            if(!AppSingleTon.METHOD_BOX.isInternetConnected()){
                startNoInternetDialog();
            }else {
                AppSingleTon.METHOD_BOX.sendEmail(MainActivity.this, "codedrizzlers@gmail.com", "BCS help - Feedback", "");
            }

        }*/ else if (id == R.id.action_rate_us_on_play) {
            if (!AppSingleTon.METHOD_BOX.isInternetConnected()) {
                startNoInternetDialog();
            } else {
                AppSingleTon.METHOD_BOX.rateToGooglePlay(MainActivity.this);
            }
        } else {
            OverflowMenuHandler menuHandler = new OverflowMenuHandler(this, id);
            menuHandler.menusAction();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ACTIVITY_REQUEST_CODE_QAP) {
            //showToast("aise");
            int position = data.getIntExtra(getString(R.string.position), 0);
            mSectionsPagerAdapter.qapSubjectListFragment.adapter.notifyItemChanged(position);
        }
    }

    @Override
    public void onListFragmentInteraction(int position) {
        showUpdateProgressDialog("data loading.");

        QapSubject item = mSectionsPagerAdapter.qapSubjectListFragment.adapter.mValues.get(position);
        Intent intent = new Intent(this, QapSubSubjectActivity.class);
        intent.putExtra(getString(R.string.subject), item);
        intent.putExtra(getString(R.string.position), position);
        startActivityForResult(intent, ACTIVITY_REQUEST_CODE_QAP);
        dismissProgressDialog();
    }

    @Override
    public void onListFragmentInteraction(MpSubject item) {

        Intent intent = new Intent(this, MpSubSubjectActivity.class);
        intent.putExtra(getString(R.string.subject), item);
        startActivity(intent);
    }

    @Override
    public void onListFragmentInteraction(PqSubject item) {
        Intent intent = new Intent(this, PqSubSubjectActivity.class);
        intent.putExtra(getString(R.string.subject), item);
        startActivity(intent);
    }

    @Override
    public void onFragmentInteraction() {

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        QapSubjectListFragment qapSubjectListFragment;
        MpSubjectListFragment mpSubjectListFragment;
        PqSubjectListFragment pqSubjectListFragment;
        OnlineModelTestFragment modelTestFragment;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            //showToast(""+position);
            if (position == 0) {

                if (qapSubjectListFragment == null) {
                    qapSubjectListFragment = QapSubjectListFragment.newInstance();
                }
                return qapSubjectListFragment;

            } else if (position == 1) {

                if (mpSubjectListFragment == null) {
                    mpSubjectListFragment = MpSubjectListFragment.newInstance();
                }
                return mpSubjectListFragment;

            } else if (position == 2) {

                if (pqSubjectListFragment == null) {
                    pqSubjectListFragment = PqSubjectListFragment.newInstance();
                }
                return pqSubjectListFragment;

            } else {
                if (modelTestFragment == null) {
                    modelTestFragment = OnlineModelTestFragment.newInstance();
                }
                return modelTestFragment;
            }

        }


        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.preparation);
                case 1:
                    return getString(R.string.mcq);
                case 2:
                    return getString(R.string.previous_question);
                case 3:
                    return getString(R.string.model_test);
            }
            return null;
        }
    }

    class UpdateStatusReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            processIntentAction(intent);
        }

    }

    void processIntentAction(Intent intent) {

        if (intent.getAction() == SyncService.FILTER_ACTION_SYNC_ERROR) {
        } else if (intent.getAction() == CategoryHeaderSync.ACTION_HEADER_SYNC_DONE) {
            processActionHeaderSyncDone();
        } else if (intent.getAction() == PreparationQuestionSyncService.ACTION_SYNC_PREPARATION_QUESTION) {
            processPreparationSyncAction();
        } else if (intent.getAction() == PreviousQuestionSyncService.ACTION_SYNC_PREVIOIUS_QUESTION) {
            processPreviousSyncAction();
        } else if (intent.getAction() == MCQuestionService.ACTION_MCQ_QUESTION_SYNC) {
            processMCQSyncAction();
        }

        notifyUpdateState(intent);
    }

    void notifyUpdateState(Intent intent) {
        if (isUpdateRunning) {
            stateUpdateAll.stateEnd(intent.getAction());
        }
    }

    void processPreviousSyncAction() {
        if (mViewPager.getCurrentItem() == 2) {
            mSectionsPagerAdapter.pqSubjectListFragment.refreshAdapter();
            pqIsUpdateNeeded = false;
        }
        dismissProgressDialog();
    }

    void processMCQSyncAction() {
        if(mViewPager.getCurrentItem() == 1) {
            mSectionsPagerAdapter.mpSubjectListFragment.refreshAdapter();
            mpIsUpdateNeeded = false;
        }
        dismissProgressDialog();
    }

    void processActionHeaderSyncDone() {
        dismissProgressDialog();
        if (!isUpdateRunning) {
            onHeaderSyncDone();
        }
        else if(isUpdateRunning){
            setAllTabsRefreshToTrue();
        }
    }

    void setAllTabsRefreshToTrue(){
        qapIsUpdateNeeded = true;
        mpIsUpdateNeeded = true;
        pqIsUpdateNeeded = true;
    }

    void processPreparationSyncAction() {
        dismissProgressDialog();
        if(mViewPager.getCurrentItem() == 0) {
            mSectionsPagerAdapter.qapSubjectListFragment.refreshAdapter();
            qapIsUpdateNeeded = false;
        }

        if(!isUpdateRunning) {
            isStartDemoTour();
        }
    }

    void isStartDemoTour() {
        if (isInstructionTourDone) {
            isInstructionTourDone = false;
            SPHandler.setIsMainActivityInstructionTourShown(false);
            startActivityInstructionTour();
        }
    }

    void onHeaderSyncDone() {
        int currentTab = mViewPager.getCurrentItem();

        switch (currentTab) {
            case 0:
                startService(R.string.first_time_preparation_question_sync, PreparationQuestionSyncService.class);
                break;
            case 1:
                startService(R.string.first_time_mcq_sync, MCQuestionService.class);
                break;
            case 2:
                startService(R.string.first_time_previous_question_sync, PreviousQuestionSyncService.class);
                break;
        }
    }

    void startNoInternetDialog() {

        ContextThemeWrapper context = new ContextThemeWrapper(this, R.style.AlertDialogCustom);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.no_internet_msg));
        builder.setMessage(getString(R.string.open_internet_msg));
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AppSingleTon.METHOD_BOX.startSetting(MainActivity.this);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));

            }
        });

        dialog.show();
    }


    void registerReceiver() {
        updateStatusReceiver = new UpdateStatusReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(SyncService.FILTER_ACTION_SYNC_DONE);
        filter.addAction(SyncService.FILTER_ACTION_SYNC_ERROR);
        filter.addAction(PreviousQuestionSyncService.ACTION_SYNC_PREVIOIUS_QUESTION);
        filter.addAction(MCQuestionService.ACTION_MCQ_QUESTION_SYNC);
        filter.addAction(CategoryHeaderSync.ACTION_HEADER_SYNC_DONE);
        filter.addAction(PreparationQuestionSyncService.ACTION_SYNC_PREPARATION_QUESTION);
        registerReceiver(updateStatusReceiver, filter);

    }

    void unregisterReceiver() {
        unregisterReceiver(updateStatusReceiver);
    }

    @Override
    public void onDestroy() {

        if (mAdView != null) {
            mAdView.destroy();
        }
        closeDatabase();
        unregisterReceiver();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        ExitMoreAppsAlert exitAlert = new ExitMoreAppsAlert(this);
        exitAlert.showExitMoreAppsAlertDialog();
    }

    //method need for gplus alert
    public void setgPlusLikeResumeInterface(GPlusLikeResumeInterface gPlusLikeResumeInterface) {
        this.gPlusLikeResumeInterface = gPlusLikeResumeInterface;
    }

    void initSetupGPlus() {
        APPURL = "https://play.google.com/store/apps/details?id=" + getString(R.string.play_store_id);
        context = this;
        mPbutton_standard = (PlusOneButton) findViewById(R.id.plus_one_button);

        manager = new AppPAlertController(this);
        //manager.showAlert();
    }

    //end of gPlus methods


    //method for showing gPlus&rRate alert
    public void promptRating() {
        //LikeRateFeedbackAlert like = new LikeRateFeedbackAlert(this);
        //if (!like.promptRating());
        ///showAdmobInterestialAd();

        manager.showAlert();
    }

    //method for background push handling
    public boolean processPushNotificationIntent() {
        //Log.d("MainFirebase", "processExtraData called.");

        BackgroundPushNotificationIntentHandler intentHandler =
                new BackgroundPushNotificationIntentHandler(this);
        return intentHandler.checkIntentAction(getIntent());
    }

    public void setIsUpdateRunning(boolean bool) {
        isUpdateRunning = bool;
    }


    public void startActivityInstructionTour() {
        TabLayout tabLayout = getTabLayout();
        Toolbar toolbar = getMainActivityToolbar();

        TextView mcqTabTitleTextView = getTabTitleTextView(1, tabLayout);
        TextView pqTabTitleTextView = getTabTitleTextView(2, tabLayout);

        TapTarget mcqTapTarget = getTapTargetView(mcqTabTitleTextView,
                getString(R.string.mcq_instruction_tab),getString(R.string.mcq_tab_hints)).id(1).targetRadius(80);
        TapTarget pqTapTarget = getTapTargetView(pqTabTitleTextView,
                getString(R.string.previous_instruction_tab),getString(R.string.previous_tab_hints)).id(2).targetRadius(80);
        TapTarget overFlowTapTarget = getOverFlowTapTarget(toolbar,
                getString(R.string.menu_button_name),getString(R.string.main_menu_hint)).id(3);

        TapTargetSequence tapTargetSequence =
                new TapTargetSequence(this)
                        .targets(
                                mcqTapTarget, pqTapTarget,
                                overFlowTapTarget)
                        .listener(getTargetSequenceListener());

        tapTargetSequence.start();


    }

    public TextView getTabTitleTextView(int id, TabLayout tabLayout) {
        return (TextView) (((LinearLayout) ((LinearLayout) tabLayout.getChildAt(0)).getChildAt(id)).getChildAt(1));
    }

    public TapTarget getOverFlowTapTarget(Toolbar toolbar, String title, String description) {
        return TapTarget.forToolbarOverflow(toolbar, title, description)
                .outerCircleColor(R.color.black)
                .outerCircleAlpha(.9f)
                .targetCircleColor(R.color.white)
                .dimColor(R.color.aluminum)
                .titleTextColor(R.color.white)
                .descriptionTextColor(R.color.white)
                .cancelable(false);
    }

    public TabLayout getTabLayout() {
        return (TabLayout) findViewById(R.id.tabs);
    }

    public Toolbar getMainActivityToolbar() {
        return (Toolbar) findViewById(R.id.toolbar);
    }

    TapTarget getTapTargetView(View v, String title, String description) {
        return TapTarget.forView(
                v, title,description)
                .outerCircleColor(R.color.black)
                .outerCircleAlpha(.9f)
                .targetCircleColor(R.color.white)
                .dimColor(R.color.aluminum)
                .titleTextColor(R.color.white)
                .descriptionTextColor(R.color.white)
                .cancelable(false);
    }

    TapTargetSequence.Listener getTargetSequenceListener() {
        return new TapTargetSequence.Listener() {

            @Override
            public void onSequenceFinish() {
                SPHandler.setIsMainActivityInstructionTourShown(false);
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        };
    }
}
