package codedrizzlers.com.bcsrelease.main;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;
import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswer;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswerDao;
import codedrizzlers.com.bcsrelease.database.QapSubSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SynchronousHandler;

public class QapSubSubjectRecyclerViewAdapter extends RecyclerView.Adapter<QapSubSubjectRecyclerViewAdapter.ViewHolder> {

    public  List<QapSubSubject> mValues;
    public ArrayList<DoneMonitor> doneMonitorArrayList;
    private final OnSubSubjectListFragmentInteractionListener mListener;

    public QapSubSubjectRecyclerViewAdapter(List<QapSubSubject> items, OnSubSubjectListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        generateDoneMonitorList();
    }

    void generateDoneMonitorList(){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                doneMonitorArrayList = new ArrayList<DoneMonitor>();

                for(int i=0;i<mValues.size();i++){
                    DoneMonitor doneMonitor = new DoneMonitor();
                    doneMonitor.numOfDone = generateNumOfDone(i);
                    doneMonitor.listSize = getDoneMonitorListSize(i);
                    doneMonitorArrayList.add(doneMonitor);
                }
            }
        };

        SynchronousHandler.postAndWait(new Handler(),runnable);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sub_subject, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        String title = AppSingleTon.METHOD_BOX.convertNumericInBangla((position + 1) + "");
        title = title + ". " + mValues.get(position).getTitle();
        holder.mTitle.setText(title);


        int size = doneMonitorArrayList.get(position).listSize;
        //doneMonitorArrayList.get(position).listSize = size;
        //doneMonitorArrayList.get(position).numOfDone = generateNumOfDone(position);

        String sub = holder.mView.getContext().getString(R.string.number_of_question);
        sub = sub + " : " + AppSingleTon.METHOD_BOX.convertNumericInBangla(size + "");
        sub = sub + holder.mView.getContext().getString(R.string.ti);
        sub = sub+ " - "+generateCompletePercentage(doneMonitorArrayList.get(position).numOfDone,
                doneMonitorArrayList.get(position).listSize, holder.mView.getContext());
        holder.mSubTitle.setText(sub);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(position);
                }
            }
        });

        if(position == mValues.size()-1) {
            mListener.nowGenerateTotalPercentage();
        }
    }

    int getDoneMonitorListSize(int position){
        QapQuestionAnswerDao questionAnswerDao = AppSingleTon.DB_MANAGER.daoSession.getQapQuestionAnswerDao();
        QueryBuilder<QapQuestionAnswer> qb = questionAnswerDao.queryBuilder();
        qb.where(QapQuestionAnswerDao.Properties.Sub_subject_remote_id
                .eq(mValues.get(position).getRemote_id()));

        return qb.list().size();
    }

    int generateNumOfDone(int position) {

        int numOfDone = 0;
        QapSubSubject subSubject = mValues.get(position);
        List<QapQuestionAnswer> dataList = AppSingleTon.DB_MANAGER.daoSession.getQapQuestionAnswerDao().queryBuilder().
                where(QapQuestionAnswerDao.Properties.Sub_subject_remote_id.eq(subSubject.getRemote_id())).list();
        for (QapQuestionAnswer each : dataList) {
            if (each.getDone()) {
                numOfDone += 1;
            }
        }
        return numOfDone;
    }

    String generateCompletePercentage(int numOfDone, int listSize, Context context){
        if(numOfDone == 0){
            String str = "0% "+ context.getString(R.string.complete);
            return str;
        }

        int complete = (numOfDone*100)/listSize;
        String str = AppSingleTon.METHOD_BOX.convertNumericInBangla(complete+"")+"% "+ context.getString(R.string.complete);
        return str;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitle, mSubTitle;
        public QapSubSubject mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitle = (TextView) view.findViewById(R.id.title);
            mSubTitle = (TextView) view.findViewById(R.id.sub_title);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }

    public class DoneMonitor{
        public int numOfDone = 0, listSize = 0;
    }

    public interface OnSubSubjectListFragmentInteractionListener {
        void onListFragmentInteraction(int position);
        void nowGenerateTotalPercentage();
    }
}
