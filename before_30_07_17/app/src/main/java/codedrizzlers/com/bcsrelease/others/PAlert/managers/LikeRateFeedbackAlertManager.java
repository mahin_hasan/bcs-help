package codedrizzlers.com.bcsrelease.others.PAlert.managers;

import android.content.Context;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.others.PAlert.LikeRateFeedbackAlert;
import codedrizzlers.com.bcsrelease.others.PAlert.common.PAlertOnSharedPrefs;
import codedrizzlers.com.bcsrelease.others.PAlert.interfaces.PAlertManager;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.internet.AccessNetworkJob;

import static codedrizzlers.com.bcsrelease.others.PAlert.common.PAlertUtil.putBooleanToPreferences;


/**
 * Created by code drizzlers on 2/9/2017.
 */

public class LikeRateFeedbackAlertManager implements PAlertManager {
    Context context;
    PAlertOnSharedPrefs prefs;
    LikeRateFeedbackAlert alert;

    public LikeRateFeedbackAlertManager(Context con){
        this.context = con;
        alert = new LikeRateFeedbackAlert(this);
        prefs = new PAlertOnSharedPrefs(context,"rate","rated");
    }

    public boolean showAlert(){
        if(prefs.prompt()){
            alert.showRFLAlert();
            return true;
        }
        return false;
    }


    public void showEmailDialog() {
        AccessNetworkJob job = new AccessNetworkJob(context);
        job.showEmailDialog();
    }

    public void endOfAlert() {
        goToRate();

        putBooleanToPreferences("rated", true);
    }
    public void goToRate() {
        AccessNetworkJob job = new AccessNetworkJob(context);
        job.goToPlayStroe(context.getString(R.string.play_store_id));
    }
}
