package codedrizzlers.com.bcsrelease.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import java.util.List;
import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.PqSubSubject;
import codedrizzlers.com.bcsrelease.database.PqSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.PqSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class PqSubSubjectActivity extends BaseActivity implements PqSubSubjectRecyclerViewAdapter.FragmentInteractionListener {

    Toolbar toolbar;
    PqSubject subject;
    ActionBar actionBar;
    RecyclerView recyclerView;
    PqSubSubjectRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp_sub_subject);
        initToolbar();
        initParse();
        initRecyclerView();
        initAdView();
    }

    void initToolbar(){

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    void initParse(){

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            subject = (PqSubject) bundle.getSerializable(getString(R.string.subject));
        }

        actionBar.setTitle(subject.getTitle());
    }

    void initSubTitle(int total){

        String sub = getString(R.string.sub_subject);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(total+"");
        sub = sub + getString(R.string.ti);

        actionBar.setSubtitle(sub);
    }

    void initRecyclerView(){

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        List<PqSubSubject> list = AppSingleTon.DB_MANAGER.daoSession.getPqSubSubjectDao().
                queryBuilder().where(PqSubSubjectDao.Properties.Subject_remote_id.eq(subject.getRemote_id())).list();

        initSubTitle(list.size());

        adapter = new PqSubSubjectRecyclerViewAdapter(list, (PqSubSubjectRecyclerViewAdapter.FragmentInteractionListener)PqSubSubjectActivity.this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return false;
    }


    @Override
    public void onListFragmentInteraction(PqSubSubject item) {
        Intent intent = new Intent(this, PqMcqActivity.class);
        intent.putExtra(getString(R.string.sub_subject), item);
        startActivity(intent);
    }
}

