package codedrizzlers.com.bcsrelease.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;
import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.PqMcq;
import codedrizzlers.com.bcsrelease.database.PqMcqDao;
import codedrizzlers.com.bcsrelease.database.PqSubSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class PqSubSubjectRecyclerViewAdapter extends RecyclerView.Adapter<PqSubSubjectRecyclerViewAdapter.ViewHolder> {

    private final List<PqSubSubject> mValues;
    private final FragmentInteractionListener mListener;

    public PqSubSubjectRecyclerViewAdapter(List<PqSubSubject> items, FragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sub_subject, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        String title = AppSingleTon.METHOD_BOX.convertNumericInBangla((position+1)+"");
        title = title+ ". " + mValues.get(position).getTitle();
        holder.mTitle.setText(title);

        PqMcqDao pqMcqDao = AppSingleTon.DB_MANAGER.daoSession.getPqMcqDao();
        QueryBuilder<PqMcq> qb = pqMcqDao.queryBuilder();
        qb.where(PqMcqDao.Properties.Sub_subject_remote_id
                .eq(mValues.get(position).getRemote_id()));

        String sub = holder.mView.getContext().getString(R.string.number_of_mcq);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(qb.list().size()+"");
        sub = sub + holder.mView.getContext().getString(R.string.ti);
        holder.mSubTitle.setText(sub);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitle, mSubTitle;
        public PqSubSubject mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitle = (TextView) view.findViewById(R.id.title);
            mSubTitle = (TextView) view.findViewById(R.id.sub_title);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }

    public interface FragmentInteractionListener {
        void onListFragmentInteraction(PqSubSubject item);
    }
}
