package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.Utils;

import android.content.SharedPreferences;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.ObjectSerializer;

/**
 * Created by code drizzlers on 3/31/2017.
 */

public class SharedPreferencesUtils {
    public static final String TAG = "SharedPreferencesUtils";

    public static void putBoolean(String key, boolean bool, SharedPreferences.Editor editor) {
        editor.putBoolean(key, bool);
        editor.commit();
    }

    public static void putInt(String key,int i, SharedPreferences.Editor editor) {
        editor.putInt(key, i);
        editor.commit();
    }


    public static <E> void putObject(String key, ArrayList<E> objects, SharedPreferences.Editor editor) {
        try {
            editor.putString(key, ObjectSerializer.serialize(objects));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    public static <E> ArrayList<E> getOgjects(SharedPreferences preferences, String key) throws Exception {
        ArrayList<E> eArrayList = new ArrayList<E>();
        try {
            ArrayList<E> eArrayListArg = new ArrayList<E>();
            String string = preferences.getString(key, ObjectSerializer.serialize(eArrayListArg));

            eArrayList = (ArrayList<E>) ObjectSerializer.deserialize(string);

            return eArrayList;
        } catch (IOException e) {
            e.printStackTrace();
            Log.v(TAG, e.getMessage(), new Exception(e.getMessage()));
            throw new Exception("Serialization error: " + e.getMessage(), e);
        }
    }

    private static void log(String msg) {
        Log.d(TAG, msg);
    }
}
