package codedrizzlers.com.bcsrelease.others.PAlert.managers;

import android.content.Context;

import java.util.ArrayList;

import codedrizzlers.com.bcsrelease.others.PAlert.interfaces.PAlertManager;


/**
 * Created by code drizzlers on 2/6/2017.
 */

public class AppPAlertController {
    Context context;

    ArrayList<PAlertManager> managers;

    public AppPAlertController(Context con) {
        this.context = con;

        managers = new ArrayList<PAlertManager>();
        addingManagers();
    }

    private void addingManagers() {
        managers.add(new LikeRateFeedbackAlertManager(context));
        managers.add(new GPlusPPAlertManager(context));
    }


    public void showAlert() {
        for (PAlertManager a :
                managers) {
            if (a.showAlert())
                break;
        }
    }
}
