package codedrizzlers.com.bcsrelease.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.turingtechnologies.materialscrollbar.ICustomAdapter;

import java.util.List;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.json.Models;

public class MtListRecyclerViewAdapter
        extends RecyclerView.Adapter<MtListRecyclerViewAdapter.ViewHolder>
        implements ICustomAdapter {

    public  List<Models.Subject> mValues;

    public MtListRecyclerViewAdapter(List<Models.Subject> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_mt_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String title = mValues.get(position).title;
        //String title = AppSingleTon.METHOD_BOX.convertNumericInBangla((position+1)+"");
        //title = title+ ". " + mValues.get(position).title;
        holder.mTitle.setText(title);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public String getCustomStringForElement(int element) {
        return ""+element;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public  View mView;
        public  TextView mTitle;

        public ViewHolder(View view) {
            super(view);

            mView = view;
            mTitle = (TextView) view.findViewById(R.id.title);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }

}
