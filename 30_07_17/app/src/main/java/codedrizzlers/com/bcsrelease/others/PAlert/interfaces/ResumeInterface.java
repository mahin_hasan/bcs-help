package codedrizzlers.com.bcsrelease.others.PAlert.interfaces;

/**
 * Created by code drizzlers on 2/6/2017.
 */

public interface ResumeInterface {
   void onResumeContext();
}
