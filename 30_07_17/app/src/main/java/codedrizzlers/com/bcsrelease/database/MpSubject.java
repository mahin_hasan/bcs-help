package codedrizzlers.com.bcsrelease.database;

import org.greenrobot.greendao.annotation.*;

import java.io.Serializable;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "MP_SUBJECT".
 */
@Entity
public class MpSubject implements Serializable{
    private static final long serialVersionUID = 1113799434508095L;

    @Id
    private Long remote_id;
    private String title;

    @Generated(hash = 1775951445)
    public MpSubject() {
    }

    public MpSubject(Long remote_id) {
        this.remote_id = remote_id;
    }

    @Generated(hash = 1642986620)
    public MpSubject(Long remote_id, String title) {
        this.remote_id = remote_id;
        this.title = title;
    }

    public Long getRemote_id() {
        return remote_id;
    }

    public void setRemote_id(Long remote_id) {
        this.remote_id = remote_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
