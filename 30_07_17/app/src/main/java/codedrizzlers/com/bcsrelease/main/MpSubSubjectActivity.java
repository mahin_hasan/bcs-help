package codedrizzlers.com.bcsrelease.main;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import java.util.List;
import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.MpSubSubject;
import codedrizzlers.com.bcsrelease.database.MpSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.MpSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class MpSubSubjectActivity extends BaseActivity implements MpSubSubjectRecyclerViewAdapter.OnSubSubjectListFragmentInteractionListener {

    Toolbar toolbar;
    MpSubject subject;
    ActionBar actionBar;
    RecyclerView recyclerView;
    MpSubSubjectRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp_sub_subject);
        initToolbar();
        initParse();
        initRecyclerView();
        //init adview
        initAdView();
    }

    void initToolbar(){

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    void initParse(){

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            subject = (MpSubject) bundle.getSerializable(getString(R.string.subject));
        }

        actionBar.setTitle(subject.getTitle());
    }

    void initSubTitle(int total){

        String sub = getString(R.string.sub_subject);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(total+"");
        sub = sub + getString(R.string.ti);

        actionBar.setSubtitle(sub);
    }

    void initRecyclerView(){

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        List<MpSubSubject> list = AppSingleTon.DB_MANAGER.daoSession.getMpSubSubjectDao().
                queryBuilder().where(MpSubSubjectDao.Properties.Subject_remote_id.eq(subject.getRemote_id())).list();

        initSubTitle(list.size());

        adapter = new MpSubSubjectRecyclerViewAdapter(list, (MpSubSubjectRecyclerViewAdapter.OnSubSubjectListFragmentInteractionListener)MpSubSubjectActivity.this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onListFragmentInteraction(MpSubSubject item) {
        Intent intent = new Intent(this, MpMcqActivity.class);
        intent.putExtra(getString(R.string.sub_subject), item);
        startActivity(intent);
    }
}

