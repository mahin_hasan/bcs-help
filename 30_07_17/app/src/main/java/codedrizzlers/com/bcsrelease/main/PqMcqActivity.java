package codedrizzlers.com.bcsrelease.main;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.turingtechnologies.materialscrollbar.TouchScrollBar;

import java.util.List;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.PqMcq;
import codedrizzlers.com.bcsrelease.database.PqMcqDao;
import codedrizzlers.com.bcsrelease.database.PqSubSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class PqMcqActivity extends BaseActivity implements PqMcqRecyclerViewAdapter.FragmentInteractionListener {

    Toolbar toolbar;
    PqSubSubject subSubject;
    ActionBar actionBar;
    RecyclerView recyclerView;
    PqMcqRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pq_mcq);
        initToolbar();
        initParse();
        initRecyclerView();
        initFastScrollBar();
        //init ad view
        initAdView();
        initInerestialAd();
    }

    void initToolbar(){

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    void initParse(){

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            subSubject = (PqSubSubject) bundle.getSerializable(getString(R.string.sub_subject));
        }

        actionBar.setTitle(subSubject.getTitle());
    }

    void initSubTitle(int total){

        String sub = getString(R.string.number_of_mcq);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(total+"");
        sub = sub + getString(R.string.ti);

        actionBar.setSubtitle(sub);
    }


    void initFastScrollBar(){
        ((TouchScrollBar)findViewById(R.id.touchScrollBar))
                //.addIndicator(new CustomIndicator(this), true)
                .setHandleColour(ContextCompat.getColor(this, R.color.grey_300))
                .setHideDuration(500)
                .setTextColour(ContextCompat.getColor(this, R.color.monsoon))
                .setBarColour(ContextCompat.getColor(this, R.color.white));
    }

    void initRecyclerView(){

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        List<PqMcq> list = AppSingleTon.DB_MANAGER.daoSession.getPqMcqDao().
                queryBuilder().where(PqMcqDao.Properties.Sub_subject_remote_id.eq(subSubject.getRemote_id())).list();

        initSubTitle(list.size());

        adapter = new PqMcqRecyclerViewAdapter(list, (PqMcqRecyclerViewAdapter.FragmentInteractionListener)PqMcqActivity.this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == android.R.id.home){
            onBackOrHome();
        }
        return false;
    }

    @Override
    public void onListFragmentInteraction(PqMcq item) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onBackOrHome();
    }
}

