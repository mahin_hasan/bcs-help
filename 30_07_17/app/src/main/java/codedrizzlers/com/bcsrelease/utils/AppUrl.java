package codedrizzlers.com.bcsrelease.utils;

public class AppUrl {


    public String BASE = "http://codedrizzler.com/";
    //public String BASE = "http://bcs-help.herokuapp.com/";
    //public String BASE = "http://10.0.2.2/";
    public String SYNC_HEADER = BASE + "sync_header/";
    public String SYNC_QUESTION_ANSWER = BASE + "sync_qap_qa/";
    public String SYNC_MP_MCQ = BASE + "sync_mp_mcq/";
    public String SYNC_PQ_MCQ = BASE + "sync_pq_mcq/";
    public String MT_MCQ = BASE + "mt_mcq/";
    public String ONLINE_MODEL_TEST = BASE + "get_a_model_test/";
    public String ONLINE_MODEL_TEST_LIST = BASE + "mt_subject/";
}
