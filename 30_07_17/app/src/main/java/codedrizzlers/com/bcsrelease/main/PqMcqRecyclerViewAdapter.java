package codedrizzlers.com.bcsrelease.main;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.turingtechnologies.materialscrollbar.ICustomAdapter;

import java.util.List;
import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.PqMcq;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class PqMcqRecyclerViewAdapter extends RecyclerView.Adapter<PqMcqRecyclerViewAdapter.ViewHolder> implements ICustomAdapter{

    private final List<PqMcq> mValues;
    private final FragmentInteractionListener mListener;

    public PqMcqRecyclerViewAdapter(List<PqMcq> items, FragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_mcq, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        String title = AppSingleTon.METHOD_BOX.convertNumericInBangla((position+1)+"");
        title = title+ ". " + mValues.get(position).getQuestion();

        holder.question.setText(title);
        holder.choiceOne.setText(mValues.get(position).getChoice_one());
        holder.choiceTwo.setText(mValues.get(position).getChoice_two());
        holder.choiceThree.setText(mValues.get(position).getChoice_three());
        holder.choiceFour.setText(mValues.get(position).getChoice_four());

        //this is need to set the text color to black
        holder.choiceOne.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.black ));
        holder.choiceTwo.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.black ));
        holder.choiceThree.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.black ));
        holder.choiceFour.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.black ));

        String answer = holder.mItem.getAnswer();

        if (answer.equals(holder.mItem.getChoice_one())) {
            holder.choiceOne.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.teal_500 ));
            holder.choiceOne.setChecked(true);

        } else if (answer.equals(holder.mItem.getChoice_two())) {
            holder.choiceTwo.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.teal_500 ));
            holder.choiceTwo.setChecked(true);

        } else if (answer.equals(holder.mItem.getChoice_three())) {
            holder.choiceThree.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.teal_500 ));
            holder.choiceThree.setChecked(true);

        } else {
            holder.choiceFour.setTextColor(ContextCompat.getColor(holder.mView.getContext(), R.color.teal_500 ));
            holder.choiceFour.setChecked(true);

        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });

        holder.choiceOne.setEnabled(false);
        holder.choiceTwo.setEnabled(false);
        holder.choiceThree.setEnabled(false);
        holder.choiceFour.setEnabled(false);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public String getCustomStringForElement(int element) {
        return ""+element;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView question;
        public PqMcq mItem;
        public RadioGroup radioGroup;
        public RadioButton choiceOne, choiceTwo, choiceThree, choiceFour;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            question = (TextView) view.findViewById(R.id.question);
            radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
            choiceOne = (RadioButton) view.findViewById(R.id.radioButton_choice_one);
            choiceTwo = (RadioButton) view.findViewById(R.id.radioButton_choice_two);
            choiceThree = (RadioButton) view.findViewById(R.id.radioButton_choice_three);
            choiceFour = (RadioButton) view.findViewById(R.id.radioButton_choice_four);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + question.getText() + "'";
        }
    }

    public interface FragmentInteractionListener {
        void onListFragmentInteraction(PqMcq item);
    }
}
