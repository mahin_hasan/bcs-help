package codedrizzlers.com.bcsrelease.main;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.QapSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SynchronousHandler;

public class QapSubjectListFragment extends BaseFragment {

    public static String TAG_HTTP_SUBJECT = "TAG_HTTP_SUBJECT";
    RecyclerView recyclerView;
    QapSubjectRecyclerViewAdapter adapter;
    private OnSubjectListFragmentInteractionListener mListener;

    public QapSubjectListFragment() {

    }

    public static QapSubjectListFragment newInstance() {
        QapSubjectListFragment fragment = new QapSubjectListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //showToast("onCreate QapSubjectListFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qap_subject_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //showToast("onActivityCreated QapSubjectListFragment");
        populateAdapter();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnSubjectListFragmentInteractionListener) {
            mListener = (OnSubjectListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }
    }

    public void populateAdapter() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                List<QapSubject> list = AppSingleTon.DB_MANAGER.daoSession.getQapSubjectDao().loadAll();
                ArrayList<QapSubject> arrayList = list.isEmpty() ? new ArrayList<QapSubject>() : (ArrayList<QapSubject>) list;
                adapter = new QapSubjectRecyclerViewAdapter(arrayList, mListener);
                recyclerView.setAdapter(adapter);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);

        //SynchronousHandler.postAndWait(handler,runnable);
    }

    MainActivity getMainActivity(){
        if(getActivity() instanceof MainActivity)
            return ((MainActivity)getActivity());
        return null;
    }

    public void refreshAdapter() {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                List<QapSubject> list = (List<QapSubject>) AppSingleTon.DB_MANAGER.daoSession.getQapSubjectDao().loadAll();
                adapter.mValues.clear();
                adapter.mValues.addAll(list);
                adapter.generaterSubtitleList();
                adapter.notifyDataSetChanged();
                Log.d("QapSubFragment","From refresh adapter runnable after notifydatasetchanged.");
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnSubjectListFragmentInteractionListener {
        void onListFragmentInteraction(int position);
    }

    @Override
    public void onDestroy() {
        AppSingleTon.ASYNC_HTTP_CLIENT.cancelRequestsByTAG(TAG_HTTP_SUBJECT, true);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
