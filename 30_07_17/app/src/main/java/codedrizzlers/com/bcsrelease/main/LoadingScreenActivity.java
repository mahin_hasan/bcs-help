package codedrizzlers.com.bcsrelease.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import codedrizzlers.com.bcsrelease.R;

/**
 * Created by code drizzlers on 6/25/2017.
 */

public class LoadingScreenActivity extends AppCompatActivity {
    //Introduce a delay
    private final int WAIT_TIME = 2500;
    private Handler uiHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHandler = new Handler(); // anything posted to this handler will run on the UI Thread
        System.out.println("LoadingScreenActivity  screen started");
        setContentView(R.layout.loading_screen_layout);
        findViewById(R.id.mainSpinner1).setVisibility(View.VISIBLE);

        final Runnable onUi = new Runnable() {
            @Override
            public void run() {
                // this will run on the main UI thread
                Intent mainIntent = new Intent(LoadingScreenActivity.this,MainActivity.class);
                LoadingScreenActivity.this.startActivity(mainIntent);
                LoadingScreenActivity.this.finish();
            }
        };

        Runnable background = new Runnable() {
            @Override
            public void run() {
                // This is the delay
                try {
                    Thread.sleep( WAIT_TIME );

                    // This will run on a background thread
                    //Simulating a long running task
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("Going to Profile Data");
                uiHandler.post( onUi );
            }
        };

        new Thread( background ).start();
    }
}
