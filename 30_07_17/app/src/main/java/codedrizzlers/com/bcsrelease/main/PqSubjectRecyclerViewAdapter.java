package codedrizzlers.com.bcsrelease.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.PqSubSubject;
import codedrizzlers.com.bcsrelease.database.PqSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.PqSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class PqSubjectRecyclerViewAdapter extends RecyclerView.Adapter<PqSubjectRecyclerViewAdapter.ViewHolder> {

    public ArrayList<PqSubject> mValues;
    private final PqSubjectListFragment.FragmentInteractionListener mListener;

    public PqSubjectRecyclerViewAdapter(ArrayList<PqSubject> items, PqSubjectListFragment.FragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_subject, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        String title = AppSingleTon.METHOD_BOX.convertNumericInBangla((position+1)+"");
        title = title+ ". " + mValues.get(position).getTitle();
        holder.mTitle.setText(title);

        PqSubSubjectDao subSubjectDao = AppSingleTon.DB_MANAGER.daoSession.getPqSubSubjectDao();
        QueryBuilder<PqSubSubject> qb = subSubjectDao.queryBuilder();
        qb.where(PqSubSubjectDao.Properties.Subject_remote_id.eq(mValues.get(position).getRemote_id()));

        String sub = holder.mView.getContext().getString(R.string.sub_subject);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(qb.list().size()+"");
        sub = sub + holder.mView.getContext().getString(R.string.ti);
        holder.mSubTitle.setText(sub);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitle, mSubTitle;
        public PqSubject mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitle = (TextView) view.findViewById(R.id.title);
            mSubTitle = (TextView) view.findViewById(R.id.sub_title);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }
}
