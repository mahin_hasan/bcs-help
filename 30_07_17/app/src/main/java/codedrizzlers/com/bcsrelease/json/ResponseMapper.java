package codedrizzlers.com.bcsrelease.json;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by goutom on 10/29/16.
 */

public class ResponseMapper {

    public class HeaderResponse {

        public ArrayList<codedrizzlers.com.bcsrelease.json.ResponseMapper.HeaderResponse.Subject> qap_subject, mp_subject, pq_subject;
        public ArrayList<codedrizzlers.com.bcsrelease.json.ResponseMapper.HeaderResponse.SubSubject> qap_sub_subject, mp_sub_subject, pq_sub_subject;
        public String last_scanned_date;

        public class Subject implements Serializable {
            public String title;
            public long id;
            public boolean is_deleted;
        }

        public class SubSubject implements Serializable {
            public String title;
            public long id, subject;
            public boolean is_deleted;
        }

        public class QuestionAnswer implements Serializable {
            public String question, answer;
            public long id, sub_subject;
            public boolean is_deleted;
        }

        public class Mcq implements Serializable {
            public long id, sub_subject;
            public String question, answer, choice_one, choice_two, choice_three, choice_four;
            public boolean is_deleted;
        }

    }

    public class OnlineModelTestResponse {

        public ArrayList<codedrizzlers.com.bcsrelease.json.ResponseMapper.OnlineModelTestResponse.Mcq> results;

        public class Mcq implements Serializable {
            public long id, subject;
            public String question, answer, choice_one, choice_two, choice_three, choice_four;
            public boolean is_deleted;
        }

    }

    public class QuestionAnswerResponse {

        public String last_scanned_date;
        public ArrayList<codedrizzlers.com.bcsrelease.json.ResponseMapper.QuestionAnswerResponse.QuestionAnswer> results;
        public int item_in_a_page, total_item, total_page, next_page, previous_page;

        public class QuestionAnswer implements Serializable {
            public String question, answer;
            public long id, sub_subject;
            public boolean is_deleted;
        }

    }

    public class McqResponse {

        public String last_scanned_date;
        public ArrayList<codedrizzlers.com.bcsrelease.json.ResponseMapper.McqResponse.Mcq> results;
        public int item_in_a_page, total_item, total_page, next_page, previous_page;

        public class Mcq implements Serializable {
            public long id, sub_subject;
            public String question, answer, choice_one, choice_two, choice_three, choice_four;
            public boolean is_deleted;
        }

    }

}


