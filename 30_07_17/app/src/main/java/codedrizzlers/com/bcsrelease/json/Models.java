package codedrizzlers.com.bcsrelease.json;

import java.io.Serializable;

/**
 * Created by goutom on 10/29/16.
 */

public class Models {

    public class Subject implements Serializable {
        public String title, date_added;
        public long id;
        public boolean is_deleted;
    }
}
