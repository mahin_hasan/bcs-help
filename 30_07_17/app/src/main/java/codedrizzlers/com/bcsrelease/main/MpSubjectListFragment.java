package codedrizzlers.com.bcsrelease.main;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.MpSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class MpSubjectListFragment extends BaseFragment {

    RecyclerView recyclerView;
    MpSubjectRecyclerViewAdapter adapter;

    private OnMpSubjectListFragmentInteractionListener mListener;

    public MpSubjectListFragment() {
    }


    public static MpSubjectListFragment newInstance() {
        MpSubjectListFragment fragment = new MpSubjectListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //showToast("onCreate MpSubjectListFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mp_subject_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //showToast("onActivityCreated MpSubjectListFragment");
        populateAdapter();
    }

    public void populateAdapter(){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                List<MpSubject> list = AppSingleTon.DB_MANAGER.daoSession.getMpSubjectDao().loadAll();
                ArrayList<MpSubject> arrayList = list.isEmpty() ? new ArrayList<MpSubject>() : (ArrayList<MpSubject>)list;
                adapter = new MpSubjectRecyclerViewAdapter(arrayList, mListener);
                recyclerView.setAdapter(adapter);
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);

    }

    public void refreshAdapter(){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try{
                ArrayList<MpSubject> list = (ArrayList<MpSubject>) AppSingleTon.DB_MANAGER.daoSession.getMpSubjectDao().loadAll();
                adapter.mValues.clear();
                adapter.mValues.addAll(list);
                adapter.notifyDataSetChanged();}
                catch (ClassCastException cce){
                    cce.printStackTrace();
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMpSubjectListFragmentInteractionListener) {
            mListener = (OnMpSubjectListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnMpSubjectListFragmentInteractionListener {
        void onListFragmentInteraction(MpSubject item);
    }
}
