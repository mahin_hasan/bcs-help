package codedrizzlers.com.bcsrelease.others.PAlert;

import android.content.Context;

import codedrizzlers.com.bcsrelease.main.MainActivity;
import codedrizzlers.com.bcsrelease.others.PAlert.interfaces.ResumeInterface;


/**
 * Created by code drizzlers on 2/8/2017.
 */

public class GPlusLikeResumeInterface implements ResumeInterface {
    MainActivity app;
    GooglePlusLikeAlert likeAlert;

    public GPlusLikeResumeInterface(Context con, GooglePlusLikeAlert alert) {
        setApp(con);
        setLikeAlert(alert);
        getApp().setgPlusLikeResumeInterface(this);
    }

    public MainActivity getApp() {
        return app;
    }

    public void setApp(Context con) {
        if (con instanceof MainActivity)
            this.app = ((MainActivity) con);
    }

    public GooglePlusLikeAlert getLikeAlert() {
        return likeAlert;
    }

    public void setLikeAlert(GooglePlusLikeAlert likeAlert) {
        this.likeAlert = likeAlert;
    }

    @Override
    public void onResumeContext() {
        likeAlert.pButtonOnResume();
    }
}
