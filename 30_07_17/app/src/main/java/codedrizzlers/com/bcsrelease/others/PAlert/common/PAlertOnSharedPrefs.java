package codedrizzlers.com.bcsrelease.others.PAlert.common;

import android.content.Context;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import static codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.internet.AccessNetworkJob.isNetworkConnected;


/**
 * Created by code drizzlers on 1/31/2017.
 */

public class PAlertOnSharedPrefs {
    Context context;
    private String sub,subBool;

    public PAlertOnSharedPrefs(Context context){
        setContext(context);
    }

    public PAlertOnSharedPrefs(Context context, String sub, String subBool){
        setContext(context);
        setSub(sub);
        setSubBool(subBool);
    }


    public void setSub(String sub) {
        this.sub = sub;
    }

    public void setSubBool(String subBool) {
        this.subBool = subBool;
    }

    public String getSub() {
        return sub;
    }

    public String getSubBool() {
        return subBool;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }



    private void updateTime() {
        PAlertUtil.putLongToPreferences(sub, getCurrentTime());
    }

    public boolean isTimeForAlert() {
        //Log.d("Rating", "lastTime : " + getLastAlertTime() + " currentTime : " + getCurrentTime());
        if (checkPrefsContains())
            if (getCurrentTime() - getLastAlertTime() > 0)
                if (isNetworkConnected())
                    if (prefsCheck())
                        return true;

        return false;

    }

    private long getCurrentTime() {
        return TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis());
    }

    private long getLastAlertTime() {
        long lastTime = PAlertUtil.getSharedPreferences().getLong(sub, getCurrentTime());

        return lastTime;
    }

    public boolean prompt() {
        try {
            checkInit();


            if (isTimeForAlert()) {
                updateTime();
                //showAlert();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private void checkInit(){
        checkPrefsContains();
    }


    public boolean prefsCheck() {
        if (!PAlertUtil.getSharedPreferences().getBoolean(subBool, false))
            return true;

        return false;
    }

    public boolean checkPrefsContains() {
        if (!PAlertUtil.getSharedPreferences().contains(sub)) {
            Log.d("Creating", "Before creating.");
            initializedPrefsRateKeys();
        }
        return true;
    }

    private void initializedPrefsRateKeys() {
        PAlertUtil.putBooleanToPreferences(subBool, false);
        PAlertUtil.putLongToPreferences(sub, getCurrentTime());
    }


}
