package codedrizzlers.com.bcsrelease.service.category;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.MpSubSubject;
import codedrizzlers.com.bcsrelease.database.MpSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.MpSubject;
import codedrizzlers.com.bcsrelease.database.MpSubjectDao;
import codedrizzlers.com.bcsrelease.database.PqSubSubject;
import codedrizzlers.com.bcsrelease.database.PqSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.PqSubject;
import codedrizzlers.com.bcsrelease.database.PqSubjectDao;
import codedrizzlers.com.bcsrelease.database.QapSubSubject;
import codedrizzlers.com.bcsrelease.database.QapSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.QapSubject;
import codedrizzlers.com.bcsrelease.database.QapSubjectDao;
import codedrizzlers.com.bcsrelease.json.ResponseMapper;
import codedrizzlers.com.bcsrelease.service.SyncService;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SPHandler;
import cz.msebera.android.httpclient.Header;

/**
 * Created by code drizzlers on 6/19/2017.
 */

public class CategoryHeaderSync extends Service {
    private static final String TAG_HTTP_HEADER_SYNC = "TAG_HTTP_HEADER_SYNC";
    public static final String ACTION_HEADER_SYNC_DONE = "ACTION_HEADER_SYNC_DONE";
    private static boolean isCompleted;

    public CategoryHeaderSync() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setIsCompleted(false);
        Log.d("HeaderSync","Calling to headerSynce()");
        headerSync();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void headerSync() {

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                Log.d("HeaderService","HeaderService is started.");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.d("syncHeaders onSuccess", "syncHeaders :" + response.toString());
                headerSyncOnSuccess(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("syncHeaders onFailure", "syncHeaders :" + errorResponse + "");
                Intent intent = new Intent(SyncService.FILTER_ACTION_SYNC_ERROR);
                sendBroadcast(intent);
                stopSelf();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        handler.setTag(TAG_HTTP_HEADER_SYNC);
        RequestParams params = new RequestParams();
        params.put(getString(R.string.last_scanned_date), SPHandler.getCategoryHeaderSyncDate());
        AppSingleTon.ASYNC_HTTP_CLIENT.post(CategoryHeaderSync.this, AppSingleTon.APP_URL.SYNC_HEADER, params, handler);
    }

    void headerSyncOnSuccess(final JSONObject response) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                ResponseMapper.HeaderResponse syncResponse = AppSingleTon.APP_JSON_PARSER.syncHeaderResponse(response);
                pushToQapSubject(syncResponse.qap_subject);
                pushToQapSubSubject(syncResponse.qap_sub_subject);
                pushToMpSubject(syncResponse.mp_subject);
                pushToMpSubSubject(syncResponse.mp_sub_subject);
                pushToPqSubject(syncResponse.pq_subject);
                pushToPqSubSubject(syncResponse.pq_sub_subject);
                stopService(syncResponse);
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);

    }

    void pushToQapSubject(final ArrayList<ResponseMapper.HeaderResponse.Subject> subjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                QapSubjectDao subjectDao = AppSingleTon.DB_MANAGER.daoSession.getQapSubjectDao();

                for (ResponseMapper.HeaderResponse.Subject subject : subjects) {

                    Log.d("SubjceDetails", subject.id + " and " + subject.title);
                    if (subjectDao == null) {
                        Log.d("subjectDao", "null");
                    }
                    QapSubject checkSubjectDb = subjectDao.load(subject.id);
                    if (checkSubjectDb == null) {

                        QapSubject subjectDb = new QapSubject(subject.id, subject.title);
                        subjectDao.insert(subjectDb);

                    } else {

                        if (subject.is_deleted == true) {
                            subjectDao.deleteByKey(subject.id);
                        } else {
                            checkSubjectDb.setTitle(subject.title);
                            subjectDao.update(checkSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToQapSubSubject(final ArrayList<ResponseMapper.HeaderResponse.SubSubject> subSubjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                QapSubSubjectDao subSubjectDao = AppSingleTon.DB_MANAGER.daoSession.getQapSubSubjectDao();

                for (ResponseMapper.HeaderResponse.SubSubject subSubject : subSubjects) {

                    QapSubSubject checkSubSubjectDb = subSubjectDao.load(subSubject.id);
                    if (checkSubSubjectDb == null) {

                        QapSubSubject subSubjectDb = new QapSubSubject(subSubject.id, subSubject.subject,
                                subSubject.title);
                        subSubjectDao.insert(subSubjectDb);

                    } else {

                        if (subSubject.is_deleted == true) {
                            subSubjectDao.deleteByKey(subSubject.id);
                        } else {
                            checkSubSubjectDb.setTitle(subSubject.title);
                            checkSubSubjectDb.setSubject_remote_id(subSubject.subject);
                            subSubjectDao.update(checkSubSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);

    }

    void pushToMpSubject(final ArrayList<ResponseMapper.HeaderResponse.Subject> subjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                MpSubjectDao subjectDao = AppSingleTon.DB_MANAGER.daoSession.getMpSubjectDao();

                for (ResponseMapper.HeaderResponse.Subject subject : subjects) {

                    MpSubject checkSubjectDb = subjectDao.load(subject.id);
                    if (checkSubjectDb == null) {

                        MpSubject subjectDb = new MpSubject(subject.id, subject.title);
                        subjectDao.insert(subjectDb);

                    } else {

                        if (subject.is_deleted == true) {
                            subjectDao.deleteByKey(subject.id);
                        } else {
                            checkSubjectDb.setTitle(subject.title);
                            subjectDao.update(checkSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToMpSubSubject(final ArrayList<ResponseMapper.HeaderResponse.SubSubject> subSubjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                MpSubSubjectDao subSubjectDao = AppSingleTon.DB_MANAGER.daoSession.getMpSubSubjectDao();

                for (ResponseMapper.HeaderResponse.SubSubject subSubject : subSubjects) {

                    MpSubSubject checkSubSubjectDb = subSubjectDao.load(subSubject.id);
                    if (checkSubSubjectDb == null) {

                        MpSubSubject subSubjectDb = new MpSubSubject(subSubject.id, subSubject.subject,
                                subSubject.title);
                        subSubjectDao.insert(subSubjectDb);

                    } else {

                        if (subSubject.is_deleted == true) {
                            subSubjectDao.deleteByKey(subSubject.id);
                        } else {
                            checkSubSubjectDb.setTitle(subSubject.title);
                            checkSubSubjectDb.setSubject_remote_id(subSubject.subject);
                            subSubjectDao.update(checkSubSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToPqSubject(final ArrayList<ResponseMapper.HeaderResponse.Subject> subjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                PqSubjectDao subjectDao = AppSingleTon.DB_MANAGER.daoSession.getPqSubjectDao();

                for (ResponseMapper.HeaderResponse.Subject subject : subjects) {

                    Log.d("PcqSubject",subject.id+" and "+subject.title);

                    PqSubject checkSubjectDb = subjectDao.load(subject.id);
                    if (checkSubjectDb == null) {

                        PqSubject subjectDb = new PqSubject(subject.id, subject.title);
                        subjectDao.insert(subjectDb);

                    } else {

                        if (subject.is_deleted == true) {
                            subjectDao.deleteByKey(subject.id);
                        } else {
                            checkSubjectDb.setTitle(subject.title);
                            subjectDao.update(checkSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToPqSubSubject(final ArrayList<ResponseMapper.HeaderResponse.SubSubject> subSubjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                PqSubSubjectDao subSubjectDao = AppSingleTon.DB_MANAGER.daoSession.getPqSubSubjectDao();

                for (ResponseMapper.HeaderResponse.SubSubject subSubject : subSubjects) {

                    Log.d("SubSubjectPqmcq",subSubject.id +" and "+subSubject.title);

                    PqSubSubject checkSubSubjectDb = subSubjectDao.load(subSubject.id);
                    if (checkSubSubjectDb == null) {

                        PqSubSubject subSubjectDb = new PqSubSubject(subSubject.id, subSubject.subject,
                                subSubject.title);
                        subSubjectDao.insert(subSubjectDb);

                    } else {

                        if (subSubject.is_deleted == true) {
                            subSubjectDao.deleteByKey(subSubject.id);
                        } else {
                            checkSubSubjectDb.setTitle(subSubject.title);
                            checkSubSubjectDb.setSubject_remote_id(subSubject.subject);
                            subSubjectDao.update(checkSubSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    public static boolean isCompleted() {
        return isCompleted;
    }

    public static void setIsCompleted(boolean isCompleted) {
        CategoryHeaderSync.isCompleted = isCompleted;
    }

    void stopService(ResponseMapper.HeaderResponse syncResponse){
        SPHandler.setCategoryHeaderSyncDate(syncResponse.last_scanned_date);
        setIsCompleted(true);
        Intent intent = new Intent(CategoryHeaderSync.ACTION_HEADER_SYNC_DONE);
        sendBroadcast(intent);
        stopSelf();
    }
}
