package codedrizzlers.com.bcsrelease.others.PAlert.managers;

import android.content.Context;

import codedrizzlers.com.bcsrelease.others.PAlert.GooglePlusLikeAlert;
import codedrizzlers.com.bcsrelease.others.PAlert.common.PAlertOnSharedPrefs;
import codedrizzlers.com.bcsrelease.others.PAlert.common.PAlertUtil;
import codedrizzlers.com.bcsrelease.others.PAlert.interfaces.PAlertManager;
import codedrizzlers.com.bcsrelease.others.PAlert.interfaces.PAlertSPBoolUpdate;


/**
 * Created by code drizzlers on 2/6/2017.
 */

public class GPlusPPAlertManager implements PAlertSPBoolUpdate,PAlertManager {
    Context context;
    PAlertOnSharedPrefs prefs;
    GooglePlusLikeAlert alert;

    public GPlusPPAlertManager(Context con){
        this.context = con;
        alert = new GooglePlusLikeAlert(context,this);
        prefs = new PAlertOnSharedPrefs(context,"g_plus_like","g_plus_liked");
    }

    public boolean showAlert(){
        if(prefs.prompt()){
            alert.showGPlusAlert();
            return true;
        }
        return false;
    }


    @Override
    public void updateBool() {
        updateBool("g_plus_liked",true);
    }

    @Override
    public void updateBool(String bool, boolean b) {
        PAlertUtil.putBooleanToPreferences(bool,b);
    }
}
