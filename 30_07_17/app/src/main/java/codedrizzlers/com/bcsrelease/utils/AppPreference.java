package codedrizzlers.com.bcsrelease.utils;

import android.content.Context;
import android.content.SharedPreferences;

import codedrizzlers.com.bcsrelease.R;

public class AppPreference {

	SharedPreferences preference;
	SharedPreferences.Editor preferenceEditor;
	String prefsFileName = "APPLICATION_SHARED_PFERENCE";
	String LAST_SCANNED_DATE = "LAST_SCANNED_DATE";
	String QAP_TUTORIAL = "QAP_TUTORIAL";
	String TOKEN = "TOKEN";

	public AppPreference(){
		preference = AppSingleTon.CONTEXT.getSharedPreferences(prefsFileName, Context.MODE_PRIVATE);
		preferenceEditor = preference.edit();
	}

	public void setLastScannedDate(String lastScannedDate){
		preferenceEditor.putString(LAST_SCANNED_DATE, lastScannedDate);
		preferenceEditor.commit();
	}

	public String getLastScannedDate(){
		return preference.getString(LAST_SCANNED_DATE, AppSingleTon.CONTEXT.getString(R.string.last_scanned_date_date));
	}

	public void setToken(String token){
		preferenceEditor.putString(TOKEN, token);
		preferenceEditor.commit();
	}

	public String getToken(){
		return preference.getString(TOKEN, null);
	}

	public void setQapTutorial(boolean flag){
		preferenceEditor.putBoolean(QAP_TUTORIAL, flag);
		preferenceEditor.commit();
	}

	public boolean getQapTutorial(){
		return preference.getBoolean(QAP_TUTORIAL, false);
	}
}
