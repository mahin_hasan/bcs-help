package codedrizzlers.com.bcsrelease.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.greendao.query.QueryBuilder;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.MpSubSubject;
import codedrizzlers.com.bcsrelease.database.MpSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.MpSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

import java.util.List;

public class MpSubjectListRecyclerViewAdapter extends RecyclerView.Adapter<MpSubjectListRecyclerViewAdapter.ViewHolder> {

    private final List<MpSubject> mValues;
    private final MpSubjectListFragment.OnMpSubjectListFragmentInteractionListener mListener;

    public MpSubjectListRecyclerViewAdapter(List<MpSubject> items, MpSubjectListFragment.OnMpSubjectListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_subject, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        String title = AppSingleTon.METHOD_BOX.convertNumericInBangla((position+1)+"");
        title = title+ ". " + mValues.get(position).getTitle();
        holder.mTitle.setText(title);

        MpSubSubjectDao subSubjectDao = AppSingleTon.DB_MANAGER.daoSession.getMpSubSubjectDao();
        QueryBuilder<MpSubSubject> qb = subSubjectDao.queryBuilder();
        qb.where(MpSubSubjectDao.Properties.Subject_remote_id.eq(mValues.get(position).getRemote_id()));

        String sub = holder.mView.getContext().getString(R.string.sub_subject);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(qb.list().size()+"");
        sub = sub + holder.mView.getContext().getString(R.string.ti);
        holder.mSubTitle.setText(sub);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitle, mSubTitle;
        public MpSubject mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitle = (TextView) view.findViewById(R.id.title);
            mSubTitle = (TextView) view.findViewById(R.id.sub_title);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }
}
