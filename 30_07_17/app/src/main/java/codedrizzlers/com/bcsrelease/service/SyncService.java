package codedrizzlers.com.bcsrelease.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.MpMcq;
import codedrizzlers.com.bcsrelease.database.MpMcqDao;
import codedrizzlers.com.bcsrelease.database.MpSubSubject;
import codedrizzlers.com.bcsrelease.database.MpSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.MpSubject;
import codedrizzlers.com.bcsrelease.database.MpSubjectDao;
import codedrizzlers.com.bcsrelease.database.PqMcq;
import codedrizzlers.com.bcsrelease.database.PqMcqDao;
import codedrizzlers.com.bcsrelease.database.PqSubSubject;
import codedrizzlers.com.bcsrelease.database.PqSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.PqSubject;
import codedrizzlers.com.bcsrelease.database.PqSubjectDao;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswer;
import codedrizzlers.com.bcsrelease.database.QapQuestionAnswerDao;
import codedrizzlers.com.bcsrelease.database.QapSubSubject;
import codedrizzlers.com.bcsrelease.database.QapSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.QapSubject;
import codedrizzlers.com.bcsrelease.database.QapSubjectDao;
import codedrizzlers.com.bcsrelease.json.ResponseMapper.HeaderResponse;
import codedrizzlers.com.bcsrelease.json.ResponseMapper.McqResponse;
import codedrizzlers.com.bcsrelease.json.ResponseMapper.QuestionAnswerResponse;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import cz.msebera.android.httpclient.Header;

public class SyncService extends Service {

    private static final String TAG_HTTP_PQ_MCQ_SYNC = "TAG_HTTP_PQ_MCQ_SYNC";
    private static final String TAG_HTTP_MP_MCQ_SYNC = "TAG_HTTP_MP_MCQ_SYNC";

    public static final String TAG_HTTP_HEADER_SYNC = "TAG_HTTP_HEADER_SYNC";
    public static final String TAG_HTTP_QUESTION_ANSWER_SYNC = "TAG_HTTP_QUESTION_ANSWER_SYNC";
    public static final String TAG_MCQ_ANSWER_SYNC = "TAG_MCQ_ANSWER_SYNC";

    public static String FILTER_ACTION_SYNC_DONE = "FILTER_ACTION_SYNC_DONE";
    public static String FILTER_ACTION_SYNC_ERROR = "FILTER_ACTION_ERROR";

    public SyncService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        headerSync();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void headerSync() {

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.d("syncHeaders onSuccess", "syncHeaders :" + response.toString());
                headerSyncOnSuccess(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("syncHeaders onFailure", "syncHeaders :" + errorResponse + "");
                Intent intent = new Intent(SyncService.FILTER_ACTION_SYNC_ERROR);
                sendBroadcast(intent);
                stopSelf();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        handler.setTag(TAG_HTTP_HEADER_SYNC);
        RequestParams params = new RequestParams();
        params.put(getString(R.string.last_scanned_date), AppSingleTon.SHARED_PREFERENCE.getLastScannedDate());
        AppSingleTon.ASYNC_HTTP_CLIENT.post(SyncService.this, AppSingleTon.APP_URL.SYNC_HEADER, params, handler);
    }

    public void questionAnswerSync(int page) {

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.d("qa onSuccess", "syncHeaders :" + response.toString());
                questionAnswerSyncOnSuccess(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("syncHeaders onFailure", "syncHeaders :" + errorResponse + "");
                Intent intent = new Intent(SyncService.FILTER_ACTION_SYNC_ERROR);
                sendBroadcast(intent);
                stopSelf();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        handler.setTag(TAG_HTTP_QUESTION_ANSWER_SYNC);
        RequestParams params = new RequestParams();
        params.put(getString(R.string.page), page);
        params.put(getString(R.string.last_scanned_date), AppSingleTon.SHARED_PREFERENCE.getLastScannedDate());
        AppSingleTon.ASYNC_HTTP_CLIENT.post(SyncService.this, AppSingleTon.APP_URL.SYNC_QUESTION_ANSWER, params, handler);
    }

    public void mpMcqSync(int page) {

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.d("qa onSuccess", "syncHeaders :" + response.toString());
                mpMcqSyncOnSuccess(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("syncHeaders onFailure", "syncHeaders :" + errorResponse + "");
                Intent intent = new Intent(SyncService.FILTER_ACTION_SYNC_ERROR);
                sendBroadcast(intent);
                stopSelf();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        handler.setTag(TAG_HTTP_MP_MCQ_SYNC);
        RequestParams params = new RequestParams();
        params.put(getString(R.string.page), page);
        params.put(getString(R.string.last_scanned_date), AppSingleTon.SHARED_PREFERENCE.getLastScannedDate());
        AppSingleTon.ASYNC_HTTP_CLIENT.post(SyncService.this, AppSingleTon.APP_URL.SYNC_MP_MCQ, params, handler);
    }

    public void pqMcqSync(int page) {

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.d("qa onSuccess", "syncHeaders :" + response.toString());
                pqMcqSyncOnSuccess(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("syncHeaders onFailure", "syncHeaders :" + errorResponse + "");
                Intent intent = new Intent(SyncService.FILTER_ACTION_SYNC_ERROR);
                sendBroadcast(intent);
                stopSelf();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        handler.setTag(TAG_HTTP_PQ_MCQ_SYNC);
        RequestParams params = new RequestParams();
        params.put(getString(R.string.page), page);
        params.put(getString(R.string.last_scanned_date), AppSingleTon.SHARED_PREFERENCE.getLastScannedDate());
        AppSingleTon.ASYNC_HTTP_CLIENT.post(SyncService.this, AppSingleTon.APP_URL.SYNC_PQ_MCQ, params, handler);
    }

    void headerSyncOnSuccess(final JSONObject response) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                HeaderResponse syncResponse = AppSingleTon.APP_JSON_PARSER.syncHeaderResponse(response);
                pushToQapSubject(syncResponse.qap_subject);
                pushToQapSubSubject(syncResponse.qap_sub_subject);
                pushToMpSubject(syncResponse.mp_subject);
                pushToMpSubSubject(syncResponse.mp_sub_subject);
                pushToPqSubject(syncResponse.pq_subject);
                pushToPqSubSubject(syncResponse.pq_sub_subject);
                questionAnswerSync(1);

            }
        };
        Handler handler = new Handler();
        handler.post(runnable);

    }

    void questionAnswerSyncOnSuccess(final JSONObject response) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                QuestionAnswerResponse syncResponse = AppSingleTon.APP_JSON_PARSER.syncQuestionAnswerResponse(response);
                pushToQapQuestionAnswer(syncResponse.results);

                if (syncResponse.next_page > 0) {
                    questionAnswerSync(syncResponse.next_page);
                } else {
                    mpMcqSync(1);
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);

    }

    void mpMcqSyncOnSuccess(final JSONObject response) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                McqResponse syncResponse = AppSingleTon.APP_JSON_PARSER.syncMcqResponse(response);
                pushToMpMcq(syncResponse.results);

                if (syncResponse.next_page > 0) {
                    mpMcqSync(syncResponse.next_page);
                } else {
                    pqMcqSync(1);
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);



        McqResponse syncResponse = AppSingleTon.APP_JSON_PARSER.syncMcqResponse(response);
        pushToMpMcq(syncResponse.results);

        if (syncResponse.next_page > 0) {
            mpMcqSync(syncResponse.next_page);
        } else {
            pqMcqSync(1);
        }
    }

    void pqMcqSyncOnSuccess(JSONObject response) {

        McqResponse syncResponse = AppSingleTon.APP_JSON_PARSER.syncMcqResponse(response);
        pushToPqMcq(syncResponse.results);


        if (syncResponse.next_page > 0) {
            pqMcqSync(syncResponse.next_page);
        } else {

            AppSingleTon.SHARED_PREFERENCE.setLastScannedDate(syncResponse.last_scanned_date);
            Intent intent = new Intent(SyncService.FILTER_ACTION_SYNC_DONE);
            sendBroadcast(intent);
            stopSelf();
        }
    }


    void pushToQapSubject(final ArrayList<HeaderResponse.Subject> subjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                QapSubjectDao subjectDao = AppSingleTon.DB_MANAGER.daoSession.getQapSubjectDao();

                for (HeaderResponse.Subject subject : subjects) {

                    Log.d("SubjceDetails", subject.id + " and " + subject.title);
                    if (subjectDao == null) {
                        Log.d("subjectDao", "null");
                    }
                    QapSubject checkSubjectDb = subjectDao.load(subject.id);
                    if (checkSubjectDb == null) {

                        QapSubject subjectDb = new QapSubject(subject.id, subject.title);
                        subjectDao.insert(subjectDb);

                    } else {

                        if (subject.is_deleted == true) {
                            subjectDao.deleteByKey(subject.id);
                        } else {
                            checkSubjectDb.setTitle(subject.title);
                            subjectDao.update(checkSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToQapSubSubject(final ArrayList<HeaderResponse.SubSubject> subSubjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                QapSubSubjectDao subSubjectDao = AppSingleTon.DB_MANAGER.daoSession.getQapSubSubjectDao();

                for (HeaderResponse.SubSubject subSubject : subSubjects) {

                    QapSubSubject checkSubSubjectDb = subSubjectDao.load(subSubject.id);
                    if (checkSubSubjectDb == null) {

                        QapSubSubject subSubjectDb = new QapSubSubject(subSubject.id, subSubject.subject,
                                subSubject.title);
                        subSubjectDao.insert(subSubjectDb);

                    } else {

                        if (subSubject.is_deleted == true) {
                            subSubjectDao.deleteByKey(subSubject.id);
                        } else {
                            checkSubSubjectDb.setTitle(subSubject.title);
                            checkSubSubjectDb.setSubject_remote_id(subSubject.subject);
                            subSubjectDao.update(checkSubSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);

    }

    void pushToMpSubject(final ArrayList<HeaderResponse.Subject> subjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                MpSubjectDao subjectDao = AppSingleTon.DB_MANAGER.daoSession.getMpSubjectDao();

                for (HeaderResponse.Subject subject : subjects) {

                    MpSubject checkSubjectDb = subjectDao.load(subject.id);
                    if (checkSubjectDb == null) {

                        MpSubject subjectDb = new MpSubject(subject.id, subject.title);
                        subjectDao.insert(subjectDb);

                    } else {

                        if (subject.is_deleted == true) {
                            subjectDao.deleteByKey(subject.id);
                        } else {
                            checkSubjectDb.setTitle(subject.title);
                            subjectDao.update(checkSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToMpSubSubject(final ArrayList<HeaderResponse.SubSubject> subSubjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                MpSubSubjectDao subSubjectDao = AppSingleTon.DB_MANAGER.daoSession.getMpSubSubjectDao();

                for (HeaderResponse.SubSubject subSubject : subSubjects) {

                    MpSubSubject checkSubSubjectDb = subSubjectDao.load(subSubject.id);
                    if (checkSubSubjectDb == null) {

                        MpSubSubject subSubjectDb = new MpSubSubject(subSubject.id, subSubject.subject,
                                subSubject.title);
                        subSubjectDao.insert(subSubjectDb);

                    } else {

                        if (subSubject.is_deleted == true) {
                            subSubjectDao.deleteByKey(subSubject.id);
                        } else {
                            checkSubSubjectDb.setTitle(subSubject.title);
                            checkSubSubjectDb.setSubject_remote_id(subSubject.subject);
                            subSubjectDao.update(checkSubSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToPqSubject(final ArrayList<HeaderResponse.Subject> subjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                PqSubjectDao subjectDao = AppSingleTon.DB_MANAGER.daoSession.getPqSubjectDao();

                for (HeaderResponse.Subject subject : subjects) {

                    Log.d("PcqSubject",subject.id+" and "+subject.title);

                    PqSubject checkSubjectDb = subjectDao.load(subject.id);
                    if (checkSubjectDb == null) {

                        PqSubject subjectDb = new PqSubject(subject.id, subject.title);
                        subjectDao.insert(subjectDb);

                    } else {

                        if (subject.is_deleted == true) {
                            subjectDao.deleteByKey(subject.id);
                        } else {
                            checkSubjectDb.setTitle(subject.title);
                            subjectDao.update(checkSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToPqSubSubject(final ArrayList<HeaderResponse.SubSubject> subSubjects) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                PqSubSubjectDao subSubjectDao = AppSingleTon.DB_MANAGER.daoSession.getPqSubSubjectDao();

                for (HeaderResponse.SubSubject subSubject : subSubjects) {

                    Log.d("SubSubjectPqmcq",subSubject.id +" and "+subSubject.title);

                    PqSubSubject checkSubSubjectDb = subSubjectDao.load(subSubject.id);
                    if (checkSubSubjectDb == null) {

                        PqSubSubject subSubjectDb = new PqSubSubject(subSubject.id, subSubject.subject,
                                subSubject.title);
                        subSubjectDao.insert(subSubjectDb);

                    } else {

                        if (subSubject.is_deleted == true) {
                            subSubjectDao.deleteByKey(subSubject.id);
                        } else {
                            checkSubSubjectDb.setTitle(subSubject.title);
                            checkSubSubjectDb.setSubject_remote_id(subSubject.subject);
                            subSubjectDao.update(checkSubSubjectDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToQapQuestionAnswer(final ArrayList<QuestionAnswerResponse.QuestionAnswer> questionAnswers) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                QapQuestionAnswerDao questionAnswerDao = AppSingleTon.DB_MANAGER.daoSession.getQapQuestionAnswerDao();

                for (QuestionAnswerResponse.QuestionAnswer questionAnswer : questionAnswers) {

                    QapQuestionAnswer checkQuestionAnswerDb = questionAnswerDao.load(questionAnswer.id);

                    if (checkQuestionAnswerDb == null) {

                        QapQuestionAnswer questionAnswerDb = new QapQuestionAnswer(questionAnswer.id,
                                questionAnswer.sub_subject, questionAnswer.is_deleted,
                                questionAnswer.question, questionAnswer.answer);
                        questionAnswerDao.insert(questionAnswerDb);

                    } else {

                        if (questionAnswer.is_deleted == true) {
                            questionAnswerDao.deleteByKey(questionAnswer.id);
                        } else {
                            checkQuestionAnswerDb.setSub_subject_remote_id(questionAnswer.sub_subject);
                            checkQuestionAnswerDb.setAnswer(questionAnswer.answer);
                            checkQuestionAnswerDb.setQuestion(questionAnswer.question);
                            questionAnswerDao.update(checkQuestionAnswerDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToMpMcq(final ArrayList<McqResponse.Mcq> mcqs) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                MpMcqDao mcqDao = AppSingleTon.DB_MANAGER.daoSession.getMpMcqDao();
                for (McqResponse.Mcq mcq : mcqs) {

                    MpMcq checkMcqDb = mcqDao.load(mcq.id);
                    if (checkMcqDb == null) {

                        MpMcq mcqDb = new MpMcq();
                        mcqDb.setRemote_id(mcq.id);
                        mcqDb.setSub_subject_remote_id(mcq.sub_subject);
                        mcqDb.setQuestion(mcq.question);
                        mcqDb.setAnswer(mcq.answer);
                        mcqDb.setChoice_one(mcq.choice_one);
                        mcqDb.setChoice_two(mcq.choice_two);
                        mcqDb.setChoice_three(mcq.choice_three);
                        mcqDb.setChoice_four(mcq.choice_four);
                        mcqDb.setDone(false);
                        mcqDao.insert(mcqDb);
                    } else {

                        if (mcq.is_deleted == true) {
                            mcqDao.deleteByKey(mcq.id);
                        } else {
                            checkMcqDb.setSub_subject_remote_id(mcq.sub_subject);
                            checkMcqDb.setQuestion(mcq.question);
                            checkMcqDb.setAnswer(mcq.answer);
                            checkMcqDb.setChoice_one(mcq.choice_one);
                            checkMcqDb.setChoice_two(mcq.choice_two);
                            checkMcqDb.setChoice_three(mcq.choice_three);
                            checkMcqDb.setChoice_four(mcq.choice_four);
                            mcqDao.update(checkMcqDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void pushToPqMcq(final ArrayList<McqResponse.Mcq> mcqs) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                PqMcqDao mcqDao = AppSingleTon.DB_MANAGER.daoSession.getPqMcqDao();

                for (McqResponse.Mcq mcq : mcqs) {
                    if(mcq.sub_subject == 87)
                        Log.d("PqQuestion",mcq.question);

                    PqMcq checkMcqDb = mcqDao.load(mcq.id);
                    if (checkMcqDb == null) {

                        PqMcq mcqDb = new PqMcq();
                        mcqDb.setRemote_id(mcq.id);
                        mcqDb.setSub_subject_remote_id(mcq.sub_subject);
                        mcqDb.setQuestion(mcq.question);
                        mcqDb.setAnswer(mcq.answer);
                        mcqDb.setChoice_one(mcq.choice_one);
                        mcqDb.setChoice_two(mcq.choice_two);
                        mcqDb.setChoice_three(mcq.choice_three);
                        mcqDb.setChoice_four(mcq.choice_four);
                        mcqDb.setDone(false);
                        mcqDao.insert(mcqDb);
                    } else {

                        if (mcq.is_deleted == true) {
                            mcqDao.deleteByKey(mcq.id);
                        } else {
                            checkMcqDb.setSub_subject_remote_id(mcq.sub_subject);
                            checkMcqDb.setQuestion(mcq.question);
                            checkMcqDb.setAnswer(mcq.answer);
                            checkMcqDb.setChoice_one(mcq.choice_one);
                            checkMcqDb.setChoice_two(mcq.choice_two);
                            checkMcqDb.setChoice_three(mcq.choice_three);
                            checkMcqDb.setChoice_four(mcq.choice_four);
                            mcqDao.update(checkMcqDb);
                        }
                    }
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }
}

    /*
    void pushToMpMcq(SyncResponse.Mcqs mcqs){

        MpMcqDao mcqDao = AppSingleTon.DB_MANAGER.daoSession.getMpMcqDao();
        StatusDao statusDao = AppSingleTon.DB_MANAGER.daoSession.getStatusDao();

        for(SyncResponse.Mcq mcq : mcqs.added){

            MpMcq mcqDb = new MpMcq();
            mcqDb.setRemote_id(mcq.id);
            mcqDb.setSub_subject_remote_id(mcq.sub_subject);
            mcqDb.setQuestion(mcq.question);
            mcqDb.setAnswer(mcq.answer);
            mcqDb.setChoice_one(mcq.choice_one);
            mcqDb.setChoice_two(mcq.choice_two);
            mcqDb.setChoice_three(mcq.choice_three);
            mcqDb.setChoice_four(mcq.choice_four);
            mcqDb.setRow_version(mcq.row_version);
            mcqDao.insert(mcqDb);

            Status statusDb = new Status();
            statusDb.setContent_type_id(contentType.mp_mcq);
            statusDb.setObject_id(mcq.id);
            statusDb.setRow_version(mcq.row_version);
            statusDao.insert(statusDb);
        }

        for(SyncResponse.Mcq mcq : mcqs.edited){

            MpMcq mcqDb = new MpMcq();
            mcqDb.setSub_subject_remote_id(mcq.sub_subject);
            mcqDb.setQuestion(mcq.question);
            mcqDb.setAnswer(mcq.answer);
            mcqDb.setChoice_one(mcq.choice_one);
            mcqDb.setChoice_two(mcq.choice_two);
            mcqDb.setChoice_three(mcq.choice_three);
            mcqDb.setChoice_four(mcq.choice_four);
            mcqDb.setRow_version(mcq.row_version);
            mcqDao.update(mcqDb);

            QueryBuilder<Status> qb = statusDao.queryBuilder();
            qb.where(StatusDao.Properties.Content_type_id.eq(contentType.mp_mcq),
                    StatusDao.Properties.Object_id.eq(mcq.id));
            List<Status> statusList = qb.list();
            if(statusList.size() > 0){
                Status temp = statusList.get(0);
                temp.setRow_version(mcq.row_version);
                statusDao.update(temp);
            }
        }

        for(SyncResponse.Status status : mcqs.deleted){
            statusDao.deleteByKey(status.id);
            mcqDao.deleteByKey(status.object_id);
        }
    }


    void pushToPqMcq(SyncResponse.Mcqs mcqs){

        PqMcqDao mcqDao = AppSingleTon.DB_MANAGER.daoSession.getPqMcqDao();
        StatusDao statusDao = AppSingleTon.DB_MANAGER.daoSession.getStatusDao();

        for(SyncResponse.Mcq mcq : mcqs.added){

            PqMcq mcqDb = new PqMcq();
            mcqDb.setRemote_id(mcq.id);
            mcqDb.setSub_subject_remote_id(mcq.sub_subject);
            mcqDb.setQuestion(mcq.question);
            mcqDb.setAnswer(mcq.answer);
            mcqDb.setChoice_one(mcq.choice_one);
            mcqDb.setChoice_two(mcq.choice_two);
            mcqDb.setChoice_three(mcq.choice_three);
            mcqDb.setChoice_four(mcq.choice_four);
            mcqDb.setRow_version(mcq.row_version);
            mcqDao.insert(mcqDb);

            Status statusDb = new Status();
            statusDb.setContent_type_id(contentType.pq_mcq);
            statusDb.setObject_id(mcq.id);
            statusDb.setRow_version(mcq.row_version);
            statusDao.insert(statusDb);
        }

        for(SyncResponse.Mcq mcq : mcqs.edited){

            PqMcq mcqDb = new PqMcq();
            mcqDb.setSub_subject_remote_id(mcq.sub_subject);
            mcqDb.setQuestion(mcq.question);
            mcqDb.setAnswer(mcq.answer);
            mcqDb.setChoice_one(mcq.choice_one);
            mcqDb.setChoice_two(mcq.choice_two);
            mcqDb.setChoice_three(mcq.choice_three);
            mcqDb.setChoice_four(mcq.choice_four);
            mcqDb.setRow_version(mcq.row_version);
            mcqDao.update(mcqDb);

            QueryBuilder<Status> qb = statusDao.queryBuilder();
            qb.where(StatusDao.Properties.Content_type_id.eq(contentType.pq_mcq),
                    StatusDao.Properties.Object_id.eq(mcq.id));
            List<Status> statusList = qb.list();
            if(statusList.size() > 0){
                Status temp = statusList.get(0);
                temp.setRow_version(mcq.row_version);
                statusDao.update(temp);
            }
        }

        for(SyncResponse.Status status : mcqs.deleted){
            statusDao.deleteByKey(status.id);
            mcqDao.deleteByKey(status.object_id);
        }
    }
*/


