package codedrizzlers.com.bcsrelease.utils;

import android.util.Log;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.main.MainActivity;
import codedrizzlers.com.bcsrelease.service.category.CategoryHeaderSync;
import codedrizzlers.com.bcsrelease.service.category.MCQuestionService;
import codedrizzlers.com.bcsrelease.service.category.PreparationQuestionSyncService;
import codedrizzlers.com.bcsrelease.service.category.PreviousQuestionSyncService;

/**
 * Created by code drizzlers on 6/20/2017.
 */

public class StateUpdateAll {
    MainActivity activity;

    private boolean isUpdateRunning = false;
    private boolean isHeaderSyncDone = false;
    private boolean isPreparationSyncDone = false;
    private boolean isMCQSyncDone = false;
    private boolean isPreviousSynceDone = false;

    public StateUpdateAll(MainActivity act){
        this.activity = act;
    }

    public void stateInit(){
        isUpdateRunning = true;
        activity.startService(R.string.update_header, CategoryHeaderSync.class);
    }

    public void stateEnd(String actionDone) {
        switch (actionDone) {
            case CategoryHeaderSync.ACTION_HEADER_SYNC_DONE:
                isHeaderSyncDone = true;
                sateChange();
                break;
            case PreparationQuestionSyncService.ACTION_SYNC_PREPARATION_QUESTION:
                isPreparationSyncDone = true;
                sateChange();
                break;
            case MCQuestionService.ACTION_MCQ_QUESTION_SYNC:
                isMCQSyncDone = true;
                sateChange();
                break;
            case PreviousQuestionSyncService.ACTION_SYNC_PREVIOIUS_QUESTION:
                isPreviousSynceDone = true;
                sateChange();
                break;
        }
    }

    private void sateChange() {
        if (isUpdateRunning) {
            if (!isHeaderSyncDone) {
                activity.startService(R.string.update_header,CategoryHeaderSync.class);
            } else if (!isPreparationSyncDone) {
                Log.d("StateUpdate","Preparation question updating.");
                activity.startService(R.string.update_preparation_question,PreparationQuestionSyncService.class);
            } else if (!isMCQSyncDone) {
                Log.d("StateUpdate","MCQ service is about to start.");
                activity.startService(R.string.update_mcq_question,MCQuestionService.class);
            } else if (!isPreviousSynceDone) {
                activity.startService(R.string.update_previous_question,PreviousQuestionSyncService.class);
            } else if (isHeaderSyncDone && isMCQSyncDone && isPreparationSyncDone && isPreviousSynceDone) {
                activity.setIsUpdateRunning(false);
                resetState();
            }
        }
    }

    private void resetState() {
        isUpdateRunning = false;
        isHeaderSyncDone = false;
        isPreparationSyncDone = false;
        isPreviousSynceDone = false;
        isMCQSyncDone = false;
    }
}
