package codedrizzlers.com.bcsrelease.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import java.util.List;
import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.QapSubSubject;
import codedrizzlers.com.bcsrelease.database.QapSubSubjectDao;
import codedrizzlers.com.bcsrelease.database.QapSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SynchronousHandler;

public class QapSubSubjectActivity extends BaseActivity  implements QapSubSubjectRecyclerViewAdapter.OnSubSubjectListFragmentInteractionListener {

    Toolbar toolbar;
    QapSubject subject;
    ActionBar actionBar;
    RecyclerView recyclerView;
    QapSubSubjectRecyclerViewAdapter adapter;
    int ACTIVITY_REQUEST_CODE = 100;
    int position ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_subject);
        initToolbar();
        initParse();
        initTitle();
        initRecyclerView();

        //init ad view
        initAdView();
    }

    void initToolbar(){

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    void initParse(){

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            subject = (QapSubject) bundle.getSerializable(getString(R.string.subject));
            position = bundle.getInt(getString(R.string.position), 0);
        }

    }

    void initTitle(){
        actionBar.setTitle(subject.getTitle());
    }

    void initSubTitle(int total){

        String sub = getString(R.string.sub_subject);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(total+"");
        sub = sub + getString(R.string.ti);

        int totalItem = 0, numOfDone = 0;

        for(QapSubSubjectRecyclerViewAdapter.DoneMonitor doneMonitor : adapter.doneMonitorArrayList){
            totalItem+=doneMonitor.listSize;
            numOfDone+=doneMonitor.numOfDone;
        }
        Log.d("numberofdone totalitem", numOfDone+" "+totalItem+" "+adapter.doneMonitorArrayList.size());
        sub = sub+ " - "+generateCompletePercentage(numOfDone, totalItem, this);
        actionBar.setSubtitle(sub);
    }

    String generateCompletePercentage(int numOfDone, int listSize, Context context){

        if(numOfDone == 0){
            String str = "0% "+ context.getString(R.string.complete);
            return str;
        }
        int complete = (numOfDone*100)/listSize;
        String str = AppSingleTon.METHOD_BOX.convertNumericInBangla(complete+"")+"% "+ context.getString(R.string.complete);
        return str;
    }

    void initRecyclerView(){

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                List<QapSubSubject> list = AppSingleTon.DB_MANAGER.daoSession.getQapSubSubjectDao().queryBuilder().
                        where(QapSubSubjectDao.Properties.Subject_remote_id.eq(subject.getRemote_id())).list();

                adapter = new QapSubSubjectRecyclerViewAdapter(list, (QapSubSubjectRecyclerViewAdapter.OnSubSubjectListFragmentInteractionListener)QapSubSubjectActivity.this);
                recyclerView.setAdapter(adapter);
            }
        };
        SynchronousHandler.postAndWait(new Handler(),runnable);
        //initSubTitle(adapter.mValues.size());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == android.R.id.home){
            setResult();
            finish();
            return true;
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            setResult();
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    void setResult(){
        Intent intent = new Intent();
        intent.putExtra(getString(R.string.position), position);
        setResult(Activity.RESULT_OK, intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ACTIVITY_REQUEST_CODE) {

            int position = data.getIntExtra(getString(R.string.position), 0);
            int numOfDone = data.getIntExtra(getString(R.string.num_of_done), 0);
            adapter.doneMonitorArrayList.get(position).numOfDone = numOfDone;
            adapter.notifyItemChanged(position);
            initSubTitle(adapter.mValues.size());
        }
    }

    @Override
    public void onListFragmentInteraction(int position) {
        Intent intent = new Intent(this, QapQuestionAnswerActivity.class);
        intent.putExtra(getString(R.string.sub_subject), adapter.mValues.get(position));
        intent.putExtra(getString(R.string.position), position);
        startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void nowGenerateTotalPercentage() {
        initSubTitle(adapter.mValues.size());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
