package codedrizzlers.com.bcsrelease.json;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class AppJsonParser {


    public ResponseMapper.HeaderResponse syncHeaderResponse(JSONObject response) {
        Gson gson = new Gson();
        ResponseMapper.HeaderResponse orm = gson.fromJson(response.toString(), ResponseMapper.HeaderResponse.class);
        return orm;
    }

    public ResponseMapper.QuestionAnswerResponse syncQuestionAnswerResponse(JSONObject response) {
        Gson gson = new Gson();
        ResponseMapper.QuestionAnswerResponse orm = gson.fromJson(response.toString(), ResponseMapper.QuestionAnswerResponse.class);
        return orm;
    }

    public ResponseMapper.McqResponse syncMcqResponse(JSONObject response) {
        Gson gson = new Gson();
        ResponseMapper.McqResponse orm = gson.fromJson(response.toString(), ResponseMapper.McqResponse.class);
        return orm;
    }

    public ResponseMapper.OnlineModelTestResponse onlineModelTestMcqResponse(JSONObject response) {
        Gson gson = new Gson();
        ResponseMapper.OnlineModelTestResponse orm = gson.fromJson(response.toString(), ResponseMapper.OnlineModelTestResponse.class);
        return orm;
    }

    public ArrayList<Models.Subject> mtList(JSONArray response){
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Models.Subject>>(){}.getType();
        ArrayList<Models.Subject> orm = gson.fromJson(response.toString(), listType);
        return orm;
    }
}
