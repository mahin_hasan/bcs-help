package codedrizzlers.com.bcsrelease.others.PAlert.interfaces;

/**
 * Created by code drizzlers on 2/8/2017.
 */

public interface PAlertManager {

    boolean showAlert();
}
