package codedrizzlers.com.bcsrelease.others.PAlert;

import android.app.AlertDialog;
import android.view.View;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.others.PAlert.common.PAlertUtil;
import codedrizzlers.com.bcsrelease.others.PAlert.managers.LikeRateFeedbackAlertManager;

import static codedrizzlers.com.bcsrelease.others.PAlert.common.PAlertUtil.initButton;


/**
 * Created by code drizzlers on 2/9/2017.
 */

public class LikeRateFeedbackAlert {
    AlertDialog alert;
    View alertView;
    LikeRateFeedbackAlertManager manager;

    final int layout = R.layout.rate_feedback_alert_layout;
    final int titleImageId = R.id.rate_feedback_alert_title_image;
    final int textViewId = R.id.rate_feedback_alert_body_textview;

    public LikeRateFeedbackAlert(LikeRateFeedbackAlertManager man) {
        this.manager = man;
    }


    public void rateFeedbackAlertShow(int drawableId, int msgId,
                                      final RateFeedbackState state) {
        AlertDialog alert = PAlertUtil.createAlert();

        View alertView = PAlertUtil.createAlertView(layout);

        alert.setView(alertView);

        PAlertUtil.initImageView(alertView, titleImageId, drawableId);

        PAlertUtil.initTextView(alertView, textViewId, msgId);


        initializeRateFeedbackButton(R.id.rate_feedback_alert_no_button, alertView,
                alert, ButtonType.NEGETIVE, state);

        initializeRateFeedbackButton(R.id.rate_feedback_alert_yes_button, alertView, alert,
                ButtonType.POSITIVE, state);

        alert.show();
    }

    public void initializeRateFeedbackButton(int buttonInt, View alertView,
                                             final AlertDialog alert, final ButtonType type,
                                             final RateFeedbackState state) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectButtonAction(type, state);
                alert.dismiss();
            }
        };

        initButton(buttonInt, alertView, listener);
    }

    private void selectButtonAction(ButtonType type,
                                    RateFeedbackState state) {
        switch (type) {
            case POSITIVE:
                setActionForRateFeedbackYesButton(state);
                break;
            case NEGETIVE:
                setActionForRateFeedbackNoButton(state);
                break;
        }
    }

    public void setActionForRateFeedbackNoButton(RateFeedbackState state) {
        switch (state) {
            case LIKE:
                rateFeedbackAlertShow(
                        R.drawable.ic_feedback_black_18dp, R.string.feedack_qs, RateFeedbackState.FEEBACK);
                break;
            case FEEBACK:
                break;
            case RATE:
                break;

        }
    }

    public void setActionForRateFeedbackYesButton(RateFeedbackState state) {
        switch (state) {
            case LIKE:
                rateFeedbackAlertShow(R.drawable.ic_rate_review_black_24dp,
                        R.string.rate_qs, RateFeedbackState.RATE);
                break;
            case RATE:
                manager.endOfAlert();
                break;
            case FEEBACK:
                manager.showEmailDialog();
                break;
        }
    }

    public void showRFLAlert() {
        rateFeedbackAlertShow(
                R.drawable.ic_thumbs_up_down_black_24dp,
                R.string.like_qs, RateFeedbackState.LIKE);
    }

    public enum RateFeedbackState {
        LIKE,
        RATE,
        FEEBACK
    }

    public enum ButtonType {
        POSITIVE,
        NEGETIVE
    }
}
