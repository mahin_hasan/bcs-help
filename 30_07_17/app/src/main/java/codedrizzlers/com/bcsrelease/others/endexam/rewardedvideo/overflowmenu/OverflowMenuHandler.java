package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.overflowmenu;

import android.util.Log;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.main.MainActivity;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.internet.AccessNetworkJob;

/**
 * Created by code drizzlers on 4/2/2017.
 */

public class OverflowMenuHandler {
    private static final String  TAG ="OverflowMenuHandler";

    MainActivity activity;
    int id;

    public OverflowMenuHandler(MainActivity activity, int id) {
        this.activity = activity;
        this.id = id;
    }

    public void menusAction(){
        switch (id){
            case R.id.about_us:
                aboutUsMenu();
                break;
            case R.id.action_rate_us_on_play:
                rateIt();
                break;
            case R.id.action_send_your_opinion:
                showEmailDialog();
                break;
            case R.id.more_apps_menu:
                moreApps();
                break;
            case R.id.menu_app_share:
                shareIt();
                break;
        }

    }

    public void aboutUsMenu(){
        AboutUsAlert alert = new AboutUsAlert(activity);
        alert.showAlert();
    }

    /**
     * method for sending email with dialog
     * for getting content
     *
     * @return
     */
    private boolean showEmailDialog() {
        AccessNetworkJob networkJob = new AccessNetworkJob(this.activity);
        return networkJob.showEmailDialog();
    }


    /**
     * method for rating and review in playstore
     */
    private void rateIt() {
        AccessNetworkJob networkJob = new AccessNetworkJob(this.activity);
        networkJob.goToPlayStroe(activity.getString(R.string.play_store_id));
    }

    private void moreApps() {
        AccessNetworkJob networkJob = new AccessNetworkJob(this.activity);
        networkJob.moreApps();
    }

    /**
     * sharing method
     */
    private void shareIt() {
        AccessNetworkJob networkJob = new AccessNetworkJob(this.activity);
        networkJob.shareIt();
    }


    private static void log(String msg){
        Log.d(TAG,msg);
    }
}
