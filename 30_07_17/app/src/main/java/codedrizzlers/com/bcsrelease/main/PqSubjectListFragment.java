package codedrizzlers.com.bcsrelease.main;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.PqSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class PqSubjectListFragment extends BaseFragment {

    RecyclerView recyclerView;
    PqSubjectRecyclerViewAdapter adapter;

    private FragmentInteractionListener mListener;

    public PqSubjectListFragment() {
    }


    public static PqSubjectListFragment newInstance() {
        PqSubjectListFragment fragment = new PqSubjectListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //showToast("onCreate PqSubjectListFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pq_subject_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //showToast("onActivityCreated PqSubjectListFragment");
        populateAdapter();
    }

    public void populateAdapter(){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                List<PqSubject> list = AppSingleTon.DB_MANAGER.daoSession.getPqSubjectDao().loadAll();
                ArrayList<PqSubject> arrayList = list.isEmpty() ? new ArrayList<PqSubject>() : (ArrayList<PqSubject>)list;
                adapter = new PqSubjectRecyclerViewAdapter(arrayList, mListener);
                recyclerView.setAdapter(adapter);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    public void refreshAdapter(){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                List<PqSubject> list = (List<PqSubject>) AppSingleTon.DB_MANAGER.daoSession.getPqSubjectDao().loadAll();
                adapter.mValues.clear();
                adapter.mValues.addAll(list);
                adapter.notifyDataSetChanged();
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FragmentInteractionListener) {
            mListener = (FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface FragmentInteractionListener {
        void onListFragmentInteraction(PqSubject item);
    }
}
