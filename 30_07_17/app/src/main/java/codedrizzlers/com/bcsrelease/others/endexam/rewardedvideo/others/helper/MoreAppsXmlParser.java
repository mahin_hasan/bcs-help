package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.helper;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by code drizzlers on 10/25/2016.
 */
public class MoreAppsXmlParser {
    private static final String ns = null;


    public class App{
        public String name, iconUrl, description, playStoreID;
        public float rating;

        public App(String name, String iconUrl, String description, String playStoreUrl, float rating){
            this.name = name;
            this.iconUrl = iconUrl;
            this.description = description;
            this.playStoreID = playStoreUrl;
            this.rating = rating;
        }
    }

    public List parse(InputStream in) throws XmlPullParserException, IOException,android.os.NetworkOnMainThreadException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readMoreApps(parser);
        } finally {
            in.close();
        }
    }

    private List readMoreApps(XmlPullParser parser) throws XmlPullParserException, IOException {
        List apps = new ArrayList();

        parser.require(XmlPullParser.START_TAG, ns, "more_apps");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("app")) {
                apps.add(readApp(parser));
            } else {
                skip(parser);
            }
        }
        return apps;
    }


    private App readApp(XmlPullParser parser) throws XmlPullParserException, IOException{
        parser.require(XmlPullParser.START_TAG, ns, "app");
        String name = null, iconUrl = null, description = null, playStoreUrl = null;
        float rating = 0f;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            // Starts by looking for the entry tag
            if (tag.equals("name")) {
                name = readStringTag(parser, tag);
            } else if(tag.equals("icon_url")){
                iconUrl = readStringTag(parser, tag);
            }else if(tag.equals("rating")){
                rating = Float.parseFloat(readStringTag(parser,tag));
            }else if(tag.equals("description")){
                description = readStringTag(parser, tag);
            }else if(tag.equals("playstore_id")){
                playStoreUrl = readStringTag(parser, tag);
            }
            else {
                skip(parser);
            }
        }
        return new App(name,iconUrl,description,playStoreUrl,rating);
    }

    private String readStringTag(XmlPullParser parser, String tag) throws XmlPullParserException, IOException{
        parser.require(XmlPullParser.START_TAG, ns, tag);
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tag);
        return name;
    }

    // For the tags title and summary, extracts their text values.
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }


}
