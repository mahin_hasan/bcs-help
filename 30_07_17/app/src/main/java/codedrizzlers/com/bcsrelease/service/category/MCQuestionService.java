package codedrizzlers.com.bcsrelease.service.category;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.MpMcq;
import codedrizzlers.com.bcsrelease.database.MpMcqDao;
import codedrizzlers.com.bcsrelease.json.ResponseMapper;
import codedrizzlers.com.bcsrelease.service.SyncService;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import codedrizzlers.com.bcsrelease.utils.SPHandler;
import cz.msebera.android.httpclient.Header;

/**
 * Created by code drizzlers on 6/19/2017.
 */

public class MCQuestionService extends Service {
    private static final String TAG_HTTP_MP_MCQ_SYNC = "TAG_HTTP_MP_MCQ_SYNC";

    public static final String ACTION_MCQ_QUESTION_SYNC = "ACTION_MCQ_QUESTION_SYNC_DONE";

    @Override
    public void onCreate() {
        super.onCreate();
        mpMcqSync(1);
        Log.d("MCQSercive","MCQService is created.");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void mpMcqSync(int page) {

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.d("mcq onSuccess", "syncHeaders :" + response.toString());
                mpMcqSyncOnSuccess(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("mcqsync onFailure", "syncHeaders :" + errorResponse + "");
                Intent intent = new Intent(SyncService.FILTER_ACTION_SYNC_ERROR);
                sendBroadcast(intent);
                stopSelf();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        };

        handler.setTag(TAG_HTTP_MP_MCQ_SYNC);
        RequestParams params = new RequestParams();
        params.put(getString(R.string.page), page);
        params.put(getString(R.string.last_scanned_date), SPHandler.getMcqQuestionSyncDate());
        AppSingleTon.ASYNC_HTTP_CLIENT.post(MCQuestionService.this, AppSingleTon.APP_URL.SYNC_MP_MCQ, params, handler);
    }

    void mpMcqSyncOnSuccess(final JSONObject response) {

       Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ResponseMapper.McqResponse syncResponse = AppSingleTon.APP_JSON_PARSER.syncMcqResponse(response);
                pushToMpMcq(syncResponse.results);

                if (syncResponse.next_page > 0) {
                    mpMcqSync(syncResponse.next_page);
                }
                else {
                    stopService(syncResponse);
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);



       /* Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                ResponseMapper.McqResponse syncResponse = AppSingleTon.APP_JSON_PARSER.syncMcqResponse(response);
                pushToMpMcq(syncResponse.results);

                if (syncResponse.next_page > 0) {
                    mpMcqSync(syncResponse.next_page);
                } else {
                    stopService(syncResponse);
                }
            }
        };

        handler.post(runnable1);*/
    }

    void pushToMpMcq(final ArrayList<ResponseMapper.McqResponse.Mcq> mcqs) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                MpMcqDao mcqDao = AppSingleTon.DB_MANAGER.daoSession.getMpMcqDao();
                for (ResponseMapper.McqResponse.Mcq mcq : mcqs) {

                    MpMcq checkMcqDb = mcqDao.load(mcq.id);
                    if (checkMcqDb == null) {

                        MpMcq mcqDb = new MpMcq();
                        mcqDb.setRemote_id(mcq.id);
                        mcqDb.setSub_subject_remote_id(mcq.sub_subject);
                        mcqDb.setQuestion(mcq.question);
                        mcqDb.setAnswer(mcq.answer);
                        mcqDb.setChoice_one(mcq.choice_one);
                        mcqDb.setChoice_two(mcq.choice_two);
                        mcqDb.setChoice_three(mcq.choice_three);
                        mcqDb.setChoice_four(mcq.choice_four);
                        mcqDb.setDone(false);
                        mcqDao.insert(mcqDb);
                    } else {

                        if (mcq.is_deleted == true) {
                            mcqDao.deleteByKey(mcq.id);
                        } else {
                            checkMcqDb.setSub_subject_remote_id(mcq.sub_subject);
                            checkMcqDb.setQuestion(mcq.question);
                            checkMcqDb.setAnswer(mcq.answer);
                            checkMcqDb.setChoice_one(mcq.choice_one);
                            checkMcqDb.setChoice_two(mcq.choice_two);
                            checkMcqDb.setChoice_three(mcq.choice_three);
                            checkMcqDb.setChoice_four(mcq.choice_four);
                            mcqDao.update(checkMcqDb);
                        }
                    }
                    Log.d("InsertMCQ","Still inserting mcq question. question number "+mcq.id);
                }
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);
    }

    void stopService(ResponseMapper.McqResponse syncResponse){
        SPHandler.setMcqQuestionSyncDate(syncResponse.last_scanned_date);
        Intent intent = new Intent(MCQuestionService.ACTION_MCQ_QUESTION_SYNC);
        sendBroadcast(intent);
        stopSelf();
    }
}
