package codedrizzlers.com.bcsrelease.main;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.turingtechnologies.materialscrollbar.TouchScrollBar;

import java.util.List;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.MpMcq;
import codedrizzlers.com.bcsrelease.database.MpMcqDao;
import codedrizzlers.com.bcsrelease.database.MpSubSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class MpMcqActivity extends BaseActivity implements MpMcqRecyclerViewAdapter.FragmentInteractionListener {

    Toolbar toolbar;
    MpSubSubject subSubject;
    ActionBar actionBar;
    RecyclerView recyclerView;
    MpMcqRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp_mcq);
        initToolbar();
        initParse();
        initRecyclerView();
        initFastScrollBar();
        //for loading adview
        initAdView();
        initInerestialAd();
    }

    void initToolbar(){

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    void initParse(){

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            subSubject = (MpSubSubject) bundle.getSerializable(getString(R.string.sub_subject));
        }

        actionBar.setTitle(subSubject.getTitle());
    }

    void initSubTitle(int total){

        String sub = getString(R.string.number_of_mcq);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(total+"");
        sub = sub + getString(R.string.ti);

        actionBar.setSubtitle(sub);
    }

    void initRecyclerView(){
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        List<MpMcq> list = AppSingleTon.DB_MANAGER.daoSession.getMpMcqDao().
                queryBuilder().where(MpMcqDao.Properties.Sub_subject_remote_id.eq(subSubject.getRemote_id())).list();

        initSubTitle(list.size());

        adapter = new MpMcqRecyclerViewAdapter(list, (MpMcqRecyclerViewAdapter.FragmentInteractionListener)MpMcqActivity.this);
        recyclerView.setAdapter(adapter);

/*        Runnable runnable = new Runnable() {
            @Override
            public void run() {
            }
        };
        Handler handler = new Handler();
        handler.post(runnable);*/
    }

    void initFastScrollBar(){
        ((TouchScrollBar)findViewById(R.id.touchScrollBar))
                //.addIndicator(new CustomIndicator(this), true)
                .setHandleColour(ContextCompat.getColor(this, R.color.grey_300))
                .setHideDuration(500)
                .setTextColour(ContextCompat.getColor(this, R.color.monsoon))
                .setBarColour(ContextCompat.getColor(this, R.color.white));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == android.R.id.home){
            onBackOrHome();
        }
        return false;
    }

    @Override
    public void onListFragmentInteraction(MpMcq item) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onBackOrHome();
    }
}

