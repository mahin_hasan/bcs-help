package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Button;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.main.OnlineModelTestActivity;

/**
 * Created by code drizzlers on 3/30/2017.
 */

//This Class name is short Form of Alert Do not Close Rewarded Video
public class ADNCRVideo {
    private static final String TAG = "ADNCRVideo";

    OnlineModelTestActivity activity;

    private String positiveButtonTitle;
    private String alertMsg;

    public ADNCRVideo(OnlineModelTestActivity activity) {
        this.activity = activity;
        this.positiveButtonTitle = activity.getString(R.string.adncrvideo_alert_positive_button_title);
        this.alertMsg = activity.getString(R.string.adncrvideo_alert_msg);
    }

    public void showAlert(){
        AlertDialog.Builder builder = getBuilder();
        showAlert(builder);
    }

    public void showAlert(AlertDialog.Builder builder){
        AlertDialog alertDialog = builder.create();

        alertDialog.show();

        changeOkTextColor(alertDialog);
    }

    private void changeOkTextColor(AlertDialog alertDialog) {
        Button ok = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        ok.setTextColor(activity.getResources().getColorStateList(R.color.white));
    }

    public AlertDialog.Builder getBuilder(){
        AlertDialog.Builder builder = initBuilder();
        builder = setAlertMessage(builder,alertMsg);
        builder = setOkButton(builder);

        return builder;
    }

    public AlertDialog.Builder initBuilder(){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);

        return builder;
    }

    public AlertDialog.Builder setAlertMessage(AlertDialog.Builder builder, String msg){
        builder.setMessage(msg);

        return builder;
    }

    public AlertDialog.Builder setOkButton(AlertDialog.Builder builder){
        builder.setPositiveButton(positiveButtonTitle, getOkButtonLisetner());

        return builder;
    }

    public DialogInterface.OnClickListener getOkButtonLisetner(){
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                activity.showRewardedAd();
            }
        };

        return listener;
    }

    private static void log(String msg){
        Log.d(TAG,msg);
    }
}
