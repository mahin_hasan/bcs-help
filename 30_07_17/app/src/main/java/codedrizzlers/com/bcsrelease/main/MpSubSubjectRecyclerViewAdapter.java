package codedrizzlers.com.bcsrelease.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;
import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.MpMcq;
import codedrizzlers.com.bcsrelease.database.MpMcqDao;
import codedrizzlers.com.bcsrelease.database.MpSubSubject;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;

public class MpSubSubjectRecyclerViewAdapter extends RecyclerView.Adapter<MpSubSubjectRecyclerViewAdapter.ViewHolder> {

    private final List<MpSubSubject> mValues;
    private final OnSubSubjectListFragmentInteractionListener mListener;

    public MpSubSubjectRecyclerViewAdapter(List<MpSubSubject> items, OnSubSubjectListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sub_subject, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        String title = AppSingleTon.METHOD_BOX.convertNumericInBangla((position+1)+"");
        title = title+ ". " + mValues.get(position).getTitle();
        holder.mTitle.setText(title);

        MpMcqDao mpMcqDao = AppSingleTon.DB_MANAGER.daoSession.getMpMcqDao();
        QueryBuilder<MpMcq> qb = mpMcqDao.queryBuilder();
        qb.where(MpMcqDao.Properties.Sub_subject_remote_id
                .eq(mValues.get(position).getRemote_id()));

        String sub = holder.mView.getContext().getString(R.string.number_of_mcq);
        sub = sub +" : " +AppSingleTon.METHOD_BOX.convertNumericInBangla(qb.list().size()+"");
        sub = sub + holder.mView.getContext().getString(R.string.ti);
        holder.mSubTitle.setText(sub);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitle, mSubTitle;
        public MpSubSubject mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitle = (TextView) view.findViewById(R.id.title);
            mSubTitle = (TextView) view.findViewById(R.id.sub_title);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }

    public interface OnSubSubjectListFragmentInteractionListener {
        void onListFragmentInteraction(MpSubSubject item);
    }
}
