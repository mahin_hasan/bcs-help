package codedrizzlers.com.bcsrelease.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SDCardManager {
	
	public  String sd_card_mother_folder = "/bcs_preparation/";
	
	public File createFile(String filePath) {
		
		File file = new File(Environment.getExternalStorageDirectory() + filePath);
		boolean success = false;
		try {
			success = file.createNewFile();
			if(success) {
				return file;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public boolean createFolder(String folderPath) {

		File folder = new File(Environment.getExternalStorageDirectory() + folderPath);
		Log.d("file path", folder.getPath());
		boolean success = false;
		if(!folder.exists())
			success = folder.mkdirs();
		return success;

/*		if(folder.exists())
			Toast.makeText(context, "exists", Toast.LENGTH_LONG).show();
		else
			Toast.makeText(context, "not found", Toast.LENGTH_LONG).show();*/
	}

	public byte[] decodeFileOne(File f) {

		BitmapFactory.Options opts = new BitmapFactory.Options ();
		opts.inSampleSize = 2;
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		Bitmap bitmap = BitmapFactory.decodeFile(f.getPath(), opts);
		bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
		byte[] data = stream.toByteArray();
		return data;
	}

	public byte[] fileToByte(File file) {

		byte[] data = null;
		try {
			InputStream is = new BufferedInputStream(new FileInputStream(file));
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			while (is.available() > 0) {
				bos.write(is.read());
			}
			data = bos.toByteArray();
			if(is!=null)
				is.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			data = null;
		} catch (IOException e) {
			e.printStackTrace();
			data = null;
		}

		return data;
	}
	// its also works for nested folder
	public List<File> getListFiles(File parentDir, String fileType) {

		ArrayList<File> inFiles = new ArrayList<File>();
		File[] files = parentDir.listFiles();
		if (files != null) {
			Log.d("file list size", files.length + "");

			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				if (file.isDirectory()) {
					inFiles.addAll(getListFiles(file, fileType));
				} else if (file.getName().endsWith(fileType)) {
					inFiles.add(file);
				}
			}
		} else {
			Log.d("null found", "mother folder is empty");
		}
		return inFiles;
	}
	 
	/* "\Trails2Bike\trail_timestamp_folder\trail.json" */
	public boolean isFileExists(String filePath){
		
		File file = new File(Environment.getExternalStorageDirectory() + filePath);
		if (file.exists())
			return true;
		return false;
	}
	
	/* "\Trails2Bike" */
	public boolean isFolderExists(String folderPath){
		
		File folder = new File(Environment.getExternalStorageDirectory() + folderPath);
		if (folder.exists())
			return true;
		return false;
	}
	
	public boolean isSDCardExists(){
		
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			return true;
		}
		return false;
	}
	
	/**
	 * @return Number of bytes available on External storage
	 */
	public static long getAvailableSpaceInBytes() {
		
	    long availableSpace = -1L;
	    StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
	    availableSpace = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();

	    return availableSpace;
	}

	/**
	 * @return Number of kilo bytes available on External storage
	 */
	public long getAvailableSpaceInKB(){
		
	    final long SIZE_KB = 1024L;
	    long availableSpace = -1L;
	    StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
	    availableSpace = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();
	    return availableSpace/SIZE_KB;
	}
	
	/**
	 * @return Number of Mega bytes available on External storage
	 */
	public long getAvailableSpaceInMB(){
		
	    final long SIZE_KB = 1024L;
	    final long SIZE_MB = SIZE_KB * SIZE_KB;
	    long availableSpace = -1L;
	    StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
	    availableSpace = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();
	    return availableSpace/SIZE_MB;
	}

}
