package codedrizzlers.com.bcsrelease.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.database.RecyclerItemClickListener;
import codedrizzlers.com.bcsrelease.json.Models;
import codedrizzlers.com.bcsrelease.utils.AppSingleTon;
import cz.msebera.android.httpclient.Header;

public class OnlineModelTestFragment extends BaseFragment {
    BroadcastReceiver networkStateBroadcastReciever;
    boolean isAlreadyPull = false;


    private OnFragmentInteractionListener mListener;
    RecyclerView recyclerView;

    public OnlineModelTestFragment() {
        // Required empty public constructor
    }

    public static OnlineModelTestFragment newInstance() {
        OnlineModelTestFragment fragment = new OnlineModelTestFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        networkStateBroadcastReciever = getNetworkStateBroadcastReciever();
        pullModelTestListWithInternetCheck();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_model_test, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        //Intent intent = new Intent(getActivity(), OnlineModelTestActivity.class);
        //startActivity(intent);

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    /*@Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pullModelTestListWithInternetCheck();
    }*/

    void pullModelTestListWithInternetCheck() {
        if (AppSingleTon.METHOD_BOX.isInternetConnected()) {
            pullModelTestList();
            isAlreadyPull = true;
        }
    }

    void pullModelTestList() {

        JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.d("mt list", response.toString());
                onMtListPullSuccess(response);
                super.onSuccess(statusCode, headers, response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFinish() {
                dismissProgressDialog();
                super.onFinish();
            }

            @Override
            public void onStart() {
                showUpdateProgressDialog("pulling...");
                super.onStart();
            }
        };
        AppSingleTon.ASYNC_HTTP_CLIENT.get(getActivity(), AppSingleTon.APP_URL.ONLINE_MODEL_TEST_LIST, handler);
    }

    void onMtListPullSuccess(JSONArray response) {

        final ArrayList<Models.Subject> list = AppSingleTon.APP_JSON_PARSER.mtList(response);
        Collections.reverse(list);
        MtListRecyclerViewAdapter adapter = new MtListRecyclerViewAdapter(list);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onDoubleClick(View view, int position) {

            }

            @Override
            public void onSingleClick(View view, int position) {
                if (AppSingleTon.METHOD_BOX.isInternetConnected()) {
                    Intent intent = new Intent(getActivity(), OnlineModelTestActivity.class);
                    intent.putExtra(getString(R.string.subject_id), list.get(position).id);
                    startActivity(intent);
                } else
                    showInternetToast();
            }

            @Override
            public void onLongPress(View view, int position) {

            }
        }));
    }

    ArrayList<Models.Subject> reverseList(ArrayList<Models.Subject> list){
        return null;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(networkStateBroadcastReciever);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(networkStateBroadcastReciever, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }

    public void showInternetToast() {
        Toast.makeText(
                getContext(), "Sorry no internet.", Toast.LENGTH_LONG
        ).show();
    }

    public BroadcastReceiver getNetworkStateBroadcastReciever() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo ni = manager.getActiveNetworkInfo();

                if (!isAlreadyPull)
                    pullModelTestListWithInternetCheck();
            }
        };
    }
}
