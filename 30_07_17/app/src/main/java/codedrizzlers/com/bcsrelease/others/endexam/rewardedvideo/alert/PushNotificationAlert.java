package codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.alert;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;



import java.util.HashMap;
import java.util.Map;

import codedrizzlers.com.bcsrelease.R;
import codedrizzlers.com.bcsrelease.others.endexam.rewardedvideo.others.internet.AccessNetworkJob;

/**
 * Created by code drizzlers on 11/6/2016.
 */

public class PushNotificationAlert {
    private static final String TAG = "PushAlertTask";

    Context context;
    Map<String , String> data = new HashMap<String, String>();


    public PushNotificationAlert(Context context, Bundle bundle){
        this.context = context;
        createData(bundle);
    }

    private void createData(Bundle bundle) {
        insertDataIntoMap("package_app_store",bundle);
        insertDataIntoMap("info_url",bundle);
        insertDataIntoMap("drizzlers_msg",bundle);
        insertDataIntoMap("only_msg",bundle);
    }

    private void insertDataIntoMap(String key, Bundle bundle){
        if(bundle.containsKey(key))
            data.put(key,bundle.getString(key));
    }

    public void isPushNotificationShouldShown(){
        //Log.d("push","come to push alert.");
        //Log.d("push","data is "+ data.toString());
        if(data.containsKey("drizzlers_msg"))
            showPushNotificationAlert();
        else
            return;
    }

    public void showPushNotificationAlert() {
        AlertDialog alert = createAlert();

        View alertView = createAlertLayoutView();
        alert.setView(alertView);

        initializeAlertTextView(alertView, R.id.push_notification_alert_body_textview,data,"drizzlers_msg");


        processButton(alertView, alert,data);

        alert.show();
    }

    private AlertDialog createAlert(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        AlertDialog alert = dialogBuilder.create();
        return alert;
    }

    private View createAlertLayoutView(){
        LayoutInflater inflater = (LayoutInflater) context. getSystemService( Context. LAYOUT_INFLATER_SERVICE );
        View alertView = inflater.inflate(R.layout.push_notification_alert_layout, null);
        return alertView;
    }

    private void initializeAlertTextView(View alertView,int textViewId,Map<String,String> data, String key){
        TextView bodyTextView = (TextView)alertView.findViewById(textViewId);
        if(data.containsKey(key))
            bodyTextView.setText(data.get(key));
    }

    private Button initializeAlertButton(View alertView, int buttonId, View.OnClickListener listener){
        //Log.d("PushCanel","Program to INITIALIZE BUTTON.");
        Button button = (Button)alertView.findViewById(buttonId);
        button.setOnClickListener(listener);

        return button;
    }

    private void processButton(final View alertView, final AlertDialog alert, final Map<String, String> data) {
        if(data.containsKey("only_msg")){
            initializeButtonForOnlyMsg(alertView,alert);
        }
        else
            initializeNormalLayoutButton(alertView, alert, data);

    }

    private void initializeButtonForOnlyMsg(View alertView, AlertDialog alert) {
        removeOkButton(alertView);
        processCancelButtonForOnlyMsg(alertView, alert);
    }

    private void processCancelButtonForOnlyMsg(View alertView, AlertDialog alert) {
        Button button =
                initializeAlertButton(alertView,R.id.push_notification_alert_cancel_button,getCancelButtonListener(alert));
        setFakeOkButtonParameters(button);
    }

    private void setFakeOkButtonParameters(Button button) {
        button.setText("Ok");
        abjustButtonLayout(button);
    }

    private void abjustButtonLayout(Button button) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) button.getLayoutParams();
        params.weight=2f;
    }
    private void removeOkButton(View alertView) {
        Button okButton = (Button)alertView.findViewById(R.id.push_notification_alert_ok_button);
        removeOkButtonFromLayout(okButton);
    }

    private void removeOkButtonFromLayout(Button button){
        ViewGroup layout = (ViewGroup) button.getParent();
        if(null!=layout) //for safety only  as you are doing onClick
            layout.removeView(button);
        layout.requestLayout();
    }

    private void initializeNormalLayoutButton(final View alertView, final AlertDialog alert, final Map<String, String> data){
        initializeAlertButton(alertView, R.id.push_notification_alert_ok_button,
                getActionButtonlistener(data,alert));

        initializeAlertButton(alertView, R.id.push_notification_alert_cancel_button,
                getCancelButtonListener(alert));
    }

    private View.OnClickListener getActionButtonlistener(final  Map<String, String> data, final AlertDialog alert){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
                setButtonAction(data,alert);
                //Log.d("PushCanel","Program to PUSH Yes LISTENER.");
            }
        };

        return listener;
    }

    private View.OnClickListener getCancelButtonListener(final AlertDialog alert){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.cancel();
            }
        };

        return listener;
    }

    private void setButtonAction(Map<String, String> data, AlertDialog alert){
        AccessNetworkJob job = new AccessNetworkJob(context);

        if(data.containsKey("package_app_store"))
            job.goToPlayStroe(data.get("package_app_store"));
        else if(data.containsKey("info_url"))
            job.goToURL(data.get("info_url"));
    }

}
