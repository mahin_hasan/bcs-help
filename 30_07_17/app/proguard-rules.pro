# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in F:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

#need to keep MyFirebaseInstanceIdService & MyFirebaseMessagingService which is probably
#only used in manifest file
#
-keep class codedrizzlers.com.bcsrelease.json.**{*;}
-keep class codedrizzlers.com.bcsrelease.R
-keep class codedrizzlers.com.bcsrelease.main.OnlineModelTestActivity
-keep class codedrizzlers.com.bcsrelease.main.QapQuestionAnswerActivity
-keep class com.getkeepsafe.taptargetview.**{*;}
-keep class android.support.v7.widget.Toolbar
-keep class android.support.design.widget.AppBarLayout
-keep public class codedrizzlers.com.bcsrelease.service.firebase.MyFirebaseInstanceIdService
-keep public class codedrizzlers.com.bcsrelease.service.firebase.MyFirebaseMessagingService
### greenDAO 3
-keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
public static java.lang.String TABLENAME;
}
-keep class **$Properties

# If you do not use SQLCipher:
-dontwarn org.greenrobot.greendao.database.**
# If you do not use RxJava:
-dontwarn rx.**
-dontwarn javax.swing.**
-dontwarn java.rmi.**
-dontwarn javax.servlet.**
-dontwarn java.beans.**
-dontwarn org.apache.**
-dontwarn org.jaxen.**
-dontwarn com.sun.org.apache.**
-dontwarn org.w3.**
-dontwarn org.jdom.**
-dontwarn javax.el.**
-dontwarn org.python.**
-dontwarn org.mozilla.**
-dontwarn org.dom4j.**
-dontwarn org.slf4j.**
-dontwarn org.zeroturnaround.**
-dontwarn freemarker.**


