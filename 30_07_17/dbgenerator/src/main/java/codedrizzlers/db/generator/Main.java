package codedrizzlers.db.generator;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;


public class Main {
    private static final String PROJECT_DIR = System.getProperty("user.dir");

    public static void main(String[] args){

        Schema schema = new Schema(1, "codedrizzlers.com.bcsrelease.database");

        /*for question answer preparation*/
        Entity qapSubject = schema.addEntity(DBFields.TABLE_QAP_SUBJECT);
        qapSubject.addLongProperty(DBFields.COLUMN_REMOTE_ID).primaryKey();
        qapSubject.addStringProperty(DBFields.COLUMN_TITLE);

        Entity qapSubSubject = schema.addEntity(DBFields.TABLE_QAP_SUB_SUBJECT);
        qapSubSubject.addLongProperty(DBFields.COLUMN_REMOTE_ID).primaryKey();
        Property qapSubjectProperty = qapSubSubject.addLongProperty(DBFields.COLUMN_SUBJECT_REMOTE_ID)
                .notNull().getProperty();
        qapSubSubject.addToOne(qapSubject, qapSubjectProperty);
        qapSubSubject.addStringProperty(DBFields.COLUMN_TITLE);

        Entity qapQuestionAnswer = schema.addEntity(DBFields.TABLE_QAP_QUESTION_ANSWER);
        qapQuestionAnswer.addLongProperty(DBFields.COLUMN_REMOTE_ID).primaryKey();
        Property qapSubSubjectProperty = qapQuestionAnswer.addLongProperty(DBFields.COLUMN_SUB_SUBJECT_REMOTE_ID)
                .notNull().getProperty();
        qapQuestionAnswer.addToOne(qapSubSubject, qapSubSubjectProperty);
        qapQuestionAnswer.addBooleanProperty(DBFields.COLUMN_DONE);
        qapQuestionAnswer.addStringProperty(DBFields.COLUMN_QUESTION);
        qapQuestionAnswer.addStringProperty(DBFields.COLUMN_ANSWER);

        /* for mcq preparation*/
        Entity mpSubject = schema.addEntity(DBFields.TABLE_MP_SUBJECT);
        mpSubject.addLongProperty(DBFields.COLUMN_REMOTE_ID).primaryKey();
        mpSubject.addStringProperty(DBFields.COLUMN_TITLE);

        Entity mpSubSubject = schema.addEntity(DBFields.TABLE_MP_SUB_SUBJECT);
        mpSubSubject.addLongProperty(DBFields.COLUMN_REMOTE_ID).primaryKey();
        Property mpSubjectProperty = mpSubSubject.addLongProperty(DBFields.COLUMN_SUBJECT_REMOTE_ID)
                .notNull().getProperty();
        mpSubSubject.addToOne(mpSubject, mpSubjectProperty);
        mpSubSubject.addStringProperty(DBFields.COLUMN_TITLE);


        Entity mpMcq = schema.addEntity(DBFields.TABLE_MP_MCQ);
        mpMcq.addLongProperty(DBFields.COLUMN_REMOTE_ID).primaryKey();
        Property mpSubSubjectProperty = mpMcq.addLongProperty(DBFields.COLUMN_SUB_SUBJECT_REMOTE_ID)
                .notNull().getProperty();
        mpMcq.addToOne(mpSubSubject, mpSubSubjectProperty);

        mpMcq.addBooleanProperty(DBFields.COLUMN_DONE);
        mpMcq.addStringProperty(DBFields.COLUMN_QUESTION);
        mpMcq.addStringProperty(DBFields.COLUMN_ANSWER);
        mpMcq.addStringProperty(DBFields.COLUMN_MCQ_CHOICE_ONE);
        mpMcq.addStringProperty(DBFields.COLUMN_MCQ_CHOICE_TWO);
        mpMcq.addStringProperty(DBFields.COLUMN_MCQ_CHOICE_THREE);
        mpMcq.addStringProperty(DBFields.COLUMN_MCQ_CHOICE_FOUR);

        /* for previous question*/
        Entity pqSubject = schema.addEntity(DBFields.TABLE_PQ_SUBJECT);
        pqSubject.addLongProperty(DBFields.COLUMN_REMOTE_ID).primaryKey();
        pqSubject.addStringProperty(DBFields.COLUMN_TITLE);

        Entity pqSubSubject = schema.addEntity(DBFields.TABLE_PQ_SUB_SUBJECT);
        pqSubSubject.addLongProperty(DBFields.COLUMN_REMOTE_ID).primaryKey();
        Property pqSubjectProperty = pqSubSubject.addLongProperty(DBFields.COLUMN_SUBJECT_REMOTE_ID)
                .notNull().getProperty();
        pqSubSubject.addToOne(pqSubject, pqSubjectProperty);
        pqSubSubject.addStringProperty(DBFields.COLUMN_TITLE);


        Entity pqMcq = schema.addEntity(DBFields.TABLE_PQ_MCQ);
        pqMcq.addLongProperty(DBFields.COLUMN_REMOTE_ID).primaryKey();
        Property pqSubSubjectProperty = pqMcq.addLongProperty(DBFields.COLUMN_SUB_SUBJECT_REMOTE_ID)
                .notNull().getProperty();
        pqMcq.addToOne(pqSubSubject, pqSubSubjectProperty);

        pqMcq.addBooleanProperty(DBFields.COLUMN_DONE);
        pqMcq.addStringProperty(DBFields.COLUMN_QUESTION);
        pqMcq.addStringProperty(DBFields.COLUMN_ANSWER);
        pqMcq.addStringProperty(DBFields.COLUMN_MCQ_CHOICE_ONE);
        pqMcq.addStringProperty(DBFields.COLUMN_MCQ_CHOICE_TWO);
        pqMcq.addStringProperty(DBFields.COLUMN_MCQ_CHOICE_THREE);
        pqMcq.addStringProperty(DBFields.COLUMN_MCQ_CHOICE_FOUR);


        try {
            DaoGenerator daoGenerator = new DaoGenerator();
            daoGenerator.generateAll(schema, PROJECT_DIR + "\\app\\src\\main\\java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
