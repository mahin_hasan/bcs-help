package codedrizzlers.db.generator;

public class DBFields {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "DATABASE_BCS_HELP";

    public static final String TABLE_QAP_SUBJECT = "QapSubject";
    public static final String TABLE_QAP_SUB_SUBJECT = "QapSubSubject";
    public static final String TABLE_QAP_QUESTION_ANSWER = "QapQuestionAnswer";

    public static final String TABLE_MP_SUBJECT = "MpSubject";
    public static final String TABLE_MP_SUB_SUBJECT = "MpSubSubject";
    public static final String TABLE_MP_MCQ = "MpMcq";

    public static final String TABLE_PQ_SUBJECT = "PqSubject";
    public static final String TABLE_PQ_SUB_SUBJECT = "PqSubSubject";
    public static final String TABLE_PQ_MCQ = "PqMcq";

    public static final String TABLE_STATUS = "Status";

    /* COMMON TABLE COLUMNS */
    public static final String ID = "id";
    public static final String REMOTE_ID = "remote_id";
    public static final String TITLE = "title";
    public static final String IS_DELETED = "is_deleted";
    //public static final String ROW_VERSION = "row_version";


    public static final String COLUMN_REMOTE_ID = REMOTE_ID;
    public static final String COLUMN_TITLE = TITLE;
    public static final String COLUMN_DONE = "done";

    //public static final String COLUMN_ROW_VERSION = ROW_VERSION;

    public static final String COLUMN_SUBJECT_REMOTE_ID = "subject_remote_id";
    public static final String COLUMN_SUB_SUBJECT_REMOTE_ID = "sub_subject_remote_id";

    public static final String COLUMN_QUESTION = "question";
    public static final String COLUMN_ANSWER = "answer";
    public static final String COLUMN_MCQ_CHOICE_ONE = "choice_one";
    public static final String COLUMN_MCQ_CHOICE_TWO = "choice_two";
    public static final String COLUMN_MCQ_CHOICE_THREE = "choice_three";
    public static final String COLUMN_MCQ_CHOICE_FOUR = "choice_four";

    /* FOR STATUS TABLE */
    public static final String COLUMN_STATUS_CONTENT_TYPE_ID = "content_type_id";
    public static final String COLUMN_STATUS_OBJECT_ID = "object_id";

}
